<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class pages extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();

			//jika belum login redirect ke login lagi
			if ($this->session->userdata('logged') <> 1) {
				redirect(site_url('form-html/login'));
			}

      		$this->load->model('about_model', 'about_m');
      		$this->load->helper('html');
		}

		public function index()
		{

		}

	    public function showAbout()
	    {
	    	if($this->about_m->getAbout()->num_rows() > 0)
	    	{
		    	$data = array('abouts' => $this->about_m->getAbout()->row());
		    	$this->load->view('view-admin/layout/header');
		    	$this->load->view('view-admin/pages/about', $data);
		    	$this->load->view('view-admin/layout/footer');
		    }
	    }

	    public function actionEdit()
	    {
	    	//set rules
			$this->form_validation->set_rules("title", "Title", "trim|required");
			$this->form_validation->set_rules("description", "Description", "trim|required");

			//set pesan
			$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');

			if($this->form_validation->run())
  			{
  				$data = array(
  					'title'       => $this->input->post('title'),
  					'description' => $this->input->post('description')
  				);

  				$this->about_m->update_about($data);

  				$this->upload_image($this->input->post('foto'));
  				if($this->upload->do_upload('foto'))
				{
					$data = array('image' => $this->upload->file_name);
					$this->about_m->update_about($data);
					
					$this->session->set_flashdata('msg', '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data Berhasi Diubah</div>');
					redirect('form-html/pages/showAbout');
				}
				else
				{
					$this->session->set_flashdata('msg', '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data Berhasi Diubah, Tidak Ada Perubahan Pada Image</div>');
					redirect('form-html/pages/showAbout');
				}
  			}
  			else
  			{
  				$this->index();
  			}
	    }

	    public function upload_image($image_name)
		{
			$directory = './images/uploads';
			if(!is_dir($directory))
			{
				mkdir($directory, 0777, TRUE);
				$dir_exist = false;
			}

			$config['upload_path'] 	 = $directory;
			$config['file_name'] 	 = url_title($image_name);
			$config['allowed_types'] = 'gif|jpg|png|jpeg'; //type yang dapat diupload
			$config['max_size'] 	 = '5048'; //maksimum besar file 2M
			$config['max_width']  	 = '5288'; //lebar maksimum 5288 px
			$config['max_height']  	 = '5288'; //tinggi maksimum 1588 px

			$this->load->library('upload', $config);
		}

		public function showProfil()
	    {
	    	if($this->about_m->getProfil()->num_rows() > 0)
	    	{
		    	$data = array('abouts' => $this->about_m->getProfil()->row());
		    	$this->load->view('view-admin/layout/header');
		    	$this->load->view('view-admin/pages/profil', $data);
		    	$this->load->view('view-admin/layout/footer');
		    }
	    }

	    public function actionEditProfil()
	    {
	    	//set rules
			$this->form_validation->set_rules("title", "Title", "trim|required");
			$this->form_validation->set_rules("description", "Description", "trim|required");

			//set pesan
			$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');

			if($this->form_validation->run())
  			{
  				$data = array(
  					'title'       => $this->input->post('title'),
  					'description' => $this->input->post('description')
  				);

  				$this->about_m->update_Profil($data);

  				$this->upload_image($this->input->post('foto'));
  				if($this->upload->do_upload('foto'))
				{
					$data = array('image' => $this->upload->file_name);
					$this->about_m->update_Profil($data);
					
					$this->session->set_flashdata('msg', '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data Berhasi Diubah</div>');
					redirect('form-html/pages/showProfil');
				}
				else
				{
					$this->session->set_flashdata('msg', '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data Berhasi Diubah, Tidak Ada Perubahan Pada Image</div>');
					redirect('form-html/pages/showProfil');
				}
  			}
  			else
  			{
  				$this->index();
  			}
	    }

	}