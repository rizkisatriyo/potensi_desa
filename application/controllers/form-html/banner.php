<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Banner extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();

			//jika belum login redirect ke login lagi
			// if ($this->session->userdata('logged') <> 1) {
				// redirect(site_url('form-html/login'));llllll
			// }

      $this->load->model('banner_front');
      $this->load->helper('html');
		}

		public function index()
		{
			$this->ShowBanners();
		}

    function Showbanners()
    {
      $data = array(
        'banners' => $this->banner_front->selectAllBannerFront(),
        'bannersProduct' => $this->banner_front->selectAllBannerProduct(),
				'bannersSingle' => $this->banner_front->selectAllBannerSingle(),
				'searchView' => $this->load->view('view-admin/layout/top_search', '', TRUE)
		);
      $this->load->view('view-admin/layout/header', $data);
      $this->load->view('view-admin/pages/banner', $data);
      $this->load->view('view-admin/layout/footer');
    }

    public function actionDelete($id)
    {
      if(!empty($id))
      {
        $data = array('status' => 0);
        $this->banner_front->delete_banner($id, $data);

        $pesan = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data Berhasil Dihapus</div>';

        redirect('form-html/banner');

      }
      else
      {
        redirect('form-html/banner');
      }
    }

    function formAddHome($pesan ='')
  	{
  		$this->load->view('view-admin/layout/header');
  		$this->load->view('view-admin/pages/add_banner_home');
  		$this->load->view('view-admin/layout/footer');
  	}

    function formAddProduct($pesan ='')
    {
      $this->load->view('view-admin/layout/header');
      $this->load->view('view-admin/pages/add_banner_product');
      $this->load->view('view-admin/layout/footer');
    }

    function actionAdd()
    {

  				$this->form_validation->set_rules('title', 'title', 'trim|required');
          $this->form_validation->set_rules('description', 'description', 'trim|required');

  				if ($this->form_validation->run() == TRUE)
          {
              $this->upload_image($this->input->post('foto'));

              if($this->upload->do_upload('foto'))
              {
                $data = array(
                  'title'       => $this->input->post('title'),
                  'description' => $this->input->post('description'),
                  'image'       => $this->upload->file_name,
                  'kode_banner' => $this->input->post('kode_banner'),
                );

                $this->banner_front->insert_banner_front($data);
                redirect('form-html/banner');
              }
              else
              {
                redirect('form-html/banner');
                // echo "jos";
              }
          }
          else
          {
              redirect('form-html/banner');
            // echo "gagal";
          }
    }

    function actionEdit()
    {
        $this->upload_image($this->input->post('foto'));
        $id   = $this->input->post('id_banner');
        $kode = $this->input->post('kode_banner');

        if(!empty($id))
        {
          if($kode == 3)
          {
              if($this->upload->do_upload('foto'))
              {
                $data = array(
                  'image'       => $this->upload->file_name,
                  'kode_banner' => $kode,
                );
              }
              else
              {
                redirect('form-html/banner');
              }
          }
          else
          {
             $data = array(
               'title'       => $this->input->post('title'),
               'description' => $this->input->post('description'),
             );

            $this->banner_front->update_banner_front($id, $data);
            
            if($this->upload->do_upload('foto'))
            {
              $data = array(
                'image'       => $this->upload->file_name,
                'kode_banner' => $kode,
                );
              }
              else
              {
                redirect('form-html/banner');
              }
          }

          $this->banner_front->update_banner_front($id, $data);
          redirect('form-html/banner');
        }
        else
        {
          redirect('form-html/banner');
        }
    }

		public function formEdit($id = null, $kode= null)
		{
			if(!empty($id) && !empty($kode))
			{
				$data = array('banner'  => $this->banner_front->getBannerDetails($id, $kode)->result());

				if($this->banner_front->getBannerDetails($id, $kode)->num_rows > 0)
				{
					$this->load->view('view-admin/layout/header');
					$this->load->view('view-admin/pages/edit_banner_home', $data);
					$this->load->view('view-admin/layout/footer');
				}
				else
				{
					redirect('form-html/banner');
				}
			}
			else
			{
				redirect('form-html/banner');
			}
		}



    public function upload_image($image_name)
    {
      $directory = './images/uploads';
      if(!is_dir($directory))
      {
        mkdir($directory, 0777, TRUE);
        $dir_exist = false;
      }

      $config['upload_path']   = $directory;
      $config['file_name']     = url_title($image_name);
      $config['allowed_types'] = 'gif|jpg|png|jpeg'; //type yang dapat diupload
      $config['max_size']      = '5048'; //maksimum besar file 2M
      $config['max_width']     = '5288'; //lebar maksimum 5288 px
      $config['max_height']    = '5288'; //tinggi maksimum 1588 px

      $this->load->library('upload', $config);
    }
}
