<?php if(! defined('BASEPATH')) exit('No direct script access allowed');

	class Blog extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();

			//jika belum login redirect ke login lagi
			if ($this->session->userdata('logged') <> 1) {
				redirect(site_url('form-html/login'));
			}

			$this->load->model('blog_model', 'blog');
			$this->load->helper('html');
		}

		public function index()
		{
			$this->showBlog();
		}

		public function showBlog()
		{
			$data = array('blogs' => $this->blog->get_all_blog());

			$this->load->view('view-admin/layout/header');
			$this->load->view('view-admin/pages/blog', $data);
			$this->load->view('view-admin/layout/footer');
		}

		//add blog
		public function formAdd($pesan = '')
		{
			$data = array('pesan' => $pesan);

			$this->load->view('view-admin/layout/header');
			$this->load->view('view-admin/pages/add_blog', $data);
			$this->load->view('view-admin/layout/footer');
		}

		public function actionAdd()
		{
			$this->setting_rules();

			//jalankan validasi
			if ($this->form_validation->run() == FALSE)
			{
				$this->formAdd();
			}
			else
			{
				$this->upload_image($this->input->post('blog_image'));

				if (!$this->upload->do_upload('blog_image'))
				{
					// $pesan = $this->upload->display_errors();
					echo "gagal";
				}
				else
				{

					$data = array(
						'blog_title'  => $this->input->post('blog_title'),
						'blog_desc'   => $this->input->post('blog_desc'),
						'blog_image'  => $this->upload->file_name,
					);

					$this->blog->insert_blog($data);

					$pesan = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data Berhasi Disimpan</div>';
				}
			}

			// $this->formAdd($pesan);
			redirect('form-html/blog', 'refresh');
		}

		public function formEdit($id = null, $pesan = null)
		{
			if(!empty($id))
			{
				$data = array(
					'blogs'  => $this->blog->get_by_id_blog($id)->result(),
					'pesan'  => $pesan
				);

				if($this->blog->get_by_id_blog($id)->num_rows > 0)
				{
					$this->load->view('view-admin/layout/header');
					$this->load->view('view-admin/pages/edit_blog', $data);
					$this->load->view('view-admin/layout/footer');
				}
				else
				{
					redirect('form-html/blog');
				}
			}
			else
			{
				redirect('form-html/blog');
			}
		}

		public function actionEdit()
		{
			$this->setting_rules();

			$id = $this->input->post('blog_id');

			//jalankan validasi
			if ($this->form_validation->run() == FALSE)
			{
				$this->formEdit($id);
			}
			else
			{
				if(!empty($id))
				{
					$data = array(
						'blog_title'  => $this->input->post('blog_title'),
						'blog_desc'   => $this->input->post('blog_desc'),
					);

					$this->blog->update_blog($id, $data);
					
					$this->upload_image($this->input->post('blog_image'));

					if($this->upload->do_upload('blog_image'))
					{
						$data = array('blog_image' => $this->upload->file_name);
						$this->blog->update_blog($id, $data);
					}

					$pesan = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data Berhasi Diubah</div>';

					// $this->formEdit($id, $pesan);
					redirect('form-html/blog', 'refresh');
				}
				else
				{
					redirect('form-html/blog', 'refresh');
				}
			}
		}

		public function actionDelete($id = null)
		{
			if(!empty($id))
			{
				// hapus data
				$data = array('blog_status' => '0');
				$this->blog->update_blog($id, $data);

				redirect('form-html/blog', 'refresh');
			}
			else
			{
				redirect('form-html/blog', 'refresh');
			}
		}

		public function setting_rules()
		{
			//set rules
			$this->form_validation->set_rules('blog_title', 'Title Blog', 'trim|required');
			$this->form_validation->set_rules('blog_desc', 'description', 'trim|required');

			$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
		}

		public function upload_image($image_name)
		{
			$directory = './images/uploads';
			if(!is_dir($directory))
			{
				mkdir($directory, 0777, TRUE);
				$dir_exist = false;
			}

			$config['upload_path'] 	 = $directory;
			$config['file_name'] 	 = url_title($image_name);
			$config['allowed_types'] = 'gif|jpg|png|jpeg'; //type yang dapat diupload		
			$config['max_size'] 	 = '5048'; //maksimum besar file 2M
			$config['max_width']  	 = '5288'; //lebar maksimum 5288 px
			$config['max_height']  	 = '5288'; //tinggi maksimum 1588 px

			$this->load->library('upload', $config);
		}
	}
