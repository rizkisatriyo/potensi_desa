<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Setting extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();

			//jika belum login redirect ke login lagi
			if ($this->session->userdata('logged') <> 1) {
				redirect(site_url('form-html/login'));
			}

			$this->load->model('setting_model', 'setting');
			$this->load->helper('html');
		}

		public function index()
		{
			$data = array(
				'logos'    => $this->setting->get_all_logo(),
				'socials'  => $this->setting->get_all_social(),
				'contacts' => $this->setting->get_all_contact(),
			);

			$this->load->view('view-admin/layout/header');
			$this->load->view('view-admin/pages/setting', $data);
			$this->load->view('view-admin/layout/footer');
		}

		/*public function getLogoById($id)
		{
			if(!empty($id))
			{
				$data = array(
					'logos'  => $this->setting->getLogoById($id)->result()
				);

				if($this->setting->getLogoById($id)->num_rows > 0)
				{
					$this->load->view('view-admin/layout/header');
					$this->load->view('view-admin/pages/edit_logo', $data);
					$this->load->view('view-admin/layout/footer');
				}
				else
				{
					redirect('form-html/setting');
				}
			}
			else
			{
				redirect('form-html/setting');
			}
		}*/

		public function actionEdit()
		{
			$tabel = $this->input->post('tabel');
			$id    = $this->input->post('id');
			$pesan = '';

			if(!empty($tabel))
			{
				switch ($tabel)
				{
					case 'logo':
						if(!empty($id))
						{
							$this->upload_image($this->input->post('logo'));
							if($this->upload->do_upload('logo'))
							{
								$data = array(
									'image' => $this->upload->file_name,
									'tentang' => $this->input->post('tentang'),
								);


								$pesan = 'logo';
							}
							else
							{
								redirect('form-html/setting');
							}
						}
						# code...
						break;
					case 'contact':
						if(!empty($id))
						{
							$data = array(
								'address'   => $this->input->post('address'),
								'telephone' => $this->input->post('telephone'),
								'email'     => $this->input->post('email')
							);

							$pesan = 'contact';
						}
						# code...
						break;
					case 'social':
						if(!empty($id))
						{
							$data = array(
								'google_plus' => $this->input->post('google_plus'),
								'twitter'     => $this->input->post('twitter'),
								'facebook'    => $this->input->post('facebook'),
								'instagram'   => $this->input->post('instagram')
							);

							$pesan = 'social';
						}
						# code...
						break;
					default:
						redirect('form-html/setting');
						# code...
						break;
				}

				$this->setting->update($id, $data, $tabel);
				$this->session->set_flashdata('aktif', $pesan);
				redirect('form-html/setting');

			}
			else
			{
				redirect('form-html/setting');
			}
		}

		public function upload_image($image_name)
		{
			$directory = './images/uploads';
			if(!is_dir($directory))
			{
				mkdir($directory, 0777, TRUE);
				$dir_exist = false;
			}

			$config['upload_path'] 	 = $directory;
			$config['file_name'] 	 = url_title($image_name);
			$config['allowed_types'] = 'gif|jpg|png|jpeg'; //type yang dapat diupload
			$config['max_size'] 	 = '5048'; //maksimum besar file 2M
			$config['max_width']  	 = '5288'; //lebar maksimum 5288 px
			$config['max_height']  	 = '5288'; //tinggi maksimum 1588 px

			$this->load->library('upload', $config);
		}
	}
