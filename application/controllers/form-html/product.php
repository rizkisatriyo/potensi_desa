<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Product extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();

			//jika belum login redirect ke login lagi
			if ($this->session->userdata('logged') <> 1) {
				redirect(site_url('form-html/login'));
			}
			
			$this->load->model('product_model', 'product');
			$this->load->library('Resize');
			$this->load->helper('html');
		}

		public function index()
		{
			$this->showProduct();
		}

		public function showProduct()
		{
			$data = array('products' => $this->product->getAllProduct());

			$this->load->view('view-admin/layout/header');
			$this->load->view('view-admin/pages/product', $data);
			$this->load->view('view-admin/layout/footer');
		}

		//add product
		public function formAdd($pesan = '')
		{
			$data = array(
				'category'   => $this->product->getAllCategory(), 
				'pesan'      => $pesan,
			);
			
			$this->load->view('view-admin/layout/header');
			$this->load->view('view-admin/pages/add_product', $data);
			$this->load->view('view-admin/layout/footer');
		}

		public function actionAdd()
		{
			$this->setting_rules();

			$type		= substr(strtolower($_FILES['foto']['name']), -4);
			$img_name	= substr($this->input->post('item_name'), 0, 20);
			$img_name 	= str_replace(" ", "_", $img_name);
			$big_image 	= "images/uploads/".$img_name."".$type;
			$med_image 	= "images/uploads/".$img_name."_med".$type;
			$thumbnail 	= "images/uploads/".$img_name."_small".$type;

			copy($_FILES['foto']['tmp_name'], $big_image);
			// big
			$resizeObj = new Resize();
			$resizeObj->setImage($big_image);
			$resizeObj->resizeImage(600, 600, 'auto');
			$resizeObj->saveImage($big_image, 100);

			// medium
			$resizeObj = new Resize();
			$resizeObj->setImage($big_image);
			$resizeObj->resizeImage(220, 220, 'auto');
			$resizeObj->saveImage($med_image, 100);

			// thumbnail
			$resizeObj = new Resize();
			$resizeObj->setImage($big_image);
			$resizeObj->resizeImage(75, 75, 'auto');
			$resizeObj->saveImage($thumbnail, 100);
		

			//jalankan validasi
			if ($this->form_validation->run() == FALSE)
			{
				$this->formAdd();
			}
			else
			{
				$data = array(
					'cat_id'          => $this->input->post('id_category'),
					'item_name'       => $this->input->post('item_name'),
					'item_desc_short' => $this->input->post('shortdesc'),
					'big_image'       => $big_image, 
					'medium_image'    => $med_image, 
					'thumbnail'       => $thumbnail, 
				);

				$this->product->insert_product($data);
				$pesan = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data Berhasi Disimpan</div>';
			}
				
			// $this->formAdd($pesan);
			redirect('form-html/product', 'refresh');
		}
		// end add product

		// edit product
		public function formEdit($id_product = null, $pesan = null)
		{
			if(!empty($id_product))
			{
				$data = array(
					'category' => $this->product->getAllCategory(),
					'product'  => $this->product->get_product($id_product)->result(),
					'pesan'    => $pesan
				);

				if($this->product->get_product($id_product)->num_rows > 0)
				{
					$this->load->view('view-admin/layout/header');
					$this->load->view('view-admin/pages/edit_product', $data);
					$this->load->view('view-admin/layout/footer');
				}
				else
				{
					redirect('form-html/product');
				}
			}
			else
			{
				redirect('form-html/product');
			}
		}

		public function actionEdit()
		{
			$this->setting_rules();

			$id_product = $this->input->post('id_product');
			
			//jalankan validasi
			if ($this->form_validation->run() == FALSE)
			{
				$this->formEdit($id_product);
			}
			else
			{
				if(!empty($id_product))
				{
					$data = array(
						'cat_id'          => $this->input->post('id_category'),
						'item_name'       => $this->input->post('item_name'),
						'item_desc_short' => $this->input->post('shortdesc'),
					);

					$this->product->update_product($id_product, $data);
					$pesan = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data Berhasi Diubah, Tidak Ada Perubahan Foto</div>';

					if($_FILES['foto']['name'] != '')
					{
						$type		= substr(strtolower($_FILES['foto']['name']), -4);
						$img_name	= substr($this->input->post('item_name'), 0, 20);
						$img_name 	= str_replace(" ", "_", $img_name);
						$big_image 	= "images/uploads/".$img_name."".$type;
						$med_image 	= "images/uploads/".$img_name."_med".$type;
						$thumbnail 	= "images/uploads/".$img_name."_small".$type;

						copy($_FILES['foto']['tmp_name'], $big_image);
						// big
						$resizeObj = new Resize();
						$resizeObj->setImage($big_image);
						$resizeObj->resizeImage(600, 600, 'auto');
						$resizeObj->saveImage($big_image, 100);

						// medium
						$resizeObj = new Resize();
						$resizeObj->setImage($big_image);
						$resizeObj->resizeImage(220, 220, 'auto');
						$resizeObj->saveImage($med_image, 100);

						// thumbnail
						$resizeObj = new Resize();
						$resizeObj->setImage($big_image);
						$resizeObj->resizeImage(75, 75, 'auto');
						$resizeObj->saveImage($thumbnail, 100);

						$data2 = array(
							'big_image'       => $big_image, 
							'medium_image'    => $med_image, 
							'thumbnail'       => $thumbnail, 
						);

						$this->product->update_product($id_product, $data2);
						$pesan = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data Berhasi Diubah, Foto berhasil diubah</div>';					
					}

					// $this->formEdit($id_product, $pesan);
					redirect('form-html/product', 'refresh');
				}
				else
				{
					redirect('form-html/product', 'refresh');
				}
			}
		}
		// end edit product

		// delete product
		public function actionDelete($id_product = null)
		{
			if(!empty($id_product))
			{
				// hapus data
				$this->product->delete_product($id_product);
				redirect('form-html/product', 'refresh');
			}
			else
			{
				redirect('form-html/product', 'refresh');
			}
		}
		// end delete product

		public function setting_rules()
		{
			//set rules
			$this->form_validation->set_rules('item_name', 'product name', 'trim|required|min_length[3]|max_length[30]');
			$this->form_validation->set_rules('shortdesc', 'description', 'trim|required');
			
			$this->form_validation->set_message('required', '%s Tidak Boleh Kosong'); 
		}
	}
