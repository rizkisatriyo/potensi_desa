<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class customer extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();

			//jika belum login redirect ke login lagi
			if ($this->session->userdata('logged') <> 1) {
				redirect(site_url('form-html/login'));
			}

			$this->load->model('customer_model', 'customer');

		}

		public function index()
		{
			$this->showCustomers();
		}

	    public function showCustomers($pesan = null)
	    {
		    $data = array(
		        'customers' => $this->customer->getCustomerList(),
		        'pesan'     => $pesan,
		        'total'     => $this->customer->getCustomerCountt()
		    );

		    $this->load->view('view-admin/layout/header');
		    $this->load->view('view-admin/pages/customer', $data);
		    $this->load->view('view-admin/layout/footer');

	    }

	    public function actionDelete($id)
	    {
	    	if(!empty($id))
			{
				$data = array('status' => 0);
				$this->customer->update_customer($id, $data);

				$pesan = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data Berhasi Dihapus</div>';

				$this->showCustomers($pesan);
			}
			else
			{
				echo "gagal";
			}
	    }

		public function customerDetails($id = null)
		{
			if(!empty($id))
			{
				$data = array('customers' => $this->customer->get_customer($id)->result());

				if($this->customer->get_customer($id)->num_rows() > 0)
				{
					$this->load->view('view-admin/layout/header');
					$this->load->view('view-admin/pages/customer_details', $data);
					$this->load->view('view-admin/layout/footer');
				}
				else
				{
					redirect('form-html/customer');
				}
			}
			else
			{
				redirect('form-html/customer');
			}
		}
	}