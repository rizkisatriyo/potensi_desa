<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Category extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();

			//jika belum login redirect ke login lagi
			if ($this->session->userdata('logged') <> 1) {
				redirect(site_url('form-html/login'));
			}

			$this->load->model('category_model', 'category');
      @session_start();
		}

		public function index()
		{
			$this->showCategories();
		}


		public function showCategories()
		{
			$data = array('categories' => $this->category->getAllCategory());

			$this->load->view('view-admin/layout/header');
			$this->load->view('view-admin/pages/category', $data);
			$this->load->view('view-admin/layout/footer');
		}

		// Tambah Kategori
    function formAdd($pesan ='')
  	{
		$this->load->view('view-admin/layout/header');
		$this->load->view('view-admin/pages/add_category');
		$this->load->view('view-admin/layout/footer');
  	}

    function actionAdd()
    {

  				$this->form_validation->set_rules('cat_name', 'category name', 'trim|required|min_length[3]|max_length[30]');

  				if ($this->form_validation->run() == FALSE)
          {
              $this->add_category();
          }
  				else
          {
          $this->category->add_category($this->input->post());
          }


    }

		public function formEdit($cat_id = null, $pesan = null)
		{
			if(!empty($cat_id))
			{
				$data = array(
					'category' => $this->category->getCategoryDetails($cat_id),
					'pesan'    => $pesan
				);

					$this->load->view('view-admin/layout/header');
					$this->load->view('view-admin/pages/edit_category', $data);
					$this->load->view('view-admin/layout/footer');
			}
			else
			{
				redirect('form-html/category');
			}
		}

  	function actionEdit()
		{
			$this->form_validation->set_rules('cat_name', 'category name', 'trim|required|min_length[3]|max_length[30]');

			$cat_id = $this->input->post('cat_id');

			//jalankan validasi
			if ($this->form_validation->run() == FALSE)
			{
				$this->formEdit($cat_id);
			}
			else
			{
				if(!empty($cat_id))
				{
					$data = array(
						'cat_name'  => $this->input->post('cat_name'),
						'cat_desc'  => $this->input->post('cat_desc'),						
					);

					$this->category->update_category($cat_id, $data);
					$pesan = '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data Berhasi Diubah, Tidak Ada Perubahan Foto</div>';

					$this->formEdit($id_product, $pesan);
					// redirect('form-html/category', 'refresh');
					// echo $this->input->post('cat_id');
				}
				else
				{
					redirect('form-html/category', 'refresh');
				}
			}
		}


     function delete_cat($id_category){
        if(!empty($id_category))
        {
          // hapus data
          $this->category->delete_category($id_category);
          redirect('form-html/category', 'refresh');
        }
        else
        {
          redirect('form-html/category', 'refresh');
        }
    	}
  }
