<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Dashboard extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();

			//jika belum login redirect ke login lagi
			if ($this->session->userdata('logged') <> 1) {
				redirect(site_url('form-html/login'));
			}
		}

		public function index()
		{
			$this->showTemplate();
		}

		public function showTemplate($page = 'layout')
		{
			if(! file_exists(APPPATH.'/views/view-admin/'.$page))
			{
				// show_404();
				echo ":P";
			}
			else
			{
				$data = array('searchView' => $this->load->view('view-admin/layout/top_search', '', TRUE));
				$this->load->view('view-admin/layout/header', $data);
				$this->load->view('view-admin/layout/main');
				$this->load->view('view-admin/layout/footer');
			}
		}

		public function product()
		{
			$this->load->view('view-admin/layout/header');
			$this->load->view('view-admin/pages/product');
			$this->load->view('view-admin/layout/footer');
		}
	}
