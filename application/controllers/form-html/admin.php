<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Admin extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();

			//jika belum login redirect ke login lagih
			if ($this->session->userdata('logged') <> 1) {
				redirect(site_url('form-html/login'));
			}

			$this->load->model('admin_model', 'admin_m');
		}

		public function index()
		{
			redirect('form-html/dashboard');
		}

		public function settingAdmin()
  		{
  			$data = array('admins' => $this->admin_m->get_admin()->row());
  			if($this->admin_m->get_admin()->num_rows > 0)
			{
		  		$this->load->view('view-admin/layout/header');
		  		$this->load->view('view-admin/pages/setting_admin', $data);
		  		$this->load->view('view-admin/layout/footer');
		  	}
  		}

  		public function actionEdit()
  		{
  			$this->setting_rules();
  			if($this->form_validation->run())
  			{
				$username     = $this->input->post('username');
				$passbaru     = $this->input->post('passwordnew');
				$passbarulagi = $this->input->post('passwordconfirm');
				$passlama     = md5($this->input->post('passwordold'));

				if(!empty($username))
				{
					// insert username
					$data = array('username' => $username);
					$this->admin_m->aksi_edit($data);
				}

				if(!empty($passlama) && !empty($passbaru) && !empty($passbarulagi))
				{
					//cek pass lama
					$data_lama = $this->admin_m->get_password_old()->row_array();

					if($this->encrypt->decode($data_lama['password']) == $passlama)
					{
						// cek matches
						if($passbaru == $passbarulagi)
						{
							$data = array('password' => $this->encrypt->encode(md5($passbaru)));
							$this->admin_m->aksi_edit($data);

							//hapus session
							$this->session->unset_userdata('admin_id');
							$this->session->unset_userdata('username');
							$this->session->unset_userdata('logged');

							//login lagi
							$this->session->set_flashdata('msg', '<div class="alert alert-success fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Data Berhasi Diubah, Silakan Login Lagi</div>');

							redirect('form-html/login');
						}
						else
						{
							redirect('form-html/admin/settingAdmin');
						}
					}
					else
					{
						$this->session->set_flashdata('msg', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Password Lama Tidak Cocok</div>');
						redirect('form-html/admin/settingAdmin');
					}
				}
				else
				{
					$this->session->set_flashdata('msg', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Field Password Harus Diisi</div>');
					redirect('form-html/admin/settingAdmin');
				}
			}
			else
			{
				$this->settingAdmin();
			}
  		}

  		public function setting_rules()
		{
			//set rules
			$this->form_validation->set_rules("username", "Username Baru", "trim|required|min_length[5]|max_length[10]");
			$this->form_validation->set_rules("passwordnew", "Password Baru", "trim");
			$this->form_validation->set_rules("passwordconfirm", "Ulangi Password Baru", "trim|matches[passwordnew]'");
			$this->form_validation->set_rules("passwordold", "Konfirmasi Password Lama", "trim");

			//set pesan
			$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
			$this->form_validation->set_message('matches', '%s Tidak Sama');
			$this->form_validation->set_message('min_length', '%s Terlalu Pendek');
			$this->form_validation->set_message('max_length', '%s Terlalu Panjang');
		}
  	}
