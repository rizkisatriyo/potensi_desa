<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Pemberitahuan extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();

			//jika belum login redirect ke login lagi
			if ($this->session->userdata('logged') <> 1) {
				redirect(site_url('form-html/login'));
			}

			$this->load->model('pemberitahuan_model', 'model_p');
			$this->load->helper('url');
      @session_start();
		}

		public function index()
		{
			$this->showPemberitahuan();
		}


		public function showPemberitahuan()
		{
			$data['pembs'] = $this->model_p->get_pemberitahuan()->result();

			$this->load->view('view-admin/layout/header');
			$this->load->view('view-admin/pages/pemberitahuan', $data);
			$this->load->view('view-admin/layout/footer');
		}

		// Tambah Kategori
    function formAdd($pesan ='')
  	{
		$this->load->view('view-admin/layout/header');
		$this->load->view('view-admin/pages/add_pemberitahuan');
		$this->load->view('view-admin/layout/footer');
  	}

    function actionAdd(){

			$judul = $this->input->post('judul');
			$tanggal = $this->input->post('tanggal');
 
			$data = array(
				'judul' => $judul,
				'tanggal' => $tanggal,
				);
			$this->model_p->insert_pemberitahuan($data,'wg_pemberitahuan');
			redirect('form-html/pemberitahuan');
		}

		public function formEdit($pemberitahuan_id)
		{
			$where = array('pemberitahuan_id' => $pemberitahuan_id);
			
			$data['p'] = $this->model_p->edit_data($where,'wg_pemberitahuan')->result();
			$this->load->view('view-admin/layout/header');
			$this->load->view('view-admin/pages/edit_pemberitahuan',$data);
			$this->load->view('view-admin/layout/footer');

		}

  	function actionEdit(){
		$pemberitahuan_id = $this->input->post('pemberitahuan_id');
		$judul = $this->input->post('judul');
		$tanggal = $this->input->post('tanggal');

		$data = array(
			'judul' => $judul,
			'tanggal' => $tanggal,
		);

		$where = array(
			'pemberitahuan_id' => $pemberitahuan_id
		);

		$this->model_p->update_data($where,$data,'wg_pemberitahuan');
		redirect('form-html/pemberitahuan');
	}


     function actionDelete($pemberitahuan_id){
        $where = array('pemberitahuan_id' => $pemberitahuan_id);
		$this->model_p->delete($where,'wg_pemberitahuan');
		redirect('form-html/pemberitahuan');
  		}
  }
