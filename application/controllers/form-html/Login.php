<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Login extends CI_Controller
	{
		public function __construct()
		{
			parent::__construct();

			//load model
			$this->load->model('login_model', 'login');
		}

		public function index()
		{
			// jika sudah login
			if ($this->session->userdata('logged') == 1) {
				redirect(site_url('form-html/dashboard'));
			}

			$this->load->view('view-admin/login_v');
		}

		public function loginAction()
		{
			// echo $this->encrypt->encode(md5('admin'));
			
			// jika sudah login
			if ($this->session->userdata('logged') == 1) {
				redirect(site_url('form-html/dashboard'));
			}

			//set rules
			$this->form_validation->set_rules("yourusername", "Username", "trim|required");
			$this->form_validation->set_rules("yourpassword", "Password", "trim|required");
			$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');

			if ($this->form_validation->run() == FALSE)
			{
				redirect('form-html/login');
			}
			else
			{
				$data = array(
					'username' => $this->input->post('yourusername'),
					'password' => md5($this->input->post('yourpassword'))
				);

				$result = $this->login->ambil_data_admin($data['username'])->row_array();

				if($this->input->post('btnLogin'))
				{
					if($result && $this->encrypt->decode($result['password']) == $data['password'])
					{
						$session = array(
								'id_admin'  => $result['admin_id'],
								'username' 	=> $result['username'],
								'logged' 	=> TRUE,
						);

						$this->session->set_userdata($session);
						//redirect ke halaman sukses
						redirect(site_url('form-html/dashboard'));
					}
					else
					{
						$this->session->set_flashdata('msg', '<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>Email dan Password Salah</div>');

                		redirect('form-html/login');
					}
				}
				else
				{
                	redirect('form-html/login');
				}
			}
		}

		public function logout()
		{
			$this->session->unset_userdata('admin_id');
			$this->session->unset_userdata('username');
			$this->session->unset_userdata('logged');
			redirect(site_url('form-html/login'));
		}
	}
