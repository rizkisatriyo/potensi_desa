<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Front extends CI_Controller {
	function __construct()
	{
		parent::__construct();
 		$this->load->model('main_model');
		$this->load->model('front_model');
    	$this->load->model('email_model');
		$this->load->model('cart_model');
		$this->load->library('form_validation');
		$this->load->model('social_media');
		$this->load->model('contact_us');
		$this->load->model('categories_home');
		$this->load->model('banner_front');
		$this->load->model('setting_model');
		$this->load->model('about_model', 'about_m');
		$this->load->model('pemberitahuan_model', 'model_p');

		// add rori
		$this->load->model('category_model');
		$this->load->model('blog_model');
		$this->load->helper(array('category_product_helper', 'html'));
		// end add rori
		$this->load->helper('html');


		session_start();
		@$cart_session= session_id();
		if($this->session->userdata('cart_session')=='')	$this->session->set_userdata('cart_session',$cart_session);

	}

	public function index()
	{
		// $data['category'] =
		// end add rori

		/*$data['last_categories']          = $this->categories_home->getLastCategoryProducts($this->uri->segment(2));
		$data['lastest_categories']       = $this->categories_home->getLastestCategoryProducts($this->uri->segment(2));
		$data['categories']               = $this->main_model			->getAllCategories();
		$data['category_products'] 	   		= $this->main_model			->getCategoryProducts($this->uri->segment(2));
		$data['cart_products']						= $this->cart_model			->getCartProducts();
		$data['home_products'] 						= $this->main_model			->getLatestProducts();
		$data['last_product'] 						= $this->main_model			->getLastProducts();
		$data['social_media']  						= $this->social_media		->selectAll();
		$data['select_contact'] 				  = $this->contact_us			->selectContacts();
		$data['banner'] = $this->banner_front->selectAllBanner();*/

		//edit by rori
		$data = array(
			'category'           => $this->category_model->getAllCategory(),
			'last_categories'    => $this->categories_home->getLastCategoryProducts($this->uri->segment(2)),
			'lastest_categories' => $this->categories_home->getLastestCategoryProducts($this->uri->segment(2)),
			'category_products'  => $this->main_model->getCategoryProducts($this->uri->segment(2)),
			'home_blog'      	 => $this->main_model->getLastestNews(),
			'last_product'       => $this->main_model->getLastProducts(),
			'social_media'       => $this->social_media->selectAll(),
			'select_contact'     => $this->contact_us->selectContacts(),
			'banner'             => $this->banner_front->selectAllBannerFront(),
			'banner_single'      => $this->banner_front->selectAllBannerSingle(),
			'blog'				 => $this->blog_model->get_all_blog(),
			'pemberitahuan'		 => $this->model_p->getAllPemberitahuan(),
			'logo'  			 => $this->setting_model->get_all_logo(),


		);
		//edit by rori

		$this->load->view("home", $data);
	}

	public function potensi()
	{
		$data['category']      = $this->category_model->getAllCategory();
		$data['social_media']  = $this->social_media->selectAll();
		$data['banner_product'] = $this->banner_front->getBannerProduct();

		$data['categories']    = $this->main_model->getAllCategories();
		$data['home_products'] = $this->main_model->getLatestProducts();
		$data['home_products1'] = $this->main_model->getLatestProducts1();
		$data['home_products2'] = $this->main_model->getLatestProducts2();
		$data['select_contact']  = $this->contact_us->selectContacts();
		$this->load->view("pages/potensi_desa",$data);

	}

	public function category()
	{
		$data['category']      		= $this->category_model->getAllCategory();
		$data['social_media']  		= $this->social_media		->selectAll();
		$data['categories']         = $this->main_model->getAllCategories();
		$data['category_products'] 	= $this->main_model->getCategoryProducts($this->uri->segment(2));
		$data['select_contact']     = $this->contact_us->selectContacts();
		$this->load->view("pages/potensi_kategori",$data);

	}


	function signout()
	{

		$sess_array = array(
				'user_name'			  =>	'',
				'email'				    =>	'',
				'user_id'			    =>	'',
				'account_type'    =>	'',
				'company_name'    =>	'',
        'cart_items_count'=>	'0',
        'total_price'     =>	'0',
				);

				$this->session->set_userdata($sess_array);
				$loc = base_url()."index.php";
				header("Location:$loc");
		}

	function details()
	{

		$pid 				= 	$this->uri->segment(2);
		$data['categories'] = 	$this->main_model->getAllCategories();
		$data['social_media']  = $this->social_media->selectAll();
		$data['product']	= 	$this->front_model->getProductDetails($pid);
		$data['select_contact']  = $this->contact_us->selectContacts();
		$this->load->view('pages/product_detail',$data);

	}

	function cart()
	{
		$data['social_media']  = $this->social_media->selectAll();
		$data['select_contact']  = $this->contact_us->selectContacts();
		$data['categories'] 	= 	$this->main_model->getAllCategories();
		$data['cart_products']	= 	$this->cart_model->getCartProducts();
		$this->load->view('cart',$data);

	}

	function buy()
	{

			$pid = $this->uri->segment(2);
			$this->cart_model->add2cart($pid);

                        $total_products_cart 	= $this->cart_model->getTotalCartProducts();
			$total_price 		 	= $this->cart_model->getTotalCartPrice();

			$this->session->set_userdata('cart_items_count',$total_products_cart);
			$this->session->set_userdata('total_price',$total_price);
			//header("Location:".$_SERVER['HTTP_REFERER']);
			redirect('front');
	}

	function update_cart()
	{

		if($this->input->post('update_action')!='')
		{
			$this->cart_model->update_cart($this->input->post());

			$total_products_cart 	 = $this->cart_model->getTotalCartProducts();
			$total_price 		 = $this->cart_model->getTotalCartPrice();
			$this->session->set_userdata('cart_items_count',$total_products_cart);
			$this->session->set_userdata('total_price',$total_price);


			redirect('front/cart');
			exit();
		}

	}


	function checkout()
	{
		$this->cart_model->saveCart();
	}



	function register()
	{

		if($this->session->userdata('user_id')!='')
			{
				redirect('front/account');
			}

		else if($this->session->userdata('user_id')=='' && $this->input->post('register_action')!='true')
		{

					$data['categories'] 	= 	$this->main_model->getAllCategories();
					$this->load->view('register',$data);

		}

		else if ($this->input->post('register_action')=='true')
		{

					$this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
					$this->form_validation->set_rules('account_type', 'Account Type', 'trim|required');

					if($this->input->post('account_type')=='Trade')
					$this->form_validation->set_rules('company_name', 'Company Name', 'trim|required');
					$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
					$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[12]|matches[password2]');
					$this->form_validation->set_rules('password2', 'Confirm Password', 'trim|required|min_length[6]|max_length[12]');

						if ($this->form_validation->run() == FALSE)
						{
								$data['categories'] 	= 	$this->main_model->getAllCategories();
								$this->load->view('pages/login',$data);
						}
						else
						{

							$this->main_model->register($this->input->post());

						}

		}


	}

	function login()
	{
		$data['social_media']  = $this->social_media->selectAll();
		$data['select_contact']  = $this->contact_us->selectContacts();
    if($this->session->userdata('user_id')=='' && $this->input->post('login_action')!='true')
		{
			$data['categories'] 	= 	$this->main_model->getAllCategories();
			$this->load->view('pages/login',$data);
		}
		else if ($this->input->post('login_action')=='true')
		{
        	$this->form_validation->set_rules('username', 'Username/Email', 'trim|required|valid_email');
					$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[12]|xss_clean');

						if ($this->form_validation->run() == FALSE)
						{
								$data['categories'] 	= 	$this->main_model->getAllCategories();
								$this->load->view('pages/login',$data);
						}
						else
						{
							$this->main_model->check_login($this->input->post());

						}

		}
		else
		{

			$data['categories'] 	= 	$this->main_model->getAllCategories();
			$this->load->view('pages/login',$data);

		}


	}

	function order_step2()
	{
		$data['social_media']  = $this->social_media->selectAll();
		$data['select_contact']  = $this->contact_us->selectContacts();
		if($this->session->userdata('user_id')=='')
		{
			redirect('front/login');
		}
		if($this->session->userdata('total_price') == '')
		{
			redirect('front');
		}
		else if($this->input->post('checkout_action')=='')
		{
			$data['categories'] 	= 	$this->main_model->getAllCategories();
			$data['products']		=	$this->cart_model->getCheckoutDetails();
			$this->load->view('checkout',$data);
		}
		else if($this->input->post('checkout_action')=='true')
		{
			$this->form_validation->set_rules('address1', 'Address 1', 'trim|required');
			$this->form_validation->set_rules('address2', 'Address 2', 'trim|required');
			$this->form_validation->set_rules('city', 'City', 'trim|required');
			$this->form_validation->set_rules('county', 'County', 'trim|required');
			$this->form_validation->set_rules('post_code', 'Post Code', 'trim|required');


			if ($this->form_validation->run() == FALSE)
			{
				$data['categories'] 	= 	$this->main_model->getAllCategories();
				$data['products']		=	$this->cart_model->getCheckoutDetails();
				$this->load->view('checkout',$data);
			}
			else
			{
				$order_id = $this->cart_model->save_order($this->input->post());
				$this->cart_model->save_address($this->input->post());
				
				redirect('front/confirm');
			}
		}
	}



	function confirm()
	{
		$data['social_media']  = $this->social_media->selectAll();
		$data['select_contact']  = $this->contact_us->selectContacts();
		if($this->session->userdata('order_id')!='')
		{
			$data['categories'] 	= 	$this->main_model->getAllCategories();
                        $this->email_model->order_confirmation();
			$this->load->view('order_confirmation',$data);

		}
		else
		{
			redirect('front');
		}

	}

	function visiMisi()
	{
		$data  = array(
			'blog' 	         => $this->blog_model->get_all_blog(),
			'abouts' 		 => $this->about_m->getAbout()->row(),
			'select_contact' => $this->contact_us->selectContacts(),
			'category'       => $this->category_model->getAllCategory(),
		);

		$this->load->view('pages/visi_misi',$data);
	}
		
	function profil()
	{
		$data  = array(
			'blog' 	         => $this->blog_model->get_all_blog(),
			'profil' 		 => $this->about_m->getProfil()->row(),
			'select_contact' => $this->contact_us->selectContacts(),
			'category'       => $this->category_model->getAllCategory(),
		);

		$this->load->view('pages/profil',$data);
	}

	function blog()
	{
		$data['blog']	        = $this->blog_model->get_all_blog();
		$data['select_contact'] = $this->contact_us->selectContacts();
		$data['categories']       = $this->category_model->getAllCategory();

		$this->load->view('pages/blog',$data);

	}
	function blogDetails($blog_id)
	{
		$id 				= 	$this->uri->segment(2);
		$data = array (

			'blog'	        		 => $this->blog_model->get_all_blog(),
			'blog_data'	        	 => $this->blog_model->getBlogDetails($blog_id),
			'select_contact'    	 => $this->contact_us->selectContacts(),
			'logo'  			  	 => $this->setting_model->get_all_logo(),
			'categories'       => $this->category_model->getAllCategory(),


		);
		$this->load->view('pages/blog_detail',$data);

	}
//////////// ********** ABOVE THIS ****************/
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
