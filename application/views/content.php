		
				<!-- HEADER BG : begin -->
		<div class="header-bg">

			<!-- HEADER IMAGE : begin -->
			<!-- To add more images just copy and edit elements with "image-layer" class (see home-2.html template for example)
			Change autoplay speed with "data-autoplay" attribute (in seconds), works only if there are more than one image -->
			<div class="header-image" data-autoplay="8">
				<div class="image-layer" style="background-image: url( '<?= base_url().'assets/template/frontend/' ?>images/background.jpg' );"></div>
				<!-- div class="image-layer" style="background-image: url( 'images/header-02.jpg' );"></div -->
			</div>
			<!-- HEADER IMAGE : begin -->

		</div>
		<!-- HEADER BG : end -->

		<!-- CORE : begin -->
		<div id="core">
			<div class="c-container">
				<div class="row">

					<!-- MIDDLE COLUMN : begin -->
					<div class="middle-column col-md-6 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1>Potensi Desa Banyusari<br><em>Kecamatan Malausma</em></h1>
							</div>
							<!-- PAGE TITLE : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">

								<!-- DIRECTORY : begin -->
								<!-- You can choose to have 2, 3 or 4 columns in Directory element.
								To change the number of columns, change the class in the following element from m-3-columns to m-2-columns or m-4-columns -->
								
								<!-- DIRECTORY : end -->

								<!-- POST LIST : begin -->
								<div class="c-post-list">
									<div class="c-content-box">
										<div class="post-list-inner">
											<i class="ico-shadow tp tp-reading"></i>
											<h2 class="post-list-title"><i class="fa fa-newspaper-o" aria-hidden="true"></i><a href="<?php echo site_url('front/blog'); ?>">  Berita <strong>Terbaru</strong></a></h2>
											<div class="post-list-content">



												<!-- FEATURED POST : begin -->
												<?php
													foreach ($home_blog as $key => $blog_data) 
													{
												?>
												<article class="featured-post m-has-thumb">
													<div id="frame-image" class="post-image">
														<a href="#"><?php
											                  	$image = array(
											                    	'src'    => 'images/uploads/'.$blog_data->blog_image,
											                      	'alt'    => 'Foto Blog',
											                    );

											                  echo img($image);
											                ?>
											            </a>
													</div>
													<div class="post-core">
														<h3 class="post-title">
															<a href="<?php echo base_url();?>index.php/front/blogDetails/<?php echo $blog_data->blog_id; ?>/<?php echo $blog_data->blog_title; ?>"><?php echo $blog_data->blog_title; ?></a>
														</h3>
														<div class="post-date"><i class="fa fa-clock-o" aria-hidden="true"></i>
															<?php
																$bulan   = date("M", strtotime($blog_data->blog_date));
																$tanggal = date("d", strtotime($blog_data->blog_date));
																$tahun   = date("Y", strtotime($blog_data->blog_date));
															?>
															<?php echo $bulan.". ".$tanggal." ".$tahun; ?>
													</div>
														<div class="post-excerpt">
															<p><?php echo $blog_data->blog_desc; ?></p>
														</div>
													</div>
												</article>
												<?php } ?>
												<!-- FEATURED POST : end -->

												<!-- POST : begin -->
												<?php
													foreach ($blog as $key => $blog_data) 
													{
												?>
												<article class="post">
													<h3 class="post-title">
														<a href="<?php echo base_url();?>index.php/front/blogDetails/<?php echo $blog_data->blog_id; ?>/<?php echo $blog_data->blog_title; ?>"><?php echo $blog_data->blog_title; ?></a>
													</h3>
													<div class="post-date">
														<?php
																$bulan   = date("M", strtotime($blog_data->blog_date));
																$tanggal = date("d", strtotime($blog_data->blog_date));
																$tahun   = date("Y", strtotime($blog_data->blog_date));
														?>
														<?php echo $bulan.". ".$tanggal." ".$tahun; ?></div>
												</article>
												<?php } ?>
												<!-- POST : end -->

												<p class="more-btn-holder"><a href="<?php echo site_url('front/blog'); ?>">Baca Semua Berita</a></p>

											</div>
										</div>
									</div>
								</div>
								<!-- POST LIST : end -->

							</div>
						</div>
						<!-- PAGE CONTENT : end -->

						<hr class="c-separator m-margin-top-small m-margin-bottom-small m-transparent hidden-lg hidden-md">

					</div>
					<!-- MIDDLE COLUMN : end -->

					<!-- LEFT COLUMN : begin -->
					<div class="left-column col-md-3 col-md-pull-6">

						<!-- SIDE MENU : begin -->
						<?php include 'menu.php'; ?>
						<!-- SIDE MENU : end -->

						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<!-- DOCUMENTS WIDGET : begin -->
								
								<!-- DOCUMENTS WIDGET : end -->

								<!-- LOCALE INFO WIDGET : begin -->
								<!-- to remove background image from this widget, simply remove "m-has-bg" class from the following element -->
								<div class="widget locale-info-widget m-has-bg">
									<div class="widget-inner">
										<h3 class="widget-title"><i class="fa fa-map-marker" aria-hidden="true"></i> Info Lokasi</h3>
										<div class="widget-content">
											<ul>
												<li>
													<div class="row-title"><h4>Negara</h4></div>
													<div class="row-value">Indonesia</div>
												</li>
												<li>
													<div class="row-title"><h4>Provinsi</h4></div>
													<div class="row-value">Jawa Barat</div>
												</li>
												<li>
													<div class="row-title"><h4>Kabupaten</h4></div>
													<div class="row-value">Majalengka</div>
												</li>
												<li>
													<div class="row-title"><h4>Area</h4></div>
													<div class="row-value">72.7 sq mi (188.4 km<sup>2</sup>)</div>
												</li>
												<li>
													<div class="row-title"><h4>Populasi</h4></div>
													<div class="row-value">4,314</div>
												</li>
												<li>
													<div class="row-title"><h4>Kordinat</h4></div>
													<div class="row-value">44°28′31″N<br>72°42′8″W</div>
												</li>
												<li>
													<div class="row-title"><h4>Zona Waktu</h4></div>
													<div class="row-value">Waktu Indonesia Barat (WIB) (UTC+7)</div>
												</li>
												<li>
													<div class="row-title"><h4>Kode Pos</h4></div>
													<div class="row-value">45464</div>
												</li>
											</ul>
            							</div>
									</div>
								</div>
								<!-- LOCALE INFO WIDGET : end -->


							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

					<!-- RIGHT COLUMN : begin -->
					<div class="right-column col-md-3">

						<!-- RIGHT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<!-- NOTICES WIDGET : begin -->
								<div class="widget notices-widget">
									<div class="widget-inner">
										<h3 class="widget-title"><i class="fa fa-bullhorn" aria-hidden="true"></i>  Pemberitahuan Desa</h3>
										<div class="widget-content">
											<?php if($pemberitahuan=='empty')
												echo " Sorry - No category found";
												else
												{

											?>
											<ul class="notice-list">
												<?php foreach($pemberitahuan as $r){?>
												<!-- NOTICE : begin -->
												<li class="notice">
													<div class="notice-inner">
														<h4 class="notice-title"><a href=""><?php echo $r->judul; ?></a></h4>
														<span class="notice-date"><?php echo $r->tanggal; ?></span>
													</div>
												</li>
												<!-- NOTICE : end -->
												<?php } ?>
											</ul>
											<?php } ?>

										</div>
									</div>
								</div>
								<!-- NOTICES WIDGET : end -->

																<?php include("widget/widget_potensi.php"); ?>


							</div>
						</aside>
						<!-- RIGHT SIDEBAR : end -->

					</div>
					<!-- RIGHT COLUMN : end -->

				</div>
			</div>
		</div>
		<!-- CORE : end -->

