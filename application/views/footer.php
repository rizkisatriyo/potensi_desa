

		<!-- FOOTER : begin -->
		<?php foreach ($select_contact as $sc): ?>
		<footer id="footer" class="m-has-bg">
			<div class="footer-bg">
				<div class="footer-inner">

					<!-- FOOTER TOP : begin -->
					<div class="footer-top">
						<div class="c-container">

							<!-- BOTTOM PANEL : begin -->
							<div id="bottom-panel">
								<div class="bottom-panel-inner">
									<div class="row">
										<div class="col-md-4">

											<!-- TEXT WIDGET : begin -->
											<div class="widget">
												<hr class="c-separator m-transparent hidden-lg hidden-md">
												<div class="widget-inner">
													<h3 class="widget-title">Tentang Desa</h3>
													<div class="widget-content">
														<p>
															Dilihat dari topologi dan kantur tanah desa Banyusari Kecamatan Malausma secara umum berupa pegunungan dan bukit yang berada di pegunungan dan bukit yang berada pada ketinggia antara 900 s/d 1.000 m diatas permukaan laut dengan suhu rata-rata 25-30 0 Celcius. Potensi Desa Banyusari cukup besar baik yang sudah dimanpaatkan maupun yang belum dimanpaatkan secara maksimal. 
														</p>
													</div>
												</div>
											</div>
											<!-- TEXT WIDGET : end -->

										</div>
										<div class="widget-col col-md-5">

											<!-- DEFINITION LIST WIDGET : begin -->
											<div class="widget definition-list-widget">
												<hr class="c-separator m-transparent hidden-lg hidden-md">
												<div class="widget-inner">
													<h3 class="widget-title"><i class="fa fa-phone" aria-hidden="true"></i>  Nomor Telepon</h3>
													<div class="widget-content">
														<dl>
															<dt>Telephone</dt>
															<dd><?php echo $sc->telephone; ?></dd>
															<dt>Alamat Email</dt>
															<dd><?php echo $sc->email; ?></dd>
														</dl>
													</div>
												</div>
											</div>
											<!-- DEFINITION LIST WIDGET : end -->

										</div>
										
										<div class="widget-col col-md-3">
											
											<!-- TEXT WIDGET : begin -->
											<div class="widget">
												<hr class="c-separator m-transparent hidden-lg hidden-md">
												<div class="widget-inner">
													<h3 class="widget-title"><i class="fa fa-map-marker" aria-hidden="true"></i> Alamat Balai Desa </h3>
													<div class="widget-content">
														<p><?php echo $sc->address; ?>
													</div>
												</div>
											</div>
											<!-- TEXT WIDGET : end -->
											

										</div>
									</div>
								</div>
							</div>
							<!-- BOTTOM PANEL : end -->

						</div>
					</div>
				<?php endforeach ?>
					<!-- FOOTER TOP : end -->

					<!-- FOOTER BOTTOM : begin -->
					<div class="footer-bottom">
						<div class="footer-bottom-inner">
							<div class="c-container">

								<!-- FOOTER SOCIAL : begin -->
								<!-- Here is the list of some popular social networks. There are more icons you can use, just refer to the documentation -->
								<div class="footer-social">
									<?php foreach ($social_media as $sm): ?>
									<ul class="c-social-icons">
										<li class="ico-twitter"><a href="<?php echo $sm->twitter; ?>"><i class="fa fa-twitter"></i></a></li>
										<li class="ico-facebook"><a href="<?php echo $sm->facebook; ?>"><i class="fa fa-facebook"></i></a></li>
										<li class="ico-instagram"><a href="<?php echo $sm->instagram; ?>"><i class="fa fa-instagram"></i></a></li>
										<!--li><a href="#"><i class="fa fa-envelope"></i></a></li-->
										<!--li class="ico-angellist"><a href="#"><i class="fa fa-angellist"></i></a></li-->
										<!--li class="ico-behance"><a href="#"><i class="fa fa-behance"></i></a></li-->
										<!--li class="ico-bitbucket"><a href="#"><i class="fa fa-bitbucket"></i></a></li-->
										<!--li class="ico-bitcoin"><a href="#"><i class="fa fa-bitcoin"></i></a></li-->
										<!--li class="ico-codepen"><a href="#"><i class="fa fa-codepen"></i></a></li-->
										<!--li class="ico-delicious"><a href="#"><i class="fa fa-delicious"></i></a></li-->
										<!--li class="ico-deviantart"><a href="#"><i class="fa fa-deviantart"></i></a></li-->
										<!--li class="ico-digg"><a href="#"><i class="fa fa-digg"></i></a></li-->
										<!--li class="ico-dribbble"><a href="#"><i class="fa fa-dribbble"></i></a></li-->
										<!--li class="ico-dropbox"><a href="#"><i class="fa fa-dropbox"></i></a></li-->
										<!--li class="ico-flickr"><a href="#"><i class="fa fa-flickr"></i></a></li-->
										<!--li class="ico-foursquare"><a href="#"><i class="fa fa-foursquare"></i></a></li-->
										<!--li class="ico-git"><a href="#"><i class="fa fa-git"></i></a></li-->
										<!--li class="ico-github"><a href="#"><i class="fa fa-github"></i></a></li-->
										<!--li class="ico-lastfm"><a href="#"><i class="fa fa-lastfm"></i></a></li-->
										<!--li class="ico-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li-->
										<!--li class="ico-paypal"><a href="#"><i class="fa fa-paypal"></i></a></li-->
										<!--li class="ico-pinterest"><a href="#"><i class="fa fa-pinterest"></i></a></li-->
										<!--li class="ico-reddit"><a href="#"><i class="fa fa-reddit"></i></a></li-->
										<!--li class="ico-skype"><a href="#"><i class="fa fa-skype"></i></a></li-->
										<!--li class="ico-soundcloud"><a href="#"><i class="fa fa-soundcloud"></i></a></li-->
										<!--li class="ico-spotify"><a href="#"><i class="fa fa-spotify"></i></a></li-->
										<!--li class="ico-steam"><a href="#"><i class="fa fa-steam"></i></a></li-->
										<!--li class="ico-trello"><a href="#"><i class="fa fa-trello"></i></a></li-->
										<!--li class="ico-tumblr"><a href="#"><i class="fa fa-tumblr"></i></a></li-->
										<!--li class="ico-twitch"><a href="#"><i class="fa fa-twitch"></i></a></li-->
										<!--li class="ico-vimeo"><a href="#"><i class="fa fa-vimeo"></i></a></li-->
										<!--li class="ico-vine"><a href="#"><i class="fa fa-vine"></i></a></li-->
										<!--li class="ico-vk"><a href="#"><i class="fa fa-vk"></i></a></li-->
										<!--li class="ico-wordpress"><a href="#"><i class="fa fa-wordpress"></i></a></li-->
										<!--li class="ico-xing"><a href="#"><i class="fa fa-xing"></i></a></li-->
										<!--li class="ico-yahoo"><a href="#"><i class="fa fa-yahoo"></i></a></li-->
										<!--li class="ico-yelp"><a href="#"><i class="fa fa-yelp"></i></a></li-->
										<!--li class="ico-youtube"><a href="#"><i class="fa fa-youtube"></i></a></li-->
									</ul>
																	<?php endforeach ?>

								</div>
								<!-- FOOTER SOCIAL : end -->

								<!-- FOOTER MENU : begin -->

								<!-- FOOTER MENU : end -->

								<!-- FOOTER TEXT : begin -->
								<div class="footer-text">
									<p>Copyright <a href="http://themeforest.net/user/LSVRthemes/portfolio">Lespi Suryanti</a></p>
								</div>
								<!-- FOOTER TEXT : end -->

							</div>
						</div>
					</div>
					<!-- FOOTER BOTTOM : end -->

				</div>
			</div>
		</footer>
		<!-- FOOTER : end -->
		<!-- SCRIPTS : begin -->
		<script href="<?= base_url().'assets/template/frontend/' ?>library/fonts/font-awesome.min" rel="stylesheet" type="text/css"></script>
		<script src="<?= base_url().'assets/' ?>template/frontend/library/js/jquery-1.9.1.min.js" type="text/javascript"></script>
		<script src="<?= base_url().'assets/template/frontend/' ?>library/js/third-party.js" type="text/javascript"></script>
 		<script src="<?= base_url().'assets/template/frontend/' ?>library/js/library.js" type="text/javascript"></script>
 		<script src="<?= base_url().'assets/template/frontend/' ?>library/js/scripts.js" type="text/javascript"></script>
		<!-- SCRIPTS : end -->

	</body>

<!-- Mirrored from demos.lsvr.sk/townpress.html/demo/ by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 11 Apr 2017 13:43:55 GMT -->
</html>