<!DOCTYPE html>
<html>
	
<!-- Mirrored from demos.lsvr.sk/townpress.html/demo/ by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 11 Apr 2017 13:42:30 GMT -->
<head>

		<meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Potensi Desa</title>
        <link rel="shortcut icon" href="<?= base_url().'assets/template/frontend/' ?>images/header-logo2.png">

		<!-- GOOGLE FONTS : begin -->
		<script href="<?= base_url().'assets/template/frontend/' ?>library/fonts/font-awesome.min" rel="stylesheet" type="text/css"></script>
		<link href="<?= base_url().'assets/template/frontend/' ?>library/fonts/fontawesome-webfont.woff" rel="stylesheet" type="text/css">
		<!-- GOOGLE FONTS : end -->

        <!-- STYLESHEETS : begin -->
		<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/template/frontend/' ?>library/css/style.css">
		<!-- To change to a different pre-defined color scheme, change "red.css" in the following element to blue.css | bluegray.css | green.css | orange.css
		Please refer to the documentation to learn how to create your own color schemes -->
        <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/template/frontend/' ?>library/css/skin/red.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/template/frontend/' ?>library/css/custom.css">
		<!--[if lte IE 9]>
		<link rel="stylesheet" type="text/css" href="library/css/oldie.css">
		<![endif]-->
		<!-- STYLESHEETS : end -->

		<style>
        #frame-image {
            /* 
            CSS Untuk croping image 
            
            Disini Anda dapat menentukan lebar dan tinggi image
            */
            width: 1200px;
            height: 300px;
            overflow: hidden;
            
            /* 
            CSS Untuk mengatur posisi image 
            Menjadikan frame sebagai patokan koordinat left/top
            */
            position: relative;
        }
        
        #frame-image img {
            /*
            CSS Untuk croping image
            Membatasi tinggi image, membiarkan width-nya auto
            Bisa juga diganti jadi height: 320px kalau tinggi image mau dipaksa jadi 320px
            */
            max-height: 850px;
            
            /* 
            CSS Untuk mengatur posisi image 
            Left/top berpatokan pada frame
            Diisi dengan nilai minus untuk memposisikan keluar dari frame
            karena frame overflow-nya dibuat jadi hidden
            posisi yg keluar area frame jadi tidak terlihat
            
            Disini Anda dapat mengatur bagian/posisi gambar yg mana yg akan ditampilkan
            */
            position: absolute;
            top: -50px;
            left: : 0px;
        }
    </style>

	</head>
	<header id="header" class="m-has-header-tools m-has-gmap">
			<div class="header-inner">

				<!-- HEADER CONTENT : begin -->
				<div class="header-content">
					<div class="c-container">
						<div class="header-content-inner">

							<!-- HEADER BRANDING : begin -->
							<!-- Logo dimensions can be changed in library/css/custom.css
							You can remove "m-large-logo" class from following element to use standard (smaller) version of logo -->
							<div class="header-branding m-large-logo">
								<a href=""><span>
									<img src="<?= base_url().'assets/template/frontend/' ?>images/header-logo.png"
										data-hires="<?= base_url().'assets/template/frontend/' ?>images/header-logo.2x.png"
										alt="TownPress - Municipality HTML Template">
								</span></a>
							</div>
							<!-- HEADER BRANDING : end -->

							<!-- HEADER TOGGLE HOLDER : begin -->
							<div class="header-toggle-holder">

								<!-- HEADER TOGGLE : begin -->
								<button type="button" class="header-toggle">
									<i class="fa fa-align-justify" aria-hidden="true"></i>
									<span>Menu</span>
								</button>
								<!-- HEADER TOGGLE : end -->

								<!-- HEADER GMAP SWITCHER : begin -->
								<button type="button" class="header-gmap-switcher" title="Show on Map">
									<i class="fa fa-map-o" aria-hidden="true"></i>
								</button>
								<!-- HEADER GMAP SWITCHER : end -->

							</div>
							<!-- HEADER TOGGLE HOLDER : end -->

							<!-- HEADER MENU : begin -->
							<!-- This menu is used as both mobile menu (displayed on devices with screen width < 991px)
							and standard header menu (only if Header element has "m-has-standard-menu" class) -->
							<nav class="header-menu">
								<ul>
									<li><a href="index-2.html">Home</a>
										
									</li>
									<li><a href="town-hall.html">Profil Desa</a>
										<ul>
											<li><a href="town-hall.html">Potensi Desa</a></li>
											<li><a href="town-council.html">Peta Desa Banyusari</a></li>
											<li><a href="phone-numbers.html">Profil Aparatur</a></li>
											<li><a href="document-list.html">Visi Misi</a></li>
									
										</ul>
									</li>
									<li><a href="">Redaksi</a>
										<ul>
											<li><a href="post-list.html">News</a></li>
											<li><a href="notice-list.html">Notices</a></li>
											<li><a href="event-list.html">Events</a></li>
											<li><a href="gallery-list.html">Galleries</a></li>
										</ul>
									</li>
									<li><a href="statistics.html">Kontak Kami</a>
										<ul>
											<li><a href="statistics.html">Statistics</a></li>
											<li><a href="virtual-tour.html">Virtual Tour</a></li>
											<li><a href="town-history.html">Town History</a></li>
											<li><a href="elements.html">Elements</a></li>
										</ul>
									</li>
								</ul>
							</nav>
							<!-- HEADER MENU : end -->

							<!-- HEADER TOOLS : begin -->
							<div class="header-tools">

								<!-- HEADER SEARCH : begin -->
								<!-- <div class="header-search">
									<form method="get" action="http://demos.lsvr.sk/townpress.html/demo/search-results.html" class="c-search-form">
										<div class="form-fields">
											<input type="text" value="" placeholder="Search this site..." name="s">
											<button type="submit" class="submit-btn"><i class="fa fa-search" aria-hidden="true"></i></button>
										</div>
									</form>
								</div> -->
								<!-- HEADER SEARCH : end -->

								<!-- HEADER GMAP SWITCHER : begin -->
								<!-- Remove following block if you are not using Google Map in this template -->
								<button type="button" class="header-gmap-switcher" title="Show on Map">
									<i class="fa fa-map-o" aria-hidden="true"></i>
									<span>Map</span>
								</button>
								<!-- HEADER GMAP SWITCHER : end -->

							</div>
							<!-- HEADER TOOLS : end -->

						</div>
					</div>
				</div>
				<!-- HEADER CONTENT : end -->

				<!-- HEADER GMAP : begin -->
				<!-- Add your address into "data-address" attribute
				Change zoom level with "data-zoom" attribute, bigger number = bigger zoom
				Change map type with "data-maptype" attribute, values: roadmap | terrain | hybrid | satellite
				API KEY IS REQUIRED! More info https://developers.google.com/maps/documentation/javascript/get-api-key -->
				<div class="header-gmap">
					<div class="gmap-canvas"
						data-google-api-key="AIzaSyCbNLiFwr1Nt1-6iirrN2xkP21rbxzHLh0"
						data-address="Banyusari, Malausma, Kabupaten Majalengka, Jawa Barat"
						data-zoom="17"
						data-maptype="hybrid"
						data-enable-mousewheel="true">
					</div>
				</div>
				<!-- HEADER GMAP : end -->

			</div>
		</header>
		<!-- HEADER : end -->