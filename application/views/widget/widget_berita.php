<!-- POSTS WIDGET : begin -->
								<div class="widget posts-widget">
									<div class="widget-inner">
										<h3 class="widget-title"><i class="fa fa-book" aria-hidden="true"></i>  Berita Terbaru</h3>
										<div class="widget-content">
											<ul class="post-list">

												<!-- POST : begin -->
												<?php
													foreach ($blog as $key => $blog_data) 
													{
												?>
												<li class="post m-active">
													<div class="post-inner">
														<h4 class="post-title"><a href="<?php echo base_url();?>index.php/front/blogDetails/<?php echo $blog_data->blog_id; ?>/<?php echo $blog_data->blog_title; ?>"><?php echo $blog_data->blog_title; ?></a></h4>
														<span class="post-date">
															<?php
																$bulan   = date("M", strtotime($blog_data->blog_date));
																$tanggal = date("d", strtotime($blog_data->blog_date));
																$tahun   = date("Y", strtotime($blog_data->blog_date));
															?>
														<?php echo $bulan.". ".$tanggal." ".$tahun; ?>



														</span>
													</div>
												</li>
												<!-- POST : end -->
												<?php } ?>
											</ul>
											<p class="show-all-btn"><a href="<?php echo site_url('front/blog'); ?>">See All Notices</a></p>
										</div>
									</div>
								</div>
								<!-- POSTS WIDGET : end -->