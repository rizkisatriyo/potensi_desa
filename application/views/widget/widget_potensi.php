								<!-- FEATURED GALLERY WIDGET : begin -->
								<div class="widget featured-gallery-widget">
									<div class="widget-inner">
										<h3 class="widget-title"><i class="fa fa-picture-o" aria-hidden="true"></i>  Potensi</h3>
										<div class="widget-content">
											<div class="gallery-image" title="Streets of TownPress">
												<a href="<?= base_url().'front/potensi' ?>"><img src="<?= base_url().'assets/template/frontend/' ?>images/featured-gallery-01.jpg" alt=""></a>
											</div>
											<p class="show-all-btn"><a href="<?= base_url().'front/potensi' ?>">Lihat Semua Potensi</a></p>
            							</div>
									</div>
								</div>
								<!-- FEATURED GALLERY WIDGET : end -->