
		<div class="header-general">
			<div class="container">
				<div class="row">
					<div class="col-md-2 col-sm-6 col-xs-12">
						<div class="logo logo-pages">
							<h1 class="hidden">topshop wordpress theme</h1>
							<a href="#"><img src="<?php echo base_url();?>assets/lib-home/images/home/logo.png" alt="" /></a>
						</div>
					</div>
					<div class="col-md-8 col-sm-12 col-xs-3">
						<nav class="main-nav main-nav-pages">
							<?php include "application/views/menu.php"; ?>
						</nav>
					</div>
					<div class="col-md-2 col-sm-6 col-md-offset-0 col-sm-offset-6 col-xs-9">
						<ul class="meta-link-ontop list-inline-block meta-link-pages pull-right">
							<li>
								<div class="search-ontop">
									<form class="search-form">
										<input onblur="if (this.value=='') this.value = this.defaultValue" onfocus="if (this.value==this.defaultValue) this.value = ''" value="Search..." type="text">
										<div class="submit-form">
											<input value="" type="submit">
										</div>
									</form>
								</div>
							</li>
							<?php
							if($this->session->userdata('user_id')=='')
							{
								?>

							<?php } else { ?>
							<li>
								<div class="mini-cart-box mini-cart-ontop">
									<a class="mini-cart-link" href="#">
										<span class="mini-cart-icon"><i class="fa fa-user" aria-hidden="true"></i></span>
									</a>
									<div class="mini-cart-content">
										<div class="mini-cart-total mont-font  clearfix">
											<a href="<?php echo site_url('user/account'); ?>">
											<strong class="pull-right">Profil</strong>
											</a>
										</div>
										<div class="mini-cart-total mont-font  clearfix">
											<a href="<?php echo site_url('front/cart'); ?>">
											<strong class="pull-right">Cart</strong>
											</a>
										</div>
										<div class="mini-cart-total mont-font  clearfix">
											<a href="<?php echo site_url('user/order'); ?>">
											<strong class="pull-right">My Orders</strong>
											</a>
										</div>
										<div class="mini-cart-total mont-font  clearfix">
											<a href="<?php echo site_url('front/signout'); ?>">
											<strong class="pull-right">Sign out</strong>
											</a>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="mini-cart-box mini-cart-ontop">
									<a class="mini-cart-link" href="cart.html">
										<span class="mini-cart-icon"><i class="fa fa-shopping-basket" aria-hidden="true"></i></span>
										<?php if($this->session->userdata('cart_items_count')=='')	{ ?>
											<span class="mini-cart-number">0</span>
									</a>
										<?php } else { ?>
											<span class="mini-cart-number"><?php echo $this->session->userdata('cart_items_count'); ?></span>
										<?php } ?>
									<div class="mini-cart-content">
										<?php if($cart_products!='empty')
										{ ?>
											<?php if($this->uri->segment(2)=='added')
											{ ?>
												<div> Product is added to the cart</div>

												<?php
											}
											?>
											<h2 class="mont-font title18 color">
												<?php
													$total_price = 0;
													foreach($cart_products as $product)
														{
															$total_price += $product->item_total_price;

												?>
											</h2>
										<div class="list-mini-cart-item">
											<div class="productmini-cat table">
												<div class="product-thumb">
													<input type="hidden" name="items[]" value="<?php echo $product->item_id;?>" />
													<a href="#" class="product-thumb-link"><img alt="" src="<?php echo base_url().$product->item_image;?>" alt="<?php echo $product->item_name;?>"></a>
												</div>
												<div class="product-info">
													<h3 class="product-title"><a href="#"><?php echo $product->item_name;?></a></h3>
													<div class="product-price">
														<ins><span>Rp.<?php echo number_format($product->item_price,2);?></span></ins>
													</div>
													<div class="product-rate">
														<div class="product-rating" style="width:100%"></div>
													</div>
												</div>
											</div>
										</div>
										<?php } ?>
										<div class="mini-cart-total mont-font  clearfix">
											<strong class="pull-left">TOTAL</strong>
											<span class="pull-right color">Rp. <?php echo number_format($total_price,2); ?></span>
										</div>
										<div class="mini-cart-button clearfix">
											<a class="mini-cart-view shop-button pull-left" href="<?php echo site_url('front/cart');?>">View my cart </a>				
										</div>
										<?php } ?>

									</div>
								</div>
							</li>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- End Menu On Top -->
	</div>
	<!-- End Header -->

		<!-- End Banner Slider -->
