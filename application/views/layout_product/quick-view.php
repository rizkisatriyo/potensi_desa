<!DOCTYPE HTML>
<html lang="en">

<!-- Mirrored from demo.7uptheme.com/html/topshop/quick-view.html by HTTrack Website Copier/3.x [XR&CO'2010], Thu, 23 Mar 2017 11:23:20 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta name="description" content="Topshop is new Wordpress theme that we have designed to help you transform your store into a beautiful online showroom. This is a fully responsive Wordpress theme, with multiple versions for homepage and multiple templates for sub pages as well" />
	<meta name="keywords" content="Topshop,7uptheme" />
	<meta name="robots" content="noodp,index,follow" />
	<meta name='revisit-after' content='1 days' />
	<title>Topshop | Detail External Link</title>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat+Alternates" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/libs/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/bootstrap-theme.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/jquery.fancybox.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/jquery-ui.min.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/owl.carousel.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/owl.transitions.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/owl.theme.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/slick.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/jquery.mCustomScrollbar.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/animate.css"/>
	<link rel="stylesheet" type="text/css" href="css/libs/hover.css"/>
	<link rel="stylesheet" type="text/css" href="css/color.css" media="all"/>
	<link rel="stylesheet" type="text/css" href="css/theme.css" media="all"/>
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="all"/>
	<link rel="stylesheet" type="text/css" href="css/browser.css" media="all"/>
	<!-- <link rel="stylesheet" type="text/css" href="css/rtl.css" media="all"/> -->
</head>
<body>
<div class="wrap">
	<div id="content">
		<div class="container">
			<div class="detail-product detail-quickview detail-external-link">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="detail-gallery">
							<div class="wrap-detail-gallery">
								<div class="mid">
									<img src="images/photos/topshop_product_02.jpg" alt=""/>
								</div>
								<div class="gallery-control">
									<div class="carousel">
										<ul class="list-none">
											<li><a href="#" class="active"><img src="images/photos/topshop_product_02.jpg" alt=""/></a></li>
											<li><a href="#"><img src="images/photos/topshop_product_03.jpg" alt=""/></a></li>
											<li><a href="#"><img src="images/photos/topshop_product_04.jpg" alt=""/></a></li>
											<li><a href="#"><img src="images/photos/topshop_product_11.jpg" alt=""/></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="detail-social">
								<a href="#" class="share-face"><i class="fa fa-facebook" aria-hidden="true"></i></a>
								<a href="#" class="share-twit"><i class="fa fa-twitter" aria-hidden="true"></i></a>
								<a href="#" class="share-pint"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
							</div>
						</div>
						<!-- End Gallery -->
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="detail-info">
							<h2 class="title-product-detail">Topshop Name Product</h2>
							<ul class="list-inline-block review-rating">
								<li>
									<div class="product-rate rate-counter">
										<div class="product-rating" style="width:100%"></div>
										<span>(1s)</span>
									</div>
								</li>
								<li>
									<a href="#" class="add-review">Add your review</a>
								</li>
							</ul>
							<div class="product-price">
								<ins><span>$400.00</span></ins>
								<del><span>$480.00</span></del>
								<span class="sale-label">-20<sup>%</sup></span>
							</div>
							<div class="product-more-info">
								<h2 class="title14">Product Features</h2>
								<p class="desc">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed diam nonummy nibh</p>
								<ul class="list-none">
									<li><a href="#">Seat Height - Floor to Seat: 24''</a></li>
									<li><a href="#">Frame Material: Wood</a></li>
									<li><a href="#">Seat Material: Wood</a></li>
									<li><a href="#">Adjustable Height: No</a></li>
									<li><a href="#">Overall: 24'' H x 17'' W x 14'' D</a></li>
								</ul>
							</div>
							<div class="detail-qty info-qty border radius">
								<a href="#" class="qty-down"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
								<span class="qty-val">1</span>
								<a href="#" class="qty-up"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
							</div>
							<a class="addcart-link add-cart-detail" href="#">Add to Cart</a>
							<div class="product-extra-link">
								<a href="#" class="wishlist-link"><i class="fa fa-heart" aria-hidden="true"></i><span>Wishlist</span></a>
								<a href="#" class="compare-link"><i class="fa fa-stumbleupon" aria-hidden="true"></i><span>Compare</span></a>
							</div>
							<div class="detail-extra">
								<p class="desc product-code">SUK: <span>tem009</span></p>
								<p class="desc product-available">Available: <span class="avail-instock">instock</span></p>
								<p class="desc tags-detail">
									<span>Tags:</span> 
									<a href="#">Homeware</a>
									<a href="#">Furniture</a>
									<a href="#">Living room</a>
									<a href="#">Gift</a>
									<a href="#">Flower</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- End Detail Product -->
		</div>	
	</div>
	<!-- End Content -->
</div>
<script type="text/javascript" src="../../../ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script type="text/javascript" src="js/libs/bootstrap.min.js"></script>
<script type="text/javascript" src="js/libs/jquery.fancybox.js"></script>
<script type="text/javascript" src="js/libs/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/libs/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/libs/jquery.jcarousellite.js"></script>
<script type="text/javascript" src="js/libs/jquery.elevatezoom.js"></script>
<script type="text/javascript" src="js/libs/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="js/libs/modernizr.custom.js"></script>
<script type="text/javascript" src="js/libs/jquery.hoverdir.js"></script>
<script type="text/javascript" src="js/libs/slick.js"></script>
<script type="text/javascript" src="js/libs/wow.js"></script>
<script type="text/javascript" src="js/theme.js"></script>
</body>

<!-- Mirrored from demo.7uptheme.com/html/topshop/quick-view.html by HTTrack Website Copier/3.x [XR&CO'2010], Thu, 23 Mar 2017 11:23:20 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
</html>