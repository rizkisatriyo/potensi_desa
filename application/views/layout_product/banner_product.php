<div id="content">
		<div class="banner-slider banner-shop-slider bg-slider">
			<div class="wrap-item" data-pagination="true" data-autoplay="true" data-transition="fade" data-navigation="true" data-itemscustom="[[0,1]]">
			<?php
				foreach ($banner_product as $key => $value) {
					# code...
				
			?>
				<div class="item-slider">
					<div class="banner-thumb"><a href="#">
						<?php 
								$image = array(
	                                'src'    => 'images/uploads/'.$value->image,
	                                'alt'    => 'Foto Baner Front',
	                            );

	                            echo img($image);
	                         ?>
					</a></div>
					<div class="banner-info">
						<div class="container">
							<div class="banner-content-text white text-center animated" data-animated="zoomIn">
								<h2 class="title30 mont-font"><?php echo $value->title; ?></h2>
								<p><?php echo $value->description; ?></p>
							</div>
						</div>
					</div>
				</div>
			<?php 
				}
			?>
			</div>
		</div>
