<!DOCTYPE HTML>
<html lang="en">

<!-- Mirrored from demo.7uptheme.com/html/topshop/grid-with-sidebar.html by HTTrack Website Copier/3.x [XR&CO'2010], Thu, 23 Mar 2017 11:22:22 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<meta name="description" content="Topshop is new Wordpress theme that we have designed to help you transform your store into a beautiful online showroom. This is a fully responsive Wordpress theme, with multiple versions for homepage and multiple templates for sub pages as well" />
	<meta name="keywords" content="Topshop,7uptheme" />
	<meta name="robots" content="noodp,index,follow" />
	<meta name='revisit-after' content='1 days' />
	<title>Topshop | Grid With Sidebar</title>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat+Alternates" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib-home/css/libs/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib-home/css/libs/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib-home/css/libs/bootstrap-theme.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib-home/css/libs/jquery.fancybox.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib-home/css/libs/jquery-ui.min.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib-home/css/libs/owl.carousel.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib-home/css/libs/owl.transitions.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib-home/css/libs/owl.theme.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib-home/css/libs/slick.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib-home/css/libs/jquery.mCustomScrollbar.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib-home/css/libs/animate.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib-home/css/libs/hover.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib-home/css/color.css" media="all"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib-home/css/theme.css" media="all"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib-home/css/responsive.css" media="all"/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/lib-home/css/browser.css" media="all"/>
	<!-- <link rel="stylesheet" type="text/css" href="css/rtl.css" media="all"/> -->

</head>
<body>
<div class="wrap">
	<div id="header">
		<div class="bg-color top-header">
		  <div class="container">
		    <div class="row">
		      <div class="col-md-6 col-sm-6 col-xs-12">
		        <ul class="list-inline-block header-top-menu">
							<li><a href="<?php echo base_url();?>">Home</a></li>
		          <li><a href="<?php echo site_url('front/aboutUs');?>">About</a></li>
		          <li><a href="<?php echo site_url('front/shop');?>">Shop</a></li>
		          <li><a href="<?php echo site_url('front/blog');?>">Blog</a></li>
		          <li><a href="#">Contact</a></li>
		          <li><a href="#">FAQ</a></li>
		        </ul>
		      </div>
		      <div class="col-md-6 col-sm-6 col-xs-12">
		        <ul class="text-right list-inline-block header-top-menu header-contact-link">
							<?php foreach ($select_contact as $value): ?>
		          <li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i>E-mail : <?php echo $value->email; ?></a></li>
		          <li><a href="#"><i class="fa fa-clock-o" aria-hidden="true"></i>8:30-21:00</a></li>
		          <li><a href="#"><i class="fa fa-volume-control-phone" aria-hidden="true"></i><?php echo $value->telephone; ?></a></li>
							<?php endforeach ?>
		        </ul>
		      </div>
		    </div>
		  </div>
		</div>
