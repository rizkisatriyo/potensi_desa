<?php include("header_product.php"); ?>
<?php include("menu_top.php"); ?>
<?php include("banner_product.php"); ?>
<?php include("breadcrum.php"); ?>
<?php include("sidebar.php"); ?>
					<div class="col-md-9 col-sm-8 col-xs-12">
						<div class="content-shop">
							<div class="sort-pagi-bar clearfix">
								<h2 class="title18 mont-font pull-left"><?php echo str_replace('-',' ',ucwords($this->uri->segment(3))); ?></h2>
								<div class="sort-view pull-right">
									<div class="view-type pull-left">
										<a href="grid-with-sidebar.html" class="grid-view active"><i class="fa fa-th-large" aria-hidden="true"></i></a>
										<a href="list-with-sidebar.html" class="list-view"><i class="fa fa-th-list" aria-hidden="true"></i></a>
									</div>
									<div class="sort-bar select-box">
										<label>Sort:</label>
										<select>
											<option value="">Recommended</option>
											<option value="">position</option>
											<option value="">price</option>
										</select>
									</div>
								</div>
							</div>
							<div class="content-grid-sidebar">
								<div class="row">
                  			<?php
      						foreach ($home_products as $product)
      						{

      							?>
									<div class="col-md-4 col-sm-6 col-xs-12">
										<div class="item-product item-product-grid">
											<div class="item <?php if($i%3==0) echo 'no-margin-right';?>">
											<div class="product-thumb box-hover-dir">
												<img src="<?php echo base_url().$product->big_image;?>" alt="<?php echo $product->item_name; ?>" alt="">
												<div class="info-product-hover-dir">
													<div class="inner-product-hover-dir">
														<div class="content-product-hover-dir">
															<a href="<?php echo base_url();?>layout_product/quick-view.php" class="quickview-link fancybox.iframe">Quick view <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
															<span class="product-total-sale"></span>
															<div class="product-extra-link">
																<a href="<?php echo site_url('buy/'.$product->item_id);?>" class="addcart-link"><i class="fa fa-shopping-basket" aria-hidden="true"></i><span>Add to cart</span></a>
																<a href="#" class="wishlist-link"><i class="fa fa-heart" aria-hidden="true"></i><span>Wishlist</span></a>

															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="product-info">
												<h3 class="product-title"><a href="<?php echo base_url();?>index.php/details/<?php echo $product->item_id; ?>/<?php echo url_title(strtolower($product->item_name)); ?>">
							                   <?php echo $product->item_name; ?></a></h3>
												<div class="product-rate">
													<div class="product-rating" style="width:100%"></div>
												</div>
												<div class="product-price">
													<ins><span><?php echo number_format($product->item_price,2); ?></span></ins>
													<del><span>$480.00</span></del>
													<span class="sale-label">-20<sup>%</sup></span>
												</div>
											</div>
										</div>
									</div>


									<!-- End Group -->
								</div>
							} // end for each
						?>
							</div>
							<div class="pagi-nav-bar text-center radius">
								<a href="#" class="btn-circle prev-page"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a>
								<a href="#" class="inline-block">1</a>
								<a href="#" class="current-page inline-block">2</a>
								<a href="#" class="inline-block">3</a>
								<a href="#" class="inline-block">4</a>
								<a href="#" class="btn-circle next-page"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Content Pages -->
	</div>
	<!-- End Content -->
<?php include("application/views/footer.php"); ?>
