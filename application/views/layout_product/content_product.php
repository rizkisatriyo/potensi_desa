
		<!-- HEADER BG : begin -->
		<div class="header-bg">

			<!-- HEADER IMAGE : begin -->
			<!-- To add more images just copy and edit elements with "image-layer" class (see home-2.html template for example)
			Change autoplay speed with "data-autoplay" attribute (in seconds), works only if there are more than one image -->
			<div class="header-image" data-autoplay="8">
				<div class="image-layer" style="background-image: url( '<?= base_url().'assets/frontend/' ?>images/header-01.jpg' );"></div>
				<!-- div class="image-layer" style="background-image: url( 'images/header-02.jpg' );"></div -->
			</div>
			<!-- HEADER IMAGE : begin -->

		</div>
		<!-- HEADER BG : end -->

		<!-- CORE : begin -->
		<div id="core">
			<div class="c-container">
				<div class="row">

					<!-- MIDDLE COLUMN : begin -->
					<div class="middle-column col-md-6 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header" class="m-has-breadcrumbs">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1>Galleries</h1>
							</div>
							<!-- PAGE TITLE : end -->

							<!-- BREADCRUMBS : begin -->
							<div class="breadcrumbs">
								<ul>
									<li class="home"><a href="index-2.html">Home</a></li>
									<li>Galleries</li>
								</ul>
							</div>
							<!-- BREADCRUMBS : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">

								<!-- GALLERY LIST PAGE : begin -->
								<div class="gallery-list-page gallery-page">
									<div class="c-gallery">
										<!-- You can change the number of columns by changing "m-3-columns" class
										in the following element to m-2-columns | m-4-columns | m-5-columns : begin -->
										<ul class="gallery-images m-layout-masonry m-3-columns">

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="<?= base_url().'assets/frontend/' ?>images/gallery-01.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">Summer Camp for Kids</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="<?= base_url().'assets/frontend/' ?>images/gallery-02.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">Annual Marathon</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="<?= base_url().'assets/frontend/' ?>images/gallery-03.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">Summer Rock Festival</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="<?= base_url().'assets/frontend/' ?>images/gallery-04.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">TownPress State Park</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="<?= base_url().'assets/frontend/' ?>images/gallery-05.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">Friendly Soccer Match</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="<?= base_url().'assets/frontend/' ?>images/gallery-06.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">TownPress Sightseeing</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="<?= base_url().'assets/frontend/' ?>images/gallery-07.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">Streets of TownPress</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="<?= base_url().'assets/frontend/' ?>images/gallery-08.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">Campgrounds</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="gallery-detail.html">
														<img src="<?= base_url().'assets/frontend/' ?>images/gallery-09.jpg" alt="">
													</a>
													<h2 class="gallery-title"><a href="gallery-detail.html">New Housing</a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->

										</ul>
									</div>
								</div>
								<!-- GALLERY LIST PAGE : begin -->

								<!-- PAGINATION : begin -->
								<div class="c-pagination">
									<ul>
										<li class="m-active"><a href="post-list.html">1</a></li>
										<li><a href="post-list.html">2</a></li>
										<li><a href="post-list.html">3</a></li>
									</ul>
								</div>
								<!-- PAGINATION : end -->

							</div>
						</div>
						<!-- PAGE CONTENT : end -->

						<hr class="c-separator m-margin-top-small m-margin-bottom-small m-transparent hidden-lg hidden-md">

					</div>
					<!-- MIDDLE COLUMN : end -->

					<!-- LEFT COLUMN : begin -->
					<div class="left-column col-md-3 col-md-pull-6">

						<!-- SIDE MENU : begin -->
						<?php include "sidebar.php"; ?>
						
						<!-- SIDE MENU : end -->

						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<!-- FEATURED GALLERY WIDGET : begin -->
								<div class="widget featured-gallery-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico"><i class="widget-ico tp tp-pictures"></i>Featured Gallery</h3>
										<div class="widget-content">
											<div class="gallery-image" title="Streets of TownPress">
												<a href="gallery-detail.html"><img src="<?= base_url().'assets/frontend/' ?>images/featured-gallery-01.jpg" alt=""></a>
											</div>
											<p class="show-all-btn"><a href="gallery-list.html">See All Galleries</a></p>
            							</div>
									</div>
								</div>
								<!-- FEATURED GALLERY WIDGET : end -->

							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

					<!-- RIGHT COLUMN : begin -->
					<div class="right-column col-md-3">

						<!-- RIGHT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<!-- CATEGORIES WIDGET : begin -->
								<div class="widget categories-widget">
									<div class="widget-inner">
										<h3 class="widget-title m-has-ico">
										<i class="widget-ico tp tp-list4"></i>Categories</h3>
										<div class="widget-content">
											<ul>
												<li><a href="gallery-list.html">Community</a> (2)</li>
												<li><a href="gallery-list.html">Music</a> (2)</li>
												<li><a href="gallery-list.html">Sights</a> (2)</li>
												<li><a href="gallery-list.html">Sport</a> (1)</li>
											</ul>
										</div>
									</div>
								</div>
								<!-- CATEGORIES WIDGET : end -->

								<!-- IMAGE WIDGET : begin -->
								<div class="widget image-widget">
									<div class="widget-inner">
										<div class="widget-content">
											<a href="#"><img src="<?= base_url().'assets/frontend/' ?>images/poster-01.jpg" alt=""></a>
										</div>
									</div>
								</div>
								<!-- IMAGE WIDGET : end -->

							</div>
						</aside>
						<!-- RIGHT SIDEBAR : end -->

					</div>
					<!-- RIGHT COLUMN : end -->

				</div>
			</div>
		</div>
		<!-- CORE : end -->
