<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Login - Admininistrator</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="Halaman Login Admin">
		<meta name="author" content="Sima Soareka Indonesia">
		<link rel="shortcut icon" href="../assets/lib-admin/css/images/favicon.png">

		<!-- css -->
		<link href="<?php echo base_url() ?> assets/lib-admin/css/twitter/bootstrap.css" rel="stylesheet">
		<link href="<?php echo base_url() ?>assets/lib-admin/css/base.css" rel="stylesheet">
		<link href="<?php echo base_url() ?>assets/lib-admin/css/twitter/responsive.css" rel="stylesheet">
		<link href="<?php echo base_url() ?>assets/lib-admin/css/jquery-ui-1.8.23.custom.css" rel="stylesheet">
		<script src="<?php echo base_url() ?>assets/lib-admin/js/plugins/modernizr.custom.32549.js"></script>
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	</head>

	<body>
		<div id="loading" class="other_pages">
			<!-- Login page -->
			<div id="login">
				<!-- cek browser support dengan modernizr -->
				<div class="support-note">
					<span class="no-csstransforms">CSS transforms are not supported in your browser</span>
					<span class="no-csstransforms3d">CSS 3D transforms are not supported in your browser</span>
					<span class="no-csstransitions">CSS transitions are not supported in your browser</span>
				</div>

				<div class="row-fluid">
					<div class="row-fluid">
						<div id="arrow_register"></div>
			            <div class="logo" class="pull-left"><a href="index.html"></a></div>
			            <div class="pull-right spacer-medium not-member members right_offset">Welcome Administrator</div>
					</div>

					<?php echo $this->session->flashdata('msg'); ?>
					<div class="row-fluid bb-bookblock" id="bb-bookblock">
						<div class="bb-item row-fluid login">
							<div class="top-background">
								<div class="fill-background right"><div class="bg row-fluid"></div></div>
								<div id="login-corner-shadow"></div>
							</div>
							<div class="row-fluid container">
								<div class="content">
									<div class="message row-fluid">
										<h4>Masukan username dan password Anda</h4>
										<h3>Have fun!</h3>
									</div>

									<form class="form-horizontal row-fluid" action="<?php echo site_url('form-html/login/loginAction'); ?>" method="post">
										<!-- username -->
										<div class="control-group row-fluid">
											<label class="row-fluid " for="inputEmail">Username</label>
											<div class="controls row-fluid input-append">
												<input type="text" name="yourusername" id="inputUsername" placeholder="Masukan Username"  class="row-fluid"> <span class="add-on"><i class="icon-user"></i></span>
											</div>
										</div>

										<!-- password -->
										<div class="control-group row-fluid">
											<label class="row-fluid " for="inputPassword">Password</label>
											<div class="controls row-fluid input-append">
												<input type="password" name="yourpassword" id="inputPassword" placeholder="Masukan Password"  class="row-fluid"> <span class="add-on"><i class="icon-lock"></i></span>
											</div>
										</div>

										<div class="control-group row-fluid"></div>
										<div class="control-group row-fluid fluid">
											<!-- remember me -->
											<div class="controls span2">
												<!-- <label class="checkbox"><input type="checkbox"> Remember me</label> -->
											</div>

											<!-- submit -->
											<div class="controls span8">
												<input type="submit" name="btnLogin" class="btn btn-primary btn-larg1e row-fluid" value="Masuk" />
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

    	<!-- js -->
    	<script src="<?= base_url().'assets/' ?>template/frontend/library/js/jquery-1.9.1.min.js" type="text/javascript"></script>

    	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>
    	<script type="text/javascript" src="<?php echo base_url() ?>assets/lib-admin/js/plugins/jquery.bookblock.js"></script>

    	<script type="text/javascript">
	    	$(function() {
	    		var Page = (function() {
	    			var config = {
	    				$bookBlock: $( '#bb-bookblock' ),
	    				$navNext  : $( '#bb-nav-next' ),
	    				$navPrev  : $( '#bb-nav-prev' ),
	    				$navJump  : $( '#bb-nav-jump' ),
	    				bb        : $( '#bb-bookblock' ).bookblock( {
	    					speed       : 800,
	    					shadowSides : 0.8,
	    					shadowFlip  : 0.7
	    				})
	    			},
	    			init = function() {
	    				initEvents();
	    			},

	    			initEvents = function() {
	    				var $slides = config.$bookBlock.children(),
	    				totalSlides = $slides.length;
	    				config.$navNext.on( 'click', function() {
	    					$("#arrow_register").fadeOut();
	    					$(".not-member").slideUp();
	    					$(".already-member").slideDown();
	    					config.bb.next();
	    					return false;
	    				});
	    			};

	    			return { init : init };
	    		})();

	    		Page.init();
	    	});
    	</script>
	</body>
</html>
