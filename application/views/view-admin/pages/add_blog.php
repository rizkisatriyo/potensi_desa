<div id="main">
  <div class="container">
    <!-- container_top -->

    <!-- End container_top -->
    <div class="row-fluid">
      <div class="span12">
          <div class="box gradient">
            <div class="title">
              <h3>
              <i class="icon-book"></i><span>Add Blog</span>
              </h3>
            </div>
            <div class="span7">
            <div class="content">
            <?php
              echo validation_errors('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>');
              echo $pesan;
              $data = array('class' => 'form-horizontal row-fluid');
              echo form_open_multipart(site_url('form-html/blog/actionAdd'), $data);
            ?>
                  <div class="form-row control-group row-fluid">
                    <label class="control-label span3" for="normal-field">Title</label>
                    <div class="controls span7">
                      <input type="text" name="blog_title" id="normal-field" class="row-fluid">
                    </div>
                  </div>

                  <!-- gambar -->
                  <div class="form-row control-group row-fluid">
                        <label class="control-label span3" for="search-input">Image</label>
                        <div class="controls span7">
                          <div class="input-append row-fluid">
                            <input type="file" name="blog_image" class="spa1n6 fileinput" id="search-input">
                          </div>
                        </div>
                  </div>

                  <!-- keterangan full -->
                  <div class="form-row control-group row-fluid">
                    <label class="control-label span3" for="editor1">Full Description</label>
                    <div class="controls span7">
                      <textarea name="blog_desc" class="desc"></textarea>
                    </div>
                  </div>

                  <input type="submit" class="btn" rel="tooltip" data-placement="top" value="Add Blog" />
                  <?php
                  echo anchor(site_url('form-html/blog'),'<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Kembali',
                    array('style' => 'text-decoration: none', 'class' => 'btn btn-default'));
                ?>
              </form>
            </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
