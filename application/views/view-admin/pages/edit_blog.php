<div id="main">
  <div class="container">
    <!-- container_top -->
    <div class="row-fluid">
      <div class="span12">
          <div class="box gradient">
            <div class="title">
              <h3>
              <i class="icon-book"></i><span>Edit Blog</span>
              </h3>
            </div>
            <div class="span7">
            <div class="content">
            <?php
              echo validation_errors('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>');
              echo $pesan;
              $data = array('class' => 'form-horizontal row-fluid');
              echo form_open_multipart(site_url('form-html/blog/actionEdit'), $data);

            foreach ($blogs as $value) { ?>
                  <input type="hidden" name="blog_id" value="<?php echo $value->blog_id; ?>">
                  <div class="form-row control-group row-fluid">
                    <label class="control-label span3" for="normal-field">Title</label>
                    <div class="controls span7">
                      <input type="text" name="blog_title" id="normal-field" class="row-fluid" value="<?php echo $value->blog_title; ?>">
                    </div>
                  </div>

                  <!-- gambar -->
                  <div class="form-row control-group row-fluid">
                        <label class="control-label span3" for="search-input">Image</label>
                        <div class="controls span7">
                          <div class="input-append row-fluid">
                            <?php
                              $image = array(
                                  'src'    => 'images/uploads/'.$value->blog_image,
                                  'alt'    => 'Foto Blog',
                                  'class'  => 'thumbnail small',
                                  'width'  => '200',
                                  'height' => '200'
                              );

                              echo img($image);
                            ?>
                            <br/>
                            <input type="file" name="blog_image" class="spa1n6 fileinput" id="search-input">
                          </div>
                        </div>
                  </div>

                  <!-- keterangan full -->
                  <div class="form-row control-group row-fluid">
                    <label class="control-label span3" for="editor1">Full Description</label>
                    <div class="controls span7">
                      <textarea name="blog_desc" class="desc" ><?php echo $value->blog_desc; ?></textarea>
                    </div>
                  </div>

                  <button type="submit" class="btn btn-md btn-default" rel="tooltip" data-placement="top" name="editBlogs" data-original-title=".btn">
                    <span class="glyphicon glyphicon-edit"></span>&nbsp;Update Blog
                  </button>
                  <?php
                  echo anchor(site_url('form-html/blog'),'<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Kembali',
                    array('style' => 'text-decoration: none', 'class' => 'btn btn-default'));
                ?>
              </form>
              <?php } ?>
            </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  
</div>
