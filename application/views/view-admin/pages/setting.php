  <div id="main">
      <div class="container">
      <!-- container_top -->

      <!-- End container_top -->
      <div class="row-fluid">
          <div class="box gradient span12">
              <div class="title"><div class="row-fluid"><h3>Setting</h3></div></div>
          
              <div class="content">
                  <ul id="tabExample1" class="nav nav-tabs">
                      <?php $active = $this->session->flashdata('aktif'); ?>
                      <?php $active = $active == '' ? 'logo' : $active ?>
                      <li class="<?php echo $active == 'logo' ? 'active' : ''; ?>"><a href="#logo" data-toggle="tab">Tentang Desa</a></li>
                      <li class="<?php echo $active == 'social' ? 'active' : ''; ?>"><a href="#socialmedia" data-toggle="tab">Sosial Media</a></li>
                      <li class="<?php echo $active == 'contact' ? 'active' : ''; ?>"><a href="#contact" data-toggle="tab">Contact</a></li>
                  </ul>
              
                  <div class="tab-content">
                      <!-- logo -->
                      <div class="tab-pane fade in <?php echo $active == 'logo' ? 'active' : ''; ?>" id="logo">
                        <form action="<?php echo site_url('form-html/setting/actionEdit'); ?>" method="post" enctype="multipart/form-data">
                          <input type="hidden" name="id" value="<?php echo $logos->id; ?>" />
                          <input type="hidden" name="tabel" value="logo" />

                          <!-- logo -->
                          <!-- <div class="form-row control-group row-fluid">
                              <label class="control-label span3" for="search-input">
                                Upload Logo
                                <br/>
                                <?php
                                  $image = array(
                                      'src'    => 'images/uploads/'.$logos->image,
                                      'alt'    => 'Foto Logo',
                                      'class'  => 'thumbnail small',
                                      'width'  => '100',
                                      'height' => '100'
                                  );

                                  echo img($image);
                                ?>
                              </label>
                              <div class="controls span7">
                                  <div class="input-append row-fluid">
                                      <input type="file" name="logo" class="spa1n6 fileinput" id="search-input">
                                  </div>
                              </div>
                          </div> -->
                          <div class="form-row control-group row-fluid">
                                <label class="control-label span3" for="max-length">tentang</label>
                                <div class="controls span5">
                                    <textarea name="tentang" id="normal-field" class="row-fluid"><?php echo $logos->tentang; ?></textarea>
                                </div>
                            </div>
                          <input type="submit" name="editLogo" value="Update" />
                        </form>
                      </div>

                      <!-- social -->
                      <div class="tab-pane fade in <?php echo $active == 'social' ? 'active' : ''; ?>" id="socialmedia">
                        <form action="<?php echo site_url('form-html/setting/actionEdit'); ?>" method="post">
                        <?php
                            foreach ($socials as $key => $social) 
                            {
                        ?>
                            <input type="hidden" name="id" value="<?php echo $social->id; ?>" />
                            <input type="hidden" name="tabel" value="social" />

                            <!-- <div class="form-row control-group row-fluid">
                                <label class="control-label span3" for="max-length">Google Plus</label>
                                <div class="controls span5">
                                    <input type="text" name="google_plus" id="normal-field" class="row-fluid" value="<?php echo $social->google_plus; ?>">
                                </div>
                            </div> -->

                            <div class="form-row control-group row-fluid">
                                <label class="control-label span3" for="max-length">Facebook</label>
                                <div class="controls span5">
                                    <input type="text" name="facebook" id="normal-field" class="row-fluid" value="<?php echo $social->facebook; ?>">
                                </div>
                            </div>
                              
                            <div class="form-row control-group row-fluid">
                                <label class="control-label span3" for="max-length">Twitter</label>
                                <div class="controls span5">
                                   <input type="text" name="twitter" id="normal-field" class="row-fluid" value="<?php echo $social->twitter; ?>">
                                </div>
                            </div>
                            
                            <div class="form-row control-group row-fluid">
                                <label class="control-label span3" for="max-length">Instagram</label>
                                <div class="controls span5">
                                    <input type="text" name="instagram" id="normal-field" class="row-fluid" value="<?php echo $social->instagram; ?>">
                                </div>
                            </div>
                          <?php 
                              } 
                          ?>
                            <input type="submit" name="editSocial" value="Update" />
                          </form>
                      </div>

                      <!-- contact -->
                      <div class="tab-pane fade in <?php echo $active == 'contact' ? 'active' : ''; ?>" id="contact">
                        <form action="<?php echo site_url('form-html/setting/actionEdit'); ?>" method="post">
                        <?php
                          foreach ($contacts as $key => $contact) 
                          {
                        ?>
                            <input type="hidden" name="id" value="<?php echo $contact->id; ?>" />
                            <input type="hidden" name="tabel" value="contact" />
                            
                            <div class="form-row control-group row-fluid">
                               <label class="control-label span3">Address</label>
                               <div class="controls span5">
                                  <input type="text" name="address" id="normal-field" class="row-fluid" value="<?php echo $contact->address; ?>">
                                </div>
                            </div>
                  
                            <div class="form-row control-group row-fluid">
                                <label class="control-label span3">Email Address</label>
                                <div class="controls span5">
                                  <input type="text" name="email" id="normal-field" class="row-fluid" value="<?php echo $contact->email; ?>">
                                </div>
                            </div>
                  
                            <div class="form-row control-group row-fluid">
                                <label class="control-label span3">No Telp</label>
                                <div class="controls span5">
                                   <input type="text" name="telephone" id="normal-field" class="row-fluid" value="<?php echo $contact->telephone; ?>">
                                </div>
                            </div>
                        <?php 
                          } 
                        ?>
                          <input type="submit" name="editContact" value="Update" />
                        </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>