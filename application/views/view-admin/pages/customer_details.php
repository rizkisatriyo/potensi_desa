<div id="main">
  <div class="container">
    <div class="row-fluid">
      <div class="span12">
        <div class="box gradient">
          <div class="title">
            <h3>
              <i class="icon-book"></i><span>Details Customer</span>
            </h3>
          </div>
          <div class="span7">
            <div class="content">
                <?php 
                  foreach ($customers as $key => $value) {
                    # code...
                  ?>
                <div class="form-row control-group row-fluid">
                 <label class="control-label span3">Name</label>
                   <div class="controls span7">
                     <input type="text" class="row-fluid" readonly="readonly" value="<?php echo $value->full_name; ?>">
                   </div>
                </div>

                 <div class="form-row control-group row-fluid">
                  <label class="control-label span3">Email</label>
                    <div class="controls span7">
                      <input type="text" class="row-fluid" readonly="readonly" value="<?php echo $value->email; ?>">
                    </div>
                 </div>

                 <div class="form-row control-group row-fluid">
                  <label class="control-label span3">Date Joined</label>
                    <div class="controls span7">
                      <input type="text" class="row-fluid" readonly="readonly" value="<?php echo $value->date_joined; ?>">
                    </div>
                 </div>

                 <div class="form-row control-group row-fluid">
                  <label class="control-label span3">Account Type</label>
                    <div class="controls span7">
                      <input type="text" class="row-fluid" readonly="readonly" value="<?php echo $value->account_type; ?>">
                    </div>
                 </div>

                 <div class="form-row control-group row-fluid">
                  <label class="control-label span3">Company Name</label>
                    <div class="controls span7">
                      <input type="text" class="row-fluid" readonly="readonly" value="<?php echo $value->company_name; ?>">
                    </div>
                 </div>
                 <?php
                  echo anchor(site_url('form-html/customer'),'<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Kembali', 
                    array('style' => 'text-decoration: none', 'class' => 'btn btn-default'));
                ?>
               </div>
              <?php
               }
                ?>
            </div>
          </div>
      </div>
    </div>
  </div>
