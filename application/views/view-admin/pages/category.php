<div id="main">
  <div class="container">

  <div id="container2">

    <div class="row-fluid">
        <div class="box gradient">
            <div class="title">
              <h5><a href="<?php echo site_url('form-html/category/formAdd'); ?>" class="btn btn-inverse" rel="tooltip" data-placement="top" data-original-title="View"><i class="gicon-plus icon-white"></i>Add Category</a></h5>
            </div><!-- End .title -->
            <div class="content top">
              <table id="datatable_example" class="responsive table table-striped table-bordered" style="width:100%;margin-bottom:0; ">
                <thead>
                  <tr>
                     <th style="width:80%;">Category</th>
                    <th class="ms no_sort" style="width:15%;">Actions</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  if($categories == 'empty')
                  {
                    echo " Sorry - No category found";
                  }
                  else
                  {
                    foreach ($categories as $value)
                    {
                      ?>
                  <tr>
                    <td><?php echo $value->cat_name; ?></td>

                    <td class="ms"><div class="btn-group1">
                      <?php
                      echo anchor('form-html/category/formEdit/'.$value->cat_id, '<i class="gicon-edit"></i>', array('class' => 'btn btn-small', 'rel' => 'tooltip', 'data-placement' => 'left', 'data-original-title' => 'Edit'));
                      ?>
                        <a href="<?php echo base_url();?>index.php/form-html/category/delete_cat/<?php echo $value->cat_id;?>"class="btn btn-inverse btn-small" rel="tooltip" data-placement="bottom" data-original-title="Remove"><i class="gicon-remove icon-white"></i></a>
                      </div>
                    </td>
                  </tr>
                  <?php
                    }
                  }
                  ?>

                </tbody>
              </table>
            </div><!-- End .content -->
        </div> <!-- End box -->
    </div> <!-- End .row-fluid -->
  </div><!-- End #container -->
</div>
