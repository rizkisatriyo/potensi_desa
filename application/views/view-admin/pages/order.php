<div id="main">
  <div class="container">

  <div id="container2">

    <div class="row-fluid">
        <div class="box gradient">
            <div class="title">
              <h4>list Order</h4>
            </div><!-- End .title -->
            <!-- jos -->
            <div class="content top">
              <table id="datatable_example" class="responsive table table-striped table-bordered" style="width:100%;margin-bottom:0; ">
                <thead>
                  <tr>
                     <th class='sorting_desc'>Order No</th>
                     <th>Order Time</th>
                     <th>Name</th>
                     <th>Status</th>
                    <th class="no_sort" style="width:15%;">Actions</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  if($orders == 'empty')
                  {
                    echo "Data Kosong";
                  }
                  else
                  {
                    foreach ($orders as $value)
                    {
                      ?>
                  <tr>
                    <td ><?php echo $value->order_id; ?></td>
                    <td><?php echo $value->order_date; ?></td>
                    <td><?php echo $value->full_name; ?></td>
                    <td>
                      <?php

                        if($value->order_status=='dispatched')
                        echo '<span style="color:green"><strong>Dispatched</strong></span>';

                        if($value->order_status=='cancelled')
                        echo '<span style="color:red"><strong>Cancelled</strong></span>';

                        if($value->order_status=='new')
                        echo '<span style="color:orange"><strong>Order Received</strong></span>';
                      ?>
                    </td>

                    <td class="ms"><div class="btn-group1">
                        <?php
                        echo anchor('form-html/order/showOrderDetail/'.$value->order_id, '<i class="gicon-eye-open icon-white"></i>', array('class' => 'btn btn-inverse btn-small', 'rel' => 'tooltip', 'data-placement' => 'bottom', 'data-original-title' => 'Remove'));
                        ?>

                        <?php if($value->order_status=='new') { ?>
                        <?php
                        echo anchor('form-html/order/cancel_order/'.$value->order_id, '<i class="gicon-repeat icon-white"></i>', array('class' => 'btn btn-inverse btn-small', 'rel' => 'tooltip', 'data-placement' => 'bottom', 'data-original-title' => 'Remove'));
                        ?>
                        <?php
                        echo anchor('form-html/order/dispatch_order/'.$value->order_id, '<i class="gicon-ok icon-white"></i>', array('class' => 'btn btn-inverse btn-small', 'rel' => 'tooltip', 'data-placement' => 'bottom', 'data-original-title' => 'Remove'));
                        ?>
                        <?php } ?>
                      </div>
                    </td>
                  </tr>
                  <?php
                    }
                  }
                  ?>

                </tbody>
              </table>
            </div><!-- End .content -->
        </div> <!-- End box -->
    </div> <!-- End .row-fluid -->
  </div><!-- End #container -->
</div>
