<div id="main">
  <div class="container">
    <!-- container_top -->

    <!-- End container_top -->
    <div class="row-fluid">
      <div class="span12">
          <div class="box gradient">
            <div class="title">
              <h3>
              <i class="icon-book"></i><span>Profil</span>
              </h3>
            </div>
            <div class="span12">
            <div class="content">
            <?php
              echo validation_errors('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>');
            echo $this->session->flashdata('msg'); 

              $data = array('class' => 'form-horizontal row-fluid');
              echo form_open_multipart(site_url('form-html/pages/actionEditProfil'), $data);
            ?>

                  <!-- gambar -->
                  <div class="form-row control-group row-fluid">
                        <label class="control-label span1" for="search-input">Image</label>
                        <div class="controls span9">
                          <div class="input-append row-fluid">
                            <?php
                              $image = array(
                                'src'    => 'images/uploads/'.$abouts->image,
                                'alt'    => 'Foto Baner Front',
                                'class'  => 'thumbnail small',
                                'width'  => '200',
                                'height' => '200'
                              );

                              echo img($image);
                            ?>
                            <input type="file" name="foto" class="spa1n6 fileinput" id="search-input">
                          </div>
                        </div>
                  </div>

                  <div class="form-row control-group row-fluid">
                    <label class="control-label span1" for="normal-field">Title</label>
                    <div class="controls span7">
                      <input type="text" name="title" id="normal-field" value="<?php echo $abouts->title; ?>" class="row-fluid">
                    </div>
                  </div>

                  <!-- keterangan full -->
                  <div class="form-row control-group row-fluid">
                    <label class="control-label span1" for="editor1">Description</label>
                    <div class="controls span7">
                      <textarea name="description" class="desc span12" rows="20"><?php echo $abouts->description; ?></textarea>
                    </div>
                  </div>

                  <input type="submit" class="btn" rel="tooltip" data-placement="top" value="Uodate" />
              </form>
            </div>
            </div>
        </div>
      </div>
    </div>
  </div>
