<div id="main">
  <div class="container">
  <div id="container2">


<div class="row-fluid">
    <div class="box gradient">

      <div class="title">

        <div class="title">
          <h5><a href="<?php echo site_url('form-html/pemberitahuan/formAdd'); ?>" class="btn btn-inverse" rel="tooltip" data-placement="top" data-original-title="View"><i class="gicon-plus icon-white"></i>Add Pemberitahuan</a></h5>
        </div><!-- End .title -->

        </div><!-- End .title -->

        <div class="content top">
          <table id="datatable_example" class="responsive table table-striped table-bordered" style="width:100%;margin-bottom:0; ">
            <thead>
              <tr>
                 <th style="width:20%;">Judul</th>
                 <th style="width:50%;">Tanggal</th>

                <th class="ms no_sort" style="width:11%;">Aksi</th>
              </tr>
            </thead>

            <tbody>
            <?php
              if($pembs == 'empty')
              {
                echo "";
              }
              else
              {
                foreach ($pembs as $value)
                {
        ?>
              <tr>
                <td><?php echo $value->judul; ?></td>
                <td><?php echo $value->tanggal; ?></td>

                <td class="ms">
                  <div class="btn-group1">
                    <?php
                    echo anchor('form-html/pemberitahuan/formEdit/'.$value->pemberitahuan_id, '<i class="gicon-edit"></i>', array('class' => 'btn btn-small', 'rel' => 'tooltip', 'data-placement' => 'left', 'data-original-title' => 'Edit'));
                    ?>

                    <?php
                    echo anchor('form-html/pemberitahuan/actionDelete/'.$value->pemberitahuan_id, '<i class="gicon-remove icon-white"></i>', array('class' => 'btn btn-inverse btn-small', 'rel' => 'tooltip', 'data-placement' => 'bottom', 'data-original-title' => 'Remove'));
                    ?>
                  </div>
                </td>
              </tr>
              <?php
                }
              }
              ?>

            </tbody>
          </table>
        </div><!-- End .content -->
</div> <!-- End box -->
</div> <!-- End .row-fluid -->


</div><!-- End #container -->
</div>
