<div id="main">
  <div class="container">
    <!-- container_top -->

    <!-- End container_top -->
    <div class="row-fluid">
      <div class="span12">
          <div class="box gradient">
            <div class="title">
              <h3>
              <i class="icon-book"></i><span>Edit Banner</span>
              </h3>
            </div>
            <div class="span7">
            <div class="content">
              <?php
              echo validation_errors('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>');
              $data = array('class' => 'form-horizontal row-fluid');
              echo form_open_multipart(site_url('form-html/banner/actionEdit'), $data);
              foreach ($banner as $value) { ?>

                  <!-- gambar -->
                  <input type="hidden" name="kode_banner" value="<?php echo $value->kode_banner; ?>" />
                  <input type="hidden" name="id_banner" value="<?php echo $value->banner_id; ?>" />
                  <div class="form-row control-group row-fluid">
                        <label class="control-label span3" for="search-input">Image</label>
                        <?php
                            $image = array(
                                'src'    => 'images/uploads/'.$value->image,
                                'alt'    => 'Foto Baner Front',
                                'class'  => 'thumbnail small',
                                'width'  => '200',
                                'height' => '200'
                            );

                            echo img($image);
                          

                            // $data = array('name' => 'foto', 'class' => 'spa1n6p fileinput', 'value' => $value->image);
                            // echo form_upload($data);
                          ?>
                          <div class="controls span7">
                            <div class="input-append row-fluid">
                              <input type="file" name="foto" class="spa1n6 fileinput" id="search-input">
                            </div>
                         </div>
                  </div>

                  <div class="form-row control-group row-fluid">
                    <label class="control-label span3" for="normal-field">Title</label>
                    <div class="controls span7">
                      <input type="text" name="title" id="normal-field" class="row-fluid" value="<?php echo $value->title; ?>">
                    </div>
                  </div>

                  <!-- keterangan full -->
                  <div class="form-row control-group row-fluid">
                    <label class="control-label span3" for="editor1">Description</label>
                    <div class="controls span7">
                      <textarea name="description" class="desc"><?php echo $value->description; ?></textarea>
                    </div>
                  </div>

                  <button type="submit" class="btn btn-md btn-default" rel="tooltip" data-placement="top" name="editProduct" data-original-title=".btn">
                    <span class="glyphicon glyphicon-edit"></span>&nbsp;Update Banner
                  </button>

                  <?php
                    echo anchor(site_url('form-html/banner'),'<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Kembali',
                      array('style' => 'text-decoration: none', 'class' => 'btn btn-default'));
                  ?>
              </form>
              <?php } ?>
            </div>
            </div>
        </div>
      </div>
    </div>
  </div>
  <div id="footer"><p>&copy; Bird Admin Template 2012</p>
    <span class="company_logo"><a href="http://www.pixelgrade.com"></a></span>
  </div> <!-- End #footer -->
</div>
