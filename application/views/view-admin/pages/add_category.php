<div id="main">
  <div class="container">
    <!-- container_top -->

    <!-- End container_top -->
    <div class="row-fluid">
      <div class="span12">
          <div class="box gradient">
            <div class="title">
              <h3>
              <i class="icon-book"></i><span>Tambah Kategori</span>
              </h3>
            </div>
            <div class="span7">
            <div class="content">
            <?php
              echo validation_errors('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>');
              $data = array('class' => 'form-horizontal row-fluid');
              echo form_open_multipart(site_url('form-html/category/actionAdd'), $data);
            ?>
                  <div class="form-row control-group row-fluid">
                    <label class="control-label span3" for="normal-field">Nama kategori</label>
                    <div class="controls span7">
                      <input type="text" name="cat_name" id="normal-field" class="row-fluid">
                    </div>
                  </div>
                   <div class="form-row control-group row-fluid">
                    <label class="control-label span3" for="normal-field">Deskripsi kategori</label>
                    <div class="controls span7">
                      <textarea " name="cat_desc" id="normal-field" class="row-fluid"></textarea>
                    </div>
                  </div>



                  <input type="submit" class="btn" rel="tooltip" data-placement="top" value="Tambah" />
                  <?php
                  echo anchor(site_url('form-html/category'),'<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Back',
                    array('style' => 'text-decoration: none', 'class' => 'btn btn-default'));
                ?>
              </form>
            </div>
            </div>
        </div>
      </div>
    </div>
  </div>
