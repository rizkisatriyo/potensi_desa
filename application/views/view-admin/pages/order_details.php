<div id="main">
  <div class="container">
    <div class="row-fluid">
      <div class="span12">
        <div class="box gradient">

          <div class="title print-area-1">
            <h3>
              <i class="icon-book"></i><span>Status : <?php echo ucwords($status->order_status); ?></span>
            </h3>
          </div>
          <div class="span6" class="print-area-1">
            <div class="content">
              <div class="form-row row-fluid">
              <h3><span>User Information</span></h3>
              <?php 
                foreach ($users as $key => $v_user) {
                  # code...
                ?>
                <table>
                    <tr>
                      <td>Order ID</td>
                      <td>: <span><strong><?php echo $v_user->order_id; ?></strong></span></td>
                    </tr>

                    <tr>
                      <td>Name</td>
                      <td>: <?php echo $v_user->full_name; ?></td>
                    </tr>

                    <tr>
                      <td>Email</td>
                      <td>: <?php echo $v_user->email; ?> </span></td>
                    </tr>

                    <tr>
                      <td>No Telp</td>
                      <td>: <?php echo $v_user->telephone; ?></td>
                    </tr>
                  
                </table>

                  <?php } ?>
               </div>
               </div>
          </div>

          <div class="span5" class="print-area-1">
            <div class="content">
							<h3><span>Delivery Address</span></h3>
              <?php foreach ($address as $key => $value) {
              ?>
                <table>
                    <tr>
                      <td>Address 1</td>
                      <td>: <span><strong><?php echo $value->address1; ?></strong></span></td>
                    </tr>

                    <tr>
                      <td>Address 2</td>
                      <td>: <?php echo $value->address2; ?></td>
                    </tr>

                    <tr>
                      <td>City</td>
                      <td>: <?php echo $value->city; ?> </span></td>
                    </tr>

                    <tr>
                      <td>Province</td>
                      <td>: <?php echo $value->county ?></td>
                    </tr>

                    <tr>
                      <td>Zip</td>
                      <td>: <?php echo $value->post_code; ?></td>
                    </tr>
                </table>
               </div>
               <?php }?>
            </div>
          </div>

					<!-- Mulai Tabel -->
					<div class="span11">
					<div class="content top" class="print-area-1" id="print-area-1">
						<h3><span>Order Summary</span></h3>
            <table id="" class="responsive table table-striped table-bordered" style="width:100%;margin-bottom:0; ">
            <thead>
            <tr>
              <th class="no_sort ">
                Product Name
              </th>
              <th class="no_sort">
                Qty
              </th>
              <th class="no_sort ue">
                Price/Item
              </th>
              <th class="ue no_sort">
                Total
              </th>
            </tr>
            </thead>

						<!-- tabel Body -->
            <tbody>
            <?php $total = 0; ?>
            <?php foreach ($orders as $key => $data) { ?>
            <tr>
              <td class=""><?php echo $data->item_name; ?></td>
              <td class=""><?php echo $data->item_quantity; ?></td>
              <td class="ms"><?php echo rupiah($data->item_price); ?></td>
              <td class="ms"><?php echo rupiah($data->item_total_price); ?></td>
            </tr>
            <?php
              $total += $data->item_total_price;
              }
            ?>
						<tr>
              <td class="" colspan="3">
                <span><strong>Total Pembayaran</strong></span>
              </td>
              <td class="ms"><?php echo rupiah($total); ?>
              </td>
            </tr>

            </tbody>
            </table>
					</div>


            <div class="row-fluid control-group">
              <div class="pull-left span6" action="#">
                <div class=" ">
                  <?php
                  echo anchor(site_url('form-html/order'),'<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Kembali', array('style' => 'text-decoration: none', 'class' => 'btn btn-default'));
                ?>
                <a class="no-print" href="javascript:printDiv('print-area-1');">Print</a>
                </div>
              </div>
            </div>
            <!-- End row-fluid -->
          </div>

      </div>
    </div>
  </div>

  <script type="text/javascript">
    function printDiv(elementId) 
    {
       var a = document.getElementById('printing-css').value;
       var b = document.getElementById(elementId).innerHTML;
       window.frames["print_frame"].document.title = document.title;
       window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
       window.frames["print_frame"].window.focus();
       window.frames["print_frame"].window.print();
    }
  </script>

  <textarea id="printing-css" style="display:none;">html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:before,blockquote:after,q:before,q:after{content:'';content:none}table{border-collapse:collapse;border-spacing:0}body{font:normal normal .8125em/1.4 Arial,Sans-Serif;background-color:white;color:#333}strong,b{font-weight:bold}cite,em,i{font-style:italic}a{text-decoration:none}a:hover{text-decoration:underline}a img{border:none}abbr,acronym{border-bottom:1px dotted;cursor:help}sup,sub{vertical-align:baseline;position:relative;top:-.4em;font-size:86%}sub{top:.4em}small{font-size:86%}kbd{font-size:80%;border:1px solid #999;padding:2px 5px;border-bottom-width:2px;border-radius:3px}mark{background-color:#ffce00;color:black}p,blockquote,pre,table,figure,hr,form,ol,ul,dl{margin:1.5em 0}hr{height:1px;border:none;background-color:#666}h1,h2,h3,h4,h5,h6{font-weight:bold;line-height:normal;margin:1.5em 0 0}h1{font-size:200%}h2{font-size:180%}h3{font-size:160%}h4{font-size:140%}h5{font-size:120%}h6{font-size:100%}ol,ul,dl{margin-left:3em}ol{list-style:decimal outside}ul{list-style:disc outside}li{margin:.5em 0}dt{font-weight:bold}dd{margin:0 0 .5em 2em}input,button,select,textarea{font:inherit;font-size:100%;line-height:normal;vertical-align:baseline}textarea{display:block;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}pre,code{font-family:"Courier New",Courier,Monospace;color:inherit}pre{white-space:pre;word-wrap:normal;overflow:auto}blockquote{margin-left:2em;margin-right:2em;border-left:4px solid #ccc;padding-left:1em;font-style:italic}table[border="1"] th,table[border="1"] td,table[border="1"] caption{border:1px solid;padding:.5em 1em;text-align:left;vertical-align:top}th{font-weight:bold}table[border="1"] caption{border:none;font-style:italic}.no-print{display:none}</textarea>
  <iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>