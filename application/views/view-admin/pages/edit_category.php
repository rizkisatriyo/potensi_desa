<div id="main">
  <div class="container">
    <!-- End container_top -->
    <div class="row-fluid">
      <div class="span12">
          <div class="box gradient">
            <div class="title">
              <h3>
              <i class="icon-book"></i><span>Edit kategori</span>
              </h3>
            </div>
            <div class="span7">
            <div class="content">
              <?php
              echo validation_errors('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>');
              echo $pesan;
              $data = array('class' => 'form-horizontal row-fluid');
              echo form_open_multipart(site_url('form-html/category/actionEdit'), $data);
              foreach ($category as $value) { ?>
                <input type="hidden" id="normal-field" name="cat_id" class="row-fluid" value="<?php echo $value->cat_id; ?>">
                <div class="form-row control-group row-fluid">
                  <label class="control-label span3" for="normal-field">Nama Kategori</label>
                  <div class="controls span7">
                    <input type="text" id="normal-field" name="cat_name" class="row-fluid" value="<?php echo $value->cat_name; ?>">
                  </div>
                </div>
                <div class="form-row control-group row-fluid">
                    <label class="control-label span3" for="normal-field">Deskripsi kategori</label>
                    <div class="controls span7">
                      <textarea " name="cat_desc" id="normal-field" class="row-fluid"><?php echo $value->cat_desc; ?></textarea>
                    </div>
                  </div>

                <button type="submit" class="btn btn-md btn-default" rel="tooltip" data-placement="top" name="editCategory" data-original-title=".btn">
                  <span class="glyphicon glyphicon-edit"></span>&nbsp;Simpan
                </button>

                <?php
                  echo anchor(site_url('form-html/category'),'<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Back',
                    array('style' => 'text-decoration: none', 'class' => 'btn btn-default'));
                ?>
              </form>
              <?php } ?>
            </div>
            </div>
        </div>
        <!-- End .box -->

      </div>
      <!-- End .span8 -->
    </div>
  </div>
