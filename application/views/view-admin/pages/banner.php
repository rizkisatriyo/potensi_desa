<div id="main">
  <div class="container">
    <!-- container_top -->

    <!-- End container_top -->
    <div class="row-fluid">
      <div class="box gradient span12">

      <div class="title">
            <div class="row-fluid">
                <h3>
                   Setting
                </h3>
            </div>
        </div><!-- End .title -->
        <div class="content">
          <ul id="tabExample1" class="nav nav-tabs">
                                    <li class="active"><a href="#home" data-toggle="tab">Banner Home</a></li>
                                    <li><a href="#product" data-toggle="tab">Banner Product</a></li>
                                    <li><a href="#promotion" data-toggle="tab">Banner Promotion</a></li>
           </ul>
           <div class="tab-content">

                                    <div class="tab-pane fade in active" id="home">
                                      <!-- gambar -->

                                      <div class="content top">
                                        <h5><a href="<?php echo site_url('form-html/banner/formAddHome'); ?>" class="btn btn-inverse" rel="tooltip" data-placement="top" data-original-title="View"><i class="gicon-plus icon-white"></i>Add Banner</a></h5>
                                        <table id="" class="responsive table table-striped table-bordered" style="width:100%;margin-bottom:0; ">
                                          <thead>
                                            <tr>
                                               <th class="jv no_sort" style="width:15%;">Image</th>
                                               <th style="width:15%;">Title</th>
                                               <th style="width:12%;">Description</th>
                                              <th class="ms no_sort" style="width:5%;">Actions</th>
                                            </tr>
                                          </thead>

                                          <tbody>
                                          <?php
                                            if($banners == 'empty')
                                            {
                                              echo " Sorry - No category found";
                                            }
                                            else
                                            {
                                              foreach ($banners as $value)
                                              {
                                      ?>
                                            <tr>
                                              <td>
                                                <?php
                                                  $image = array(
                                                      'src'    => 'images/uploads/'.$value->image,
                                                      'alt'    => 'Foto Baner Front',
                                                      'class'  => 'thumbnail small',
                                                      'width'  => '200',
                                                      'height' => '200'
                                                  );

                                                  echo img($image);
                                                ?>
                                                 </td>

                                              <td><?php echo $value->title; ?></td>
                                              <td><?php echo $value->description; ?></td>

                                              <td class="ms">
                                                <div class="btn-group1">
                                                  <?php
                                                  echo anchor('form-html/banner/formEdit/'.$value->banner_id.'/'.$value->kode_banner, '<i class="gicon-edit"></i>', array('class' => 'btn btn-small', 'rel' => 'tooltip', 'data-placement' => 'left', 'data-original-title' => 'Edit'));
                                                  ?>

                                                  <?php
                                                  echo anchor('form-html/banner/actionDelete/'.$value->banner_id, '<i class="gicon-remove icon-white"></i>', array('class' => 'btn btn-inverse btn-small', 'rel' => 'tooltip', 'data-placement' => 'bottom', 'data-original-title' => 'Remove'));
                                                  ?>
                                                </div>
                                              </td>
                                            </tr>
                                            <?php
                                              }
                                            }
                                            ?>

                                          </tbody>
                                        </table>
                                      </div>


                                      </div>

                                      <!-- product -->
                                      <div class="tab-pane fade " id="product">
                                        <!-- gambar -->

                                        <div class="content top">
                                          <h5><a href="<?php echo site_url('form-html/banner/formAddProduct'); ?>" class="btn btn-inverse" rel="tooltip" data-placement="top" data-original-title="View"><i class="gicon-plus icon-white"></i>Add Banner</a></h5>
                                          <table id="" class="responsive table table-striped table-bordered" style="width:100%;margin-bottom:0; ">
                                            <thead>
                                              <tr>
                                                 <th class="jv no_sort" style="width:15%;">Image</th>
                                                 <th style="width:15%;">Title</th>
                                                 <th style="width:12%;">Description</th>
                                                <th class="ms no_sort" style="width:5%;">Actions</th>
                                              </tr>
                                            </thead>

                                            <tbody>
                                            <?php
                                              foreach ($bannersProduct as $product)
                                              {
                                              ?>
                                              <tr>
                                                <td>
                                                <?php
                                                  $image = array(
                                                      'src'    => 'images/uploads/'.$product->image,
                                                      'alt'    => 'Foto Baner Front',
                                                      'class'  => 'thumbnail small',
                                                      'width'  => '200',
                                                      'height' => '200'
                                                  );

                                                  echo img($image);
                                                ?>
                                                   </td>

                                               <td><?php echo $product->title; ?></td>
                                              <td><?php echo $product->description; ?></td>

                                                <td class="ms">
                                                  <div class="btn-group1">
                                                    <?php
                                                    echo anchor('form-html/banner/formEdit/'.$product->banner_id.'/'.$product->kode_banner, '<i class="gicon-edit"></i>', array('class' => 'btn btn-small', 'rel' => 'tooltip', 'data-placement' => 'left', 'data-original-title' => 'Edit'));
                                                    ?>

                                                    <?php
                                                    echo anchor('form-html/banner/actionDelete/'.$product->banner_id, '<i class="gicon-remove icon-white"></i>', array('class' => 'btn btn-inverse btn-small', 'rel' => 'tooltip', 'data-placement' => 'bottom', 'data-original-title' => 'Remove'));
                                                    ?>
                                                  </div>
                                                </td>
                                              </tr>
                                              <?php } ?>
                                            </tbody>
                                          </table>
                                        </div>
                                        </div>

                                      <!-- product single -->
                                      <div class="tab-pane fade" id="promotion">
                                        <div class="form-row control-group row-fluid">
                                        <?php echo form_open_multipart(site_url('form-html/banner/actionEdit'));
                                        ?>
                                        <input type="hidden" name="kode_banner" value="3" />
                                        <input type="hidden" name="id_banner" value="<?php echo $bannersSingle->banner_id; ?>" />
                                        <label class="control-label span3" for="max-length">image</label>
                                          <div class="controls span5">
                                             <?php
                                                  $image = array(
                                                      'src'    => 'images/uploads/'.$bannersSingle->image,
                                                      'alt'    => 'Foto Baner Front',
                                                      'class'  => 'thumbnail small',
                                                      'width'  => '200',
                                                      'height' => '200'
                                                  );

                                                  echo img($image);
                                                ?>
                                            <input type="file" name="foto" id="normal-field" class="row-fluid" value="">
                                          </div>
                                        </div>
                                        <input type="submit" class="btn" rel="tooltip" data-placement="top" value="Update Banner" />
                                        </form>
                                </div>
        </div> <!-- End .content -->
    </div>
    </div>
  </div>
