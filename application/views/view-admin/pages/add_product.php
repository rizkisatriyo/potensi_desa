<div id="main">
  <div class="container">
    <!-- container_top -->

    <!-- End container_top -->
    <div class="row-fluid">
      <div class="span12">
          <div class="box gradient">
            <div class="title">
              <h3>
              <i class="icon-book"></i><span>Tambah Potensi</span>
              </h3>
            </div>
            <div class="span7">
            <div class="content">
            <?php
              echo validation_errors('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>');
              echo $pesan;
              $data = array('class' => 'form-horizontal row-fluid');
              echo form_open_multipart(site_url('form-html/product/actionAdd'), $data);
            ?>
                  <div class="form-row control-group row-fluid">
                    <label class="control-label span3" for="normal-field">Nama Potensi</label>
                    <div class="controls span7">
                      <input type="text" name="item_name" id="normal-field" class="row-fluid">
                    </div>
                  </div>

                  <!-- category -->
                  <div class="form-row control-group row-fluid">
                      <label class="control-label span3" for="default-select">Kategori</label>
                      <div class="controls span7">
                        <select data-placeholder="" class="chzn-select" id="default-select" name="id_category">
                          <?php
                            foreach ($category as $value)
                            {
                          ?>
                          <option value="<?php echo $value->cat_id; ?>"><?php echo $value->cat_name; ?></option>
                          <?php
                            }
                          ?>
                        </select>
                      </div>
                  </div>

                  <!-- gambar -->
                  <div class="form-row control-group row-fluid">
                        <label class="control-label span3" for="search-input">Gambar</label>
                        <div class="controls span7">
                          <div class="input-append row-fluid">
                            <input type="file" name="foto" class="spa1n6 fileinput" id="search-input">
                          </div>
                        </div>
                  </div>

                  <!-- keterangan penden -->
                  <div class="form-row control-group row-fluid">
                        <label class="control-label span3" for="desc">Keterangan</label>
                        <div class="controls span7">
                          <textarea name="shortdesc" class="desc"></textarea>
                        </div>
                  </div>

                  <input type="submit" class="btn" rel="tooltip" data-placement="top" value="Tambah" />
                  <?php
                  echo anchor(site_url('form-html/product'),'<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Kembali',
                    array('style' => 'text-decoration: none', 'class' => 'btn btn-default'));
                ?>
              </form>
            </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
