<div id="main">
  <div class="container">
  <div id="container2">


<div class="row-fluid">
    <div class="box gradient">

      <div class="title">

        <div class="title">
          <h5><a href="<?php echo site_url('form-html/product/formAdd'); ?>" class="btn btn-inverse" rel="tooltip" data-placement="top" data-original-title="View"><i class="gicon-plus icon-white"></i>Tambahkan Potensi</a></h5>
        </div><!-- End .title -->

        </div><!-- End .title -->

        <div class="content top">
          <table id="datatable_example" class="responsive table table-striped table-bordered" style="width:100%;margin-bottom:0; ">
            <thead>
              <tr>
                 <th class="jv no_sort">Image</th>
                 <th style="width:15%;">Name</th>
                 <th style="width:13%;">Category</th>
                 <th style="width:32%;">Description</th>
                <th class="ms no_sort" style="width:17%;">Actions</th>
              </tr>
            </thead>

            <tbody>
            <?php
              if($products == 'empty')
              {
                echo " Sorry - No category found";
              }
              else
              {
                foreach ($products as $value)
                {
        ?>
              <tr>
                <td><img class="thumbnail small" <img src="<?php echo base_url().$value->thumbnail; ?>"></td>

                <td><?php echo $value->item_name; ?></td>
                <td><?php echo $value->cat_name; ?></td>
                <td><?php echo $value->item_desc_short; ?></td>

                <td class="ms">
                  <div class="btn-group1">
                    <?php
                    echo anchor('form-html/product/formEdit/'.$value->item_id, '<i class="gicon-edit"></i>', array('class' => 'btn btn-small', 'rel' => 'tooltip', 'data-placement' => 'left', 'data-original-title' => 'Edit'));
                    ?>

                    <?php
                    echo anchor('form-html/product/actionDelete/'.$value->item_id, '<i class="gicon-remove icon-white"></i>', array('class' => 'btn btn-inverse btn-small', 'rel' => 'tooltip', 'data-placement' => 'bottom', 'data-original-title' => 'Remove'));
                    ?>
                  </div>
                </td>
              </tr>
              <?php
                }
              }
              ?>

            </tbody>
          </table>
        </div><!-- End .content -->
</div> <!-- End box -->
</div> <!-- End .row-fluid -->


</div><!-- End #container -->
</div>
