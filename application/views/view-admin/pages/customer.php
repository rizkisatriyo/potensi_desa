<div id="main">
  <div class="container">

  <div id="container2">

    <div class="row-fluid">
        <?php echo $pesan; ?>
        <div class="box gradient">
            <div class="title">
              <h5>Customer List</h5>
            </div><!-- End .title -->
            <div class="content top">
              <table id="datatable_example" class="responsive table table-striped table-bordered" style="width:100%;margin-bottom:0; ">
                <thead>
                  <tr>
                     <th>Customer No</th>
                     <th>Name</th>
                     <th>Email</th>
                     <th>Date Registered</th>
                    <th class="ms no_sort" style="width:15%;">Actions</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                  if($customers == 'empty')
                  {
                    // echo "Data Kosong";
                  }
                  else
                  {
                    foreach ($customers as $value)
                    {
                      ?>
                  <tr>
                    <td><?php echo $value->user_id; ?></td>
                    <td><?php echo $value->full_name; ?></td>
                    <td><?php echo $value->email; ?></td>
                    <td><?php echo $value->date_joined; ?></td>

                    <td class="ms">
                      <div class="btn-group1">
                        <?php
                        echo anchor(site_url('form-html/customer/customerDetails/'.$value->user_id), '<i class="gicon-eye-open"></i>', array('class' => 'btn btn-small', 'rel' => 'tooltip', 'data-placement' => 'left', 'data-original-title' => 'Edit'));
                        ?>

                        <?php
                        echo anchor(site_url('form-html/customer/actionDelete/'.$value->user_id), '<i class="gicon-remove icon-white"></i>', array('class' => 'btn btn-inverse', 'rel' => 'tooltip', 'data-placement' => 'bottom', 'data-original-title' => 'Remove'));
                        ?>
                        <?php
                        // echo anchor('form-html/customer/formEdit/'.$value->user_id, '<i class="gicon-shopping-cart"></i>', array('class' => 'btn btn-small', 'rel' => 'tooltip', 'data-placement' => 'left', 'data-original-title' => 'Edit'));
                        ?>
                        </div>
                    </td>
                  </tr>
                  <?php
                    }
                  }
                  ?>

                </tbody>
              </table>
            </div><!-- End .content -->
        </div> <!-- End box -->
    </div> <!-- End .row-fluid -->
  </div><!-- End #container -->
</div>
