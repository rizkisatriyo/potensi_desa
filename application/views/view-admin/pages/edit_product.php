<div id="main">
  <div class="container">
    <!-- End container_top -->
    <div class="row-fluid">
      <div class="span12">
          <div class="box gradient">
            <div class="title">
              <h3>
              <i class="icon-book"></i><span>Edit Potensi</span>
              </h3>
            </div>
            <div class="span7">
            <div class="content">
              <?php
              echo validation_errors('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>');
              echo $pesan;
              $data = array('class' => 'form-horizontal row-fluid');
              echo form_open_multipart(site_url('form-html/product/actionEdit'), $data);
              foreach ($product as $value) { ?>
                <input type="hidden" id="normal-field" name="id_product" class="row-fluid" value="<?php echo $value->item_id; ?>">
                <div class="form-row control-group row-fluid">
                  <label class="control-label span3" for="normal-field">Nama Potensi</label>
                  <div class="controls span7">
                    <input type="text" id="normal-field" name="item_name" class="row-fluid" value="<?php echo $value->item_name; ?>">
                  </div>
                </div>
                <div class="form-row control-group row-fluid">
                <label class="control-label span3" for="default-select">Kategori</label>
                <div class="controls span7">
                  <select name="id_category" class="chzn-select" id="default-select">
                    <?php
                      foreach ($category as $cat)
                      {
                    ?>
                        <option value="<?php echo $cat->cat_id; ?>" <?php echo $cat->cat_id == $value->cat_id ? 'selected' : ''; ?>><?php echo $cat->cat_name; ?></option>
                    <?php
                      }
                    ?>
                  </select>
                </div>
                </div>
                <div class="form-row control-group row-fluid">
                  <label class="control-label span3" for="search-input">Image</label>
                  <div class="controls span7">
                    <div class="input-append row-fluid">
                    <?php
                        $image = array(
                          'src'    => base_url().'/'.$value->medium_image,
                          'class'  => 'img-rounded img-responsive',
                          'width'  => '150',
                          'height' => '150'
                        );

                        echo img($image);
                        echo "<br/>";

                        $data = array('name' => 'foto', 'class' => 'spa1n6p fileinput', 'value' => $value->medium_image);
                        echo form_upload($data);
                      ?>

                      <!-- <input type="file" class="spa1n6 fileinput" id="search-input"> -->
                    </div>
                  </div>
                </div>
                <div class="form-row control-group row-fluid">
                  <label class="control-label span3" for="desc">Deskripsi</label>
                  <div class="controls span7">
                    <textarea name="shortdesc" class="desc" rows="5" ><?php echo $value->item_desc_short; ?></textarea>
                  </div>
                </div>
                <button type="submit" class="btn btn-md btn-default" rel="tooltip" data-placement="top" name="editProduct" data-original-title=".btn">
                  <span class="glyphicon glyphicon-edit"></span>&nbsp;Simpan
                </button>

                <?php
                  echo anchor(site_url('form-html/product'),'<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Kembali',
                    array('style' => 'text-decoration: none', 'class' => 'btn btn-default'));
                ?>
              </form>
              <?php } ?>
            </div>
            </div>
        </div>
        <!-- End .box -->

      </div>
      <!-- End .span8 -->
    </div>
  </div>
