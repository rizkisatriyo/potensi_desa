<div id="main">
  <div class="container">
    <!-- container_top -->

    <!-- End container_top -->
    <div class="row-fluid">
      <div class="span12">
          <div class="box gradient">
            <div class="title">
              <h3>
              <i class="icon-book"></i><span>Setting Admin</span>
              </h3>
            </div>
            <div class="span7">
            <div class="content">
            <?php
              echo validation_errors('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>');
              echo $this->session->flashdata('msg'); 
              $data = array('class' => 'form-horizontal row-fluid');
              echo form_open_multipart(site_url('form-html/admin/actionEdit'), $data);
              ?>
                  <div class="form-row control-group row-fluid">
                    <label class="control-label span3" for="normal-field">Username</label>
                    <div class="controls span7">
                      <input type="text" value="<?php echo $admins->username; ?>" placeholder="Harus Diisi" name="username" id="normal-field" class="row-fluid">
                    </div>
                  </div>

                  <!-- password lama -->
                  <div class="form-row control-group row-fluid">
                    <label class="control-label span3" for="normal-field">Password Lama</label>
                    <div class="controls span7">
                      <input type="password" name="passwordold" placeholder="Kosongkan Password Jika Tidak Ada Perubahan" name="password" id="normal-field" class="row-fluid">
                    </div>
                  </div>

                  <!-- password baru-->
                  <div class="form-row control-group row-fluid">
                    <label class="control-label span3" for="normal-field">Password Baru</label>
                    <div class="controls span7">
                      <input type="password" name="passwordnew" placeholder="Kosongkan Password Jika Tidak Ada Perubahan" name="password" id="normal-field" class="row-fluid">
                    </div>
                  </div>

                    <!-- password confirm baru-->
                  <div class="form-row control-group row-fluid">
                    <label class="control-label span3" for="normal-field">Confirm Password Baru</label>
                    <div class="controls span7">
                      <input type="password" name="passwordconfirm" placeholder="Harus Diisi Jika Ada Perubahan Password" name="password" id="normal-field" class="row-fluid">
                    </div>
                  </div>


                  <input type="submit" class="btn" rel="tooltip" data-placement="top" value="Save" />
                   <?php
                  echo anchor(site_url('form-html/admin'),'<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Kembali',
                    array('style' => 'text-decoration: none', 'class' => 'btn btn-default'));
                  ?>
              </form>
            </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
