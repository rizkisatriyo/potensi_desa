<div id="main">
  <div class="container">
    <!-- container_top -->

    <!-- End container_top -->
    <div class="row-fluid">
      <div class="span12">
          <div class="box gradient">
            <div class="title">
              <h3>
              <i class="icon-book"></i><span>Edit Pembertahuan</span>
              </h3>
            </div>
            <div class="span7">
            <div class="content">
            <?php
              echo validation_errors('<div class="alert alert-danger fade in"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>', '</div>');
              $data = array('class' => 'form-horizontal row-fluid');
              echo form_open_multipart(site_url('form-html/pemberitahuan/actionAdd'), $data);
              foreach ($p as $value) { ?>

                  <div class="form-row control-group row-fluid">
                    <label class="control-label span3" for="normal-field">Judul</label>
                    <div class="controls span7">
                      <input type="hidden" name="pemberitahuan_id" value="<?php echo $value->pemberitahuan_id ?>">
                      <input type="text" name="judul" id="normal-field" class="row-fluid" value="<?php echo $value->judul ?>">
                    </div>
                  </div>
                   <div class="form-row control-group row-fluid">
                    <label class="control-label span3" for="normal-field">Tanggal</label>
                    <div class="controls span7">
                      <input type="text" name="tanggal" id="normal-field" class="row-fluid" value="<?php echo $value->tanggal ?>">
                    </div>
                  </div>



                  <input type="submit" class="btn" rel="tooltip" data-placement="top" value="Simpan" />
                  <?php
                  echo anchor(site_url('form-html/pemberitahuan'),'<span class="glyphicon glyphicon-arrow-left"></span>&nbsp;Back',
                    array('style' => 'text-decoration: none', 'class' => 'btn btn-default'));
                ?>
              </form>
                            <?php } ?>

            </div>
            </div>
        </div>
      </div>
    </div>
  </div>
