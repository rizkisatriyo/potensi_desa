<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Page-Enter" content="blendTrans(Duration=0.2)">
		<meta http-equiv="Page-Exit" content="blendTrans(Duration=0.2)">
		<title>Home - Admin Dashboard</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/lib-admin/css/images/favicon.png">

		<!-- css -->
		<link href="<?php echo base_url(); ?>assets/lib-admin/js/plugins/chosen/chosen/chosen.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/lib-admin/css/twitter/bootstrap.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/lib-admin/css/base.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/lib-admin/css/twitter/responsive.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/lib-admin/css/jquery-ui-1.8.23.custom.css" rel="stylesheet">
		<script src="<?php echo base_url(); ?>assets/lib-admin/js/plugins/modernizr.custom.32549.js"></script>
		<script src="http://static.ak.fbcdn.net/connect.php/js/FB.Share" type="text/javascript"></script>

		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
      	<![endif]-->
	</head>
<body>
	<!-- loading -->
	<div id="loading"><img src="<?php echo base_url(); ?>assets/lib-admin/img/ajax-loader.gif"></div>

	<div id="responsive_part">
		<!-- logo -->
		<div class="logo"><a href="index.html"></a></div>
		<ul class="nav responsive">
			<li>
				<btn class="btn btn-la1rge btn-i1nfo responsive_menu icon_item" data-toggle="collapse" data-target="#sidebar"><i class="icon-reorder"></i></btn>
			</li>
		</ul>
	</div>

	<!-- sidebar -->
	<div id="sidebar" class="collapse">
		<div class="logo"><a href="index.html"></a></div>
		<ul id="sidebar_menu" class="navbar nav nav-list sidebar_box">
			 <li class="accordion-group <?php echo $this->uri->segment(2) == 'dashboard' ? 'active' : ''; ?>">
				<a class="dashboard" href="<?php echo site_url('form-html/dashboard'); ?>"><img src="<?php echo base_url(); ?>assets/lib-admin/img/menu_icons/dashboard.png">Dashboard</a>
			</li>
			<li class="accordion-group <?php echo $this->uri->segment(2) == 'category' ? 'active' : ''; ?>">
				<a class="widgets" href="<?php echo site_url('form-html/category'); ?>"><img src="<?php echo base_url(); ?>assets/lib-admin/img/menu_icons/others.png">Kategori Potensi</a>
			</li>
			<li class="accordion-group <?php echo $this->uri->segment(2) == 'product' ? 'active' : ''; ?>">
				<a class="widgets" href="<?php echo site_url('form-html/product'); ?>"><img src="<?php echo base_url(); ?>assets/lib-admin/img/menu_icons/docs.png">Potensi</a>
			</li>
			<li class="accordion-group <?php echo $this->uri->segment(2) == 'pemberitahuan' ? 'active' : ''; ?>">
				<a class="widgets" href="<?php echo site_url('form-html/pemberitahuan'); ?>"><img src="<?php echo base_url(); ?>assets/lib-admin/img/menu_icons/docs.png">Pemberitahuan</a>
			</li>
			<li class="accordion-group <?php echo $this->uri->segment(2) == 'blog' ? 'active' : ''; ?>">
				<a class="widgets" href="<?php echo site_url('form-html/blog'); ?>"><img src="<?php echo base_url(); ?>assets/lib-admin/img/menu_icons/tables.png">Berita</a>
			</li>
			<li class="accordion-group <?php echo $this->uri->segment(2) == 'pages' ? 'active' : ''; ?>">
			    <a class="accordion-toggle widgets collapsed" data-toggle="collapse" data-parent="#sidebar_menu" href="#collapse2">
			    <img src="<?php echo base_url(); ?>assets/lib-admin/img/menu_icons/tables.png">Halaman</a>
				    <ul id="collapse2" class="accordion-body collapse">
				      <li class="accordion-group"><a href="<?php echo site_url('form-html/pages/showAbout'); ?>">Visi Misi</a></li>
				      <li class="accordion-group"><a href="<?php echo site_url('form-html/pages/showProfil'); ?>">Profil </a></li>
				    </ul>
	   		</li>
<!-- 			<li class="accordion-group <?php echo $this->uri->segment(2) == 'banner' ? 'active' : ''; ?>">
				<a class="widgets" href="<?php echo site_url('form-html/banner'); ?>"><img src="<?php echo base_url(); ?>assets/lib-admin/img/menu_icons/gallery.png">Banner</a>
			</li>
 -->			<li class="accordion-group <?php echo $this->uri->segment(2) == 'setting' ? 'active' : ''; ?>">
				<a class="widgets" href="<?php echo site_url('form-html/setting'); ?>"><img src="<?php echo base_url(); ?>assets/lib-admin/img/menu_icons/settings.png">Settings</a>
			</li>
		</ul>
	</div>
