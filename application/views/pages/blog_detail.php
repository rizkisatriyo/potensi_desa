<?php include("header_pages.php"); ?>
		<!-- HEADER BG : begin -->
		<div class="header-bg">

			<!-- HEADER IMAGE : begin -->
			<!-- To add more images just copy and edit elements with "image-layer" class (see home-2.html template for example)
			Change autoplay speed with "data-autoplay" attribute (in seconds), works only if there are more than one image -->
			<div class="header-image" data-autoplay="8">
				<div class="image-layer" style="background-image: url( '<?= base_url().'assets/template/frontend/' ?>images/background.jpg' );"></div>
				<!-- div class="image-layer" style="background-image: url( 'images/header-02.jpg' );"></div -->
			</div>
			<!-- HEADER IMAGE : begin -->

		</div>
		<!-- HEADER BG : end -->
		<!-- CORE : begin -->
		<div id="core">
			<div class="c-container">
				<div class="row">

					<!-- MIDDLE COLUMN : begin -->
					<div class="large-column col-md-6 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header" class="m-has-breadcrumbs">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1><?php echo $blog_data->blog_title; ?></h1>
							</div>
							<!-- PAGE TITLE : end -->

							<!-- BREADCRUMBS : begin -->
							<div class="breadcrumbs">
								<ul>
									<li class="home"><a href="index-2.html">Home</a></li>
									<li><a href="post-list.html">News</a></li>
									<li><?php echo $blog_data->blog_title; ?></li>
								</ul>
							</div>
							<!-- BREADCRUMBS : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">

								<!-- POST DETAIL PAGE : begin -->
								<div class="post-page post-detail-page">

									<!-- POST : begin -->
									<div class="post">
										<div class="post-inner c-content-box m-no-padding">

											<!-- POST IMAGE : begin -->
											<div class="post-image">
												<?php
									                  	$image = array(
									                    	'src'    => 'images/uploads/'.$blog_data->blog_image,
									                      	'alt'    => 'Foto Blog',
									                      	'width'	 => '400px',
									                      	'height' => 'auto',
									                    );

									                  echo img($image);
									                ?>
											</div>
											<!-- POST IMAGE : end -->

											<!-- POST CORE : begin -->
											<div class="post-core">

												<!-- POST CONTENT : begin -->
												<div class="post-content">
													<div class="post-content-inner">

														<?php echo $blog_data->blog_desc; ?>

													</div>
												</div>
												<!-- POST CONTENT : end -->

											</div>
											<!-- POST CORE : end -->

											<!-- POST FOOTER : begin -->
											<div class="post-footer">
												<div class="post-footer-inner">

													<!-- POST INFO : begin -->
													<div class="post-info">
														<i class="fa fa-clock-o" aria-hidden="true"></i>

														<!-- POST DATE : begin -->
														<span class="post-date">
															<?php
																$bulan   = date("M", strtotime($blog_data->blog_date));
																$blog_date = date("d", strtotime($blog_data->blog_date));
																$tahun   = date("Y", strtotime($blog_data->blog_date));
															?>
															<?php echo $bulan.". ".$blog_date." ".$tahun; ?>
														</span>
														<!-- POST DATE : end -->

														<!-- POST COMMENTS : begin -->
														<span class="post-comments">
															<a href="post-detail.html#comments">Comments (0)</a>
														</span>
														<!-- POST COMMENTS : end -->

													</div>
													<!-- POST INFO : end -->

													<!-- POST TAGS : begin -->
													
													<!-- POST TAGS : end -->

												</div>
											</div>
											<!-- POST FOOTER : end -->

										</div>
									</div>
									<!-- POST : end -->

									<!-- POST NAVIGATION : begin -->
									<div class="post-navigation">
										<div class="c-content-box">
											<ul>

												

											</ul>
										</div>
									</div>
									<!-- POST NAVIGATION : end -->

								</div>
								<!-- POST DETAIL PAGE : end -->

							</div>
						</div>
						<!-- PAGE CONTENT : end -->

						<hr class="c-separator m-margin-top-small m-margin-bottom-small m-transparent hidden-lg hidden-md">

					</div>
					<!-- MIDDLE COLUMN : end -->

					<!-- LEFT COLUMN : begin -->
					<div class="left-column col-md-3 col-md-pull-6">

						<!-- SIDE MENU : begin -->
						<?php include("application/views/menu.php"); ?>

						<!-- SIDE MENU : end -->

						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<?php include("application/views/widget/widget_potensi.php"); ?>

								<!-- CATEGORIES WIDGET : begin -->
								<div class="widget categories-widget">
									<div class="widget-inner">
										<h3 class="widget-title">
										<i class="fa fa-list-alt" aria-hidden="true"></i>  Categories</h3>
										<div class="widget-content">
											<?php if($categories=='empty')
												echo " Sorry - No category found";
												else
												{

											?>
											<ul>
												<?php foreach($categories as $category){?>
												<li><a href="<?php echo base_url();?>index.php/category/<?php echo $category->cat_id;?>/<?php echo url_title(strtolower($category->cat_name));?>"><?php echo $category->cat_name;?></a>
												</li>
												<?php } ?>
											</ul>
											<?php } ?>
										</div>
									</div>
								</div>
								<!-- CATEGORIES WIDGET : end -->

								<!-- POSTS WIDGET : begin -->
								<div class="widget posts-widget">
									<div class="widget-inner">
										<h3 class="widget-title"><i class="fa fa-book" aria-hidden="true"></i>  Berita Terbaru</h3>
										<div class="widget-content">
											<ul class="post-list">

												<!-- POST : begin -->
												<?php
													foreach ($blog as $key => $blog_data) 
													{
												?>
												<li class="post m-active">
													<div class="post-inner">
														<h4 class="post-title"><a href="<?php echo base_url();?>index.php/front/blogDetails/<?php echo $blog_data->blog_id; ?>/<?php echo $blog_data->blog_title; ?>"><?php echo $blog_data->blog_title; ?></a></h4>
														<span class="post-date">
															<?php
																$bulan   = date("M", strtotime($blog_data->blog_date));
																$blog_date = date("d", strtotime($blog_data->blog_date));
																$tahun   = date("Y", strtotime($blog_data->blog_date));
															?>
														<?php echo $bulan.". ".$blog_date." ".$tahun; ?>



														</span>
													</div>
												</li>
												<!-- POST : end -->
												<?php } ?>
											</ul>
											<p class="show-all-btn"><a href="post-list.html">See All Notices</a></p>
										</div>
									</div>
								</div>
								<!-- POSTS WIDGET : end -->
							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->
				</div>
			</div>
		</div>		
			<?php include("application/views/footer.php"); ?>