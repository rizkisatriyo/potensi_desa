<?php include("header_pages.php"); ?>
		<!-- HEADER BG : begin -->
		<div class="header-bg">

			<!-- HEADER IMAGE : begin -->
			<!-- To add more images just copy and edit elements with "image-layer" class (see home-2.html template for example)
			Change autoplay speed with "data-autoplay" attribute (in seconds), works only if there are more than one image -->
			<div class="header-image" data-autoplay="8">
				<div class="image-layer" style="background-image: url( '<?= base_url().'assets/template/frontend/' ?>images/background.jpg' );"></div>
				<!-- div class="image-layer" style="background-image: url( 'images/header-02.jpg' );"></div -->
			</div>
			<!-- HEADER IMAGE : begin -->

		</div>
		<!-- HEADER BG : end -->

		<!-- CORE : begin -->
		<div id="core">
			<div class="c-container">
				<div class="row">

					<!-- MIDDLE COLUMN : begin -->
					<div class="middle-column col-md-9 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header" class="m-has-breadcrumbs">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1>Visi Misi</h1>
							</div>
							<!-- PAGE TITLE : end -->

							<!-- BREADCRUMBS : begin -->
							<div class="breadcrumbs">
								<ul>
									<li class="home"><a href="">Home</a></li>
									<li><?php echo $abouts->title; ?></li>
								</ul>
							</div>
							<!-- BREADCRUMBS : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">
								<center>
									<?php
									    $image = array(
									    	'src'    => 'images/uploads/'.$abouts->image,
									        'alt'    => 'Foto About',
									        'width'	 => '50%',
									    );

									    echo img($image);
									?>
								</center><br>
								<p><?php echo $abouts->description; ?></p>


								<!-- GMAP : end -->

								<div class="row">
									<div class="col-md-6">

										<!-- FEATURE : begin -->
										<!-- FEATURE : end -->

									</div>
								</div>

							</div>
						</div>
						<!-- PAGE CONTENT : end -->

						<hr class="c-separator m-margin-top-small m-margin-bottom-small m-transparent hidden-lg hidden-md">

					</div>
					<!-- MIDDLE COLUMN : end -->

					<!-- LEFT COLUMN : begin -->
					
					<div class="left-column col-md-3 col-md-pull-9">

						<!-- SIDE MENU : begin -->
					<?php include("application/views/menu.php"); ?>

						<!-- SIDE MENU : end -->

						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

							<?php include("application/views/widget/widget_potensi.php"); ?>



								<?php include("application/views/widget/widget_berita.php"); ?>
							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

				</div>
			</div>
		</div>
		<!-- CORE : end -->
<?php include("application/views/footer.php"); ?>
