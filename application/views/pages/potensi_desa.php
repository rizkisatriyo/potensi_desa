<?php include("header_pages.php"); ?>		<!-- HEADER : end -->

		
		<!-- HEADER BG : begin -->
		<div class="header-bg">

			<!-- HEADER IMAGE : begin -->
			<!-- To add more images just copy and edit elements with "image-layer" class (see home-2.html template for example)
			Change autoplay speed with "data-autoplay" attribute (in seconds), works only if there are more than one image -->
			<div class="header-image" data-autoplay="8">
			<div class="image-layer" style="background-image: url( '<?= base_url().'assets/template/frontend/' ?>images/background.jpg' );"></div>

				<!-- div class="image-layer" style="background-image: url( 'images/header-02.jpg' );"></div -->
			</div>
			<!-- HEADER IMAGE : begin -->

		</div>
		<!-- HEADER BG : end -->

		<!-- CORE : begin -->
		<div id="core">
			<div class="c-container">
				<div class="row">

					<!-- MIDDLE COLUMN : begin -->
					<div class="middle-column col-md-6 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header" class="m-has-breadcrumbs">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1>Potensi Desa</h1>
							</div>
							<!-- PAGE TITLE : end -->

							<!-- BREADCRUMBS : begin -->
							<div class="breadcrumbs">
								<ul>
									<li class="home"><a href="">Home</a></li>
									<li>Potensi Desa</li>
								</ul>
							</div>
							<!-- BREADCRUMBS : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">

								<!-- GALLERY LIST PAGE : begin -->
								<div class="gallery-list-page gallery-page">
									<div class="c-gallery">
										<!-- You can change the number of columns by changing "m-3-columns" class
										in the following element to m-2-columns | m-4-columns | m-5-columns : begin -->
										<ul class="gallery-images m-layout-masonry m-3-columns">
											<?php
					      						foreach ($home_products as $product)
					      						{

      										?>
											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="<?php echo base_url();?>index.php/category/<?php echo $product->cat_id;?>/<?php echo url_title(strtolower($product->cat_name));?>">
														<img src="<?php echo base_url().$product->big_image;?>" alt="">
													</a>
													<h2 class="gallery-title"><a href="<?php echo base_url();?>index.php/category/<?php echo $product->cat_id;?>/<?php echo url_title(strtolower($product->cat_name));?>"><?php echo $product->cat_name;?></a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->
											<?php 
												} // end for each
											 // end if
										
											?>
											<?php
					      						foreach ($home_products1 as $product)
					      						{

      										?>
											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="<?php echo base_url();?>index.php/category/<?php echo $product->cat_id;?>/<?php echo url_title(strtolower($product->cat_name));?>">
														<img src="<?php echo base_url().$product->big_image;?>" alt="">
													</a>
													<h2 class="gallery-title"><a href="<?php echo base_url();?>index.php/category/<?php echo $product->cat_id;?>/<?php echo url_title(strtolower($product->cat_name));?>"><?php echo $product->cat_name;?></a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->
											<?php 
												} // end for each
											 // end if
										
											?>
											<?php
					      						foreach ($home_products2 as $product)
					      						{

      										?>
											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<div class="gallery-image-inner">
													<a href="<?php echo base_url();?>index.php/category/<?php echo $product->cat_id;?>/<?php echo url_title(strtolower($product->cat_name));?>">
														<img src="<?php echo base_url().$product->big_image;?>" alt="">
													</a>
													<h2 class="gallery-title"><a href="<?php echo base_url();?>index.php/category/<?php echo $product->cat_id;?>/<?php echo url_title(strtolower($product->cat_name));?>"><?php echo $product->cat_name;?></a></h2>
												</div>
											</li>
											<!-- GALLERY IMAGE : end -->
											<?php 
												} // end for each
											 // end if
										
											?>
										</ul>
									</div>
								</div>
								<!-- GALLERY LIST PAGE : begin -->


							</div>
						</div>
						<!-- PAGE CONTENT : end -->

						<hr class="c-separator m-margin-top-small m-margin-bottom-small m-transparent hidden-lg hidden-md">

					</div>
					<!-- MIDDLE COLUMN : end -->

					<!-- LEFT COLUMN : begin -->
					<div class="left-column col-md-3 col-md-pull-6">

						<!-- SIDE MENU : begin -->
						<?php include("application/views/menu.php"); ?>
						<!-- SIDE MENU : end -->

						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<!-- FEATURED GALLERY WIDGET : begin -->
								<?php include("application/views/widget/widget_potensi.php"); ?>

								<!-- FEATURED GALLERY WIDGET : end -->

							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

					<!-- RIGHT COLUMN : begin -->
					<div class="right-column col-md-3">

						<!-- RIGHT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<!-- CATEGORIES WIDGET : begin -->
								<div class="widget categories-widget">
									<div class="widget-inner">
										<h3 class="widget-title">
										<i class="fa fa-list-alt" aria-hidden="true"></i>  Categories</h3>
										<div class="widget-content">
											<?php if($categories=='empty')
												echo " Sorry - No category found";
												else
												{

											?>
											<ul>
												<?php foreach($category as $category){?>
												<li><a href="<?php echo base_url();?>index.php/category/<?php echo $category->cat_id;?>/<?php echo url_title(strtolower($category->cat_name));?>"><?php echo $category->cat_name;?></a>
												</li>
												<?php } ?>
											</ul>
											<?php } ?>
										</div>
									</div>
								</div>
								<!-- CATEGORIES WIDGET : end -->

								<!-- IMAGE WIDGET : begin -->
								<div class="widget image-widget">
									<div class="widget-inner">
										<div class="widget-content">
											<a href="#"><img src="images/poster-01.jpg" alt=""></a>
										</div>
									</div>
								</div>
								<!-- IMAGE WIDGET : end -->

							</div>
						</aside>
						<!-- RIGHT SIDEBAR : end -->

					</div>
					<!-- RIGHT COLUMN : end -->

				</div>
			</div>
		</div>
		<!-- CORE : end -->
<?php include("application/views/footer.php"); ?>