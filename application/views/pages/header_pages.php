<!DOCTYPE html>
<html>
	
<!-- Mirrored from demos.lsvr.sk/townpress.html/demo/ by HTTrack Website Copier/3.x [XR&CO'2010], Tue, 11 Apr 2017 13:42:30 GMT -->
<head>

		<meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Potensi Desa</title>
        <link rel="shortcut icon" href="<?= base_url().'assets/template/frontend/' ?>images/header-logo2.png">

		<!-- GOOGLE FONTS : begin -->
		<script href="<?= base_url().'assets/template/frontend/' ?>library/fonts/font-awesome.min" rel="stylesheet" type="text/css"></script>
		<link href="<?= base_url().'assets/template/frontend/' ?>library/fonts/townpress.ttf" rel="stylesheet" type="text/css">
		<link href="<?= base_url().'assets/template/frontend/' ?>library/fonts/fontawesome-webfont.woff" rel="stylesheet" type="text/css">
		<!-- GOOGLE FONTS : end -->

        <!-- STYLESHEETS : begin -->
		<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/template/frontend/' ?>library/css/style.css">
		<!-- To change to a different pre-defined color scheme, change "red.css" in the following element to blue.css | bluegray.css | green.css | orange.css
		Please refer to the documentation to learn how to create your own color schemes -->
        <link rel="stylesheet" type="text/css" href="<?= base_url().'assets/template/frontend/' ?>library/css/skin/red.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url().'assets/template/frontend/' ?>library/css/custom.css">
		<!--[if lte IE 9]>
		<link rel="stylesheet" type="text/css" href="library/css/oldie.css">
		<![endif]-->
		<style>
        
        #frame-image img {
        	width: 800px;
        	height: 300px;  
        }
    </style>
		<!-- STYLESHEETS : end -->


	</head>
	<header id="header" class="m-has-header-tools m-has-gmap">
			<div class="header-inner">

				<!-- HEADER CONTENT : begin -->
				<div class="header-content">
					<div class="c-container">
						<div class="header-content-inner">

							<!-- HEADER BRANDING : begin -->
							<!-- Logo dimensions can be changed in library/css/custom.css
							You can remove "m-large-logo" class from following element to use standard (smaller) version of logo -->
							<div class="header-branding m-medium-logo">
								<a href=""><span>
									<img src="<?= base_url().'assets/template/frontend/' ?>images/header-logo2.png"
										data-hires="<?= base_url().'assets/template/frontend/' ?>images/header-logo2.2x.png"
										alt="TownPress - Municipality HTML Template">
								</span></a>
							</div>
							<!-- HEADER BRANDING : end -->

							<!-- HEADER TOGGLE HOLDER : begin -->
							<div class="header-toggle-holder">

								<!-- HEADER TOGGLE : begin -->
								<button type="button" class="header-toggle">
									<i class="fa fa-align-justify" aria-hidden="true"></i>
									<span>Menu</span>
								</button>
								<!-- HEADER TOGGLE : end -->
							</div>
							<!-- HEADER TOGGLE HOLDER : end -->

							<!-- HEADER MENU : begin -->
							<!-- This menu is used as both mobile menu (displayed on devices with screen width < 991px)
							and standard header menu (only if Header element has "m-has-standard-menu" class) -->
							<nav class="header-menu">
								<ul>
									<li><a href="index-2.html">Home</a>
										
									</li>
									<li><a href="town-hall.html">Profil Desa</a>
										<ul>
											<li><a href="town-hall.html">Potensi Desa</a></li>
											<li><a href="town-council.html">Peta Desa Banyusari</a></li>
											<li><a href="phone-numbers.html">Profil Aparatur</a></li>
											<li><a href="document-list.html">Visi Misi</a></li>
									
										</ul>
									</li>
									<li><a href="post-list.html">Redaksi</a>
										<ul>
											<li><a href="post-list.html">News</a></li>
											<li><a href="notice-list.html">Notices</a></li>
											<li><a href="event-list.html">Events</a></li>
											<li><a href="gallery-list.html">Galleries</a></li>
										</ul>
									</li>
									<li><a href="statistics.html">Kontak Kami</a>
										<ul>
											<li><a href="statistics.html">Statistics</a></li>
											<li><a href="virtual-tour.html">Virtual Tour</a></li>
											<li><a href="town-history.html">Town History</a></li>
											<li><a href="elements.html">Elements</a></li>
										</ul>
									</li>
								</ul>
							</nav>
							<!-- HEADER MENU : end -->

							<!-- HEADER TOOLS : begin -->
							<div class="header-tools">

								<!-- HEADER SEARCH : begin -->
								<!-- <div class="header-search">
									<form method="get" action="http://demos.lsvr.sk/townpress.html/demo/search-results.html" class="c-search-form">
										<div class="form-fields">
											<input type="text" value="" placeholder="Search this site..." name="s">
											<button type="submit" class="submit-btn"><i class="fa fa-search" aria-hidden="true"></i></button>
										</div>
									</form>
								</div> -->
								<!-- HEADER SEARCH : end -->

								<!-- HEADER GMAP SWITCHER : begin -->
								<!-- Remove following block if you are not using Google Map in this template -->
								<!-- HEADER GMAP SWITCHER : end -->

							</div>
							<!-- HEADER TOOLS : end -->

						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- HEADER : end -->
			<div class="header-bg">

			<!-- HEADER IMAGE : begin -->
			<!-- To add more images just copy and edit elements with "image-layer" class (see home-2.html template for example)
			Change autoplay speed with "data-autoplay" attribute (in seconds), works only if there are more than one image -->
			<div class="header-image" data-autoplay="8">
				<div class="image-layer" style="background-image: url( '<?= base_url().'assets/template/frontend/' ?>images/background.jpg' );"></div>
				<!-- div class="image-layer" style="background-image: url( 'images/header-02.jpg' );"></div -->
			</div>
			<!-- HEADER IMAGE : begin -->

		</div>