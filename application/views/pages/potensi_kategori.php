<?php include("header_pages.php"); ?>		<!-- HEADER : end -->

		
		<!-- HEADER BG : begin -->
		<div class="header-bg">

			<!-- HEADER IMAGE : begin -->
			<!-- To add more images just copy and edit elements with "image-layer" class (see home-2.html template for example)
			Change autoplay speed with "data-autoplay" attribute (in seconds), works only if there are more than one image -->
			<div class="header-image" data-autoplay="8">
			<div class="image-layer" style="background-image: url( '<?= base_url().'assets/template/frontend/' ?>images/background.jpg' );"></div>

				<!-- div class="image-layer" style="background-image: url( 'images/header-02.jpg' );"></div -->
			</div>
			<!-- HEADER IMAGE : begin -->

		</div>
		<!-- HEADER BG : end -->

		<!-- CORE : begin -->
		<div id="core">
			<div class="c-container">
				<div class="row">

					<!-- MIDDLE COLUMN : begin -->
					<div class="middle-column col-md-6 col-md-push-3">

						<!-- PAGE HEADER : begin -->
						<div id="page-header" class="m-has-breadcrumbs">

							<!-- PAGE TITLE : begin -->
							<div class="page-title">
								<h1>Potensi Desa</h1>
							</div>
							<!-- PAGE TITLE : end -->

							<!-- BREADCRUMBS : begin -->
							<div class="breadcrumbs">
								<ul>
									<li class="home"><a href="index-2.html">Home</a></li>
									<li>Potensi Desa</li>
								</ul>
							</div>
							<!-- BREADCRUMBS : end -->

						</div>
						<!-- PAGE HEADER : end -->

						<!-- PAGE CONTENT : begin -->
						<div id="page-content">
							<div class="page-content-inner">

								<!-- GALLERY LIST PAGE : begin -->
								<div class="gallery-list-page gallery-page">
									<div class="c-gallery">
										<!-- You can change the number of columns by changing "m-3-columns" class
										in the following element to m-2-columns | m-4-columns | m-5-columns : begin -->
										<ul class="gallery-images m-layout-masonry m-3-columns ">
										<?php
											$i=1;
											if($category_products!='empty')

											{
											foreach ($category_products as $product)
											{
												$kategori = $this->db->get_where('wg_categories', array('cat_id' => $product->cat_id))->row();

										?>
										
											<!-- GALLERY IMAGE : begin -->
											<li class="gallery-image">
												<a href="<?php echo base_url().$product->big_image;?>" alt="<?php echo $product->item_name; ?>" class="lightbox" title="<?php echo $product->item_desc_short;?>">
													<img src="<?php echo base_url().$product->big_image;?>" alt="">
												</a>
											</li>
											<!-- GALLERY IMAGE : end -->

											
											<?php $i++;
												} // end for each
											} // end if
											else  echo "Nothing Found";
											?>

										</ul>
										<h1><p><?php echo $kategori->cat_name; ?></p></h1>
											<p align="justify">
												<?php echo $kategori->cat_desc; ?>
											</p>
											
									</a>
									</div>
								</div>
								<!-- GALLERY LIST PAGE : begin -->

								<!-- PAGINATION : begin -->
						<!-- PAGINATION : end -->

							</div>
						</div>
						<!-- PAGE CONTENT : end -->

						<hr class="c-separator m-margin-top-small m-margin-bottom-small m-transparent hidden-lg hidden-md">

					</div>
					<!-- MIDDLE COLUMN : end -->

					<!-- LEFT COLUMN : begin -->
					<div class="left-column col-md-3 col-md-pull-6">

						<!-- SIDE MENU : begin -->
						<?php include("application/views/menu.php"); ?>

						<!-- SIDE MENU : end -->

						<!-- LEFT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

														<?php include("application/views/widget/widget_potensi.php"); ?>


							</div>
						</aside>
						<!-- LEFT SIDEBAR : end -->

					</div>
					<!-- LEFT COLUMN : end -->

					<!-- RIGHT COLUMN : begin -->
					<div class="right-column col-md-3">

						<!-- RIGHT SIDEBAR : begin -->
						<aside class="sidebar">
							<div class="widget-list">

								<!-- CATEGORIES WIDGET : begin -->
								<div class="widget categories-widget">
									<div class="widget-inner">
										<h3 class="widget-title">
										<i class="fa fa-list-alt" aria-hidden="true"></i>  Categories</h3>
										<div class="widget-content">
											<?php if($categories=='empty')
												echo " Sorry - No category found";
												else
												{

											?>
											<ul>
												<?php foreach($categories as $category){?>
												<li><a href="<?php echo base_url();?>index.php/category/<?php echo $category->cat_id;?>/<?php echo url_title(strtolower($category->cat_name));?>"><?php echo $category->cat_name;?></a>
												</li>
												<?php } ?>
											</ul>
											<?php } ?>
										</div>
									</div>
								</div>
								<!-- CATEGORIES WIDGET : end -->

								

							</div>
						</aside>
						<!-- RIGHT SIDEBAR : end -->

					</div>
					<!-- RIGHT COLUMN : end -->

				</div>
			</div>
		</div>
		<!-- CORE : end -->
<?php include("application/views/footer.php"); ?>