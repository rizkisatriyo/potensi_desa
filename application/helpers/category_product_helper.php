<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 	
 	if (!function_exists('category_product'))
	{
		function category_product($id_product)
		{ 
			$var =& get_instance();
			$var->load->database();
			$var->db->select();
			$var->db->from('wg_items');
			$var->db->where('cat_id', $id_product);
			$var->db->order_by('cat_id', 'desc');
			$var->db->limit(1, 0);
			$data = $var->db->get()->result();
			return $data;
		}
	}

	if (!function_exists('category_product_all'))
	{
		function category_product_all($id_product)
		{ 
			$var =& get_instance();
			$var->load->database();

			$var->db->select();
			$var->db->from('wg_items');
			$var->db->where('cat_id', $id_product);
			$var->db->limit(3, 1);
			$data = $var->db->get()->result();
			return $data;
		}
	}