<?php
class categories_home extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function getLastCategoryProducts()
    {

        $this->db->select("*");
        $this->db->where("cat_id",55, 56, 57, 58, 59);
        $this->db->limit(1);
        $this->db->order_by('item_id','DESC');
        $result = $this->db->get('wg_items');
        if($result->num_rows()>0)
        return $result->result();
        else
        return 'empty';

    }

    function getLastestCategoryProducts()
    {

        $this->db->select("*");
        $this->db->where("cat_id",55, 56, 57, 58, 59);
        $this->db->limit(4);
        $this->db->order_by('item_id','DESC');
        $result = $this->db->get('wg_items');
        if($result->num_rows()>0)
        return $result->result();
        else
        return 'empty';

    }
  }
