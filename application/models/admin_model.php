<?php
	class admin_model extends CI_Model
	{
		public function get_admin()
		{
			$this->db->where('admin_id', '4');
			return $this->db->get('wg_admin');
		}

		public function get_password_old()
		{
			$this->db->select('password');
			$this->db->from('wg_admin');
			$this->db->where('admin_id', '4');
			$rs = $this->db->get();

			if($rs->num_rows() > 0)
			{
				return $rs;
			}
			else
			{
				return FALSE;
			}
		}

		public function aksi_edit($data = array())
		{
			if(!empty($data) && is_array($data))
			{
				$this->db->where('admin_id', '4');
				$this->db->update('wg_admin', $data);
			}
		}
	}
