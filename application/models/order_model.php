<?php
    class Order_model extends CI_Model
    {
        private $tabel_item = 'wg_orders';

        public function __construct()
        {
            parent::__construct();
        }

        public function getOrderList()
		{
			$this->db->select('*');
			$this->db->from('wg_orders');
			$this->db->join('wg_users', 'wg_users.user_id = wg_orders.user_id');
			$result = $this->db->get();

			if($result->num_rows() > 0)
			{
				return $result->result();
			}
			else
			{
				return "empty";
			}
		}


      function getOrderCount()
		{
				$this->db->select("*");
				$this->db->order_by('order_id','DESC');
				$result = $this->db->get('wg_orders');
				if($result->num_rows()>0)
				return $result->num_rows();
				else
				return 'empty';
		}


		public function getOrderDetails($order_id)
		{
			if(!empty($order_id))
			{
				$this->db->select('i.item_name, c.item_quantity, c.item_price, c.item_total_price');
      			$this->db->from('wg_orders as o');
	      		$this->db->join('wg_cart as c', 'c.order_id=o.order_id', 'left');
      			$this->db->join('wg_items as i', 'i.item_id=c.item_id', 'left');
      			$this->db->where('o.order_id', $order_id);
      			return $this->db->get();
      		}
		}

		function getUserOrder($order_id)
	      {
	      	if(!empty($order_id))
	        {
	      		$this->db->select('o.order_id, u.full_name, u.email, u.telephone');
	      		$this->db->from('wg_orders as o');
	      		$this->db->join('wg_users as u', 'u.user_id=o.user_id', 'left');
	      		$this->db->where('o.order_id', $order_id);
	      		return $this->db->get()->result();
	          }
	      }

      function getAddressOrder($order_id)
      {
      	if(!empty($order_id))
        {
      		$this->db->select('ad.address1, ad.address2, ad.city, ad.county, ad.post_code');
      		$this->db->from('wg_address as ad');
      		$this->db->where('ad.order_id', $order_id);
      		return $this->db->get()->result();
          }
      }

      public function getStatus($order_id)
      {
      	if(!empty($order_id))
      	{
	      	$this->db->select('o.order_status');
	      	$this->db->from('wg_orders as o');
	      	$this->db->where('o.order_id', $order_id);
	      	return $this->db->get()->row();
	     }
      }

      function cancel_order($order_id)
		{

			$this->db->where('order_id',$order_id);
			$this->db->set('order_status','cancelled');
			$this->db->update('wg_orders');
			$url = base_url()."index.php/form-html/order";
			header("Location:$url");

		}
	   function dispatch_order($order_id)
		{

			$this->db->where('order_id',$order_id);
			$this->db->set('order_status','dispatched');
			$this->db->update('wg_orders');
			$url = base_url()."index.php/form-html/order";
			header("Location:$url");

		}

}
