<?php if(! defined('BASEPATH')) exit('No direct script access allowed');
	class Blog_model extends CI_Model
	{
		private $tabel_blog = 'wg_blog';

        public function __construct()
        {
            parent::__construct();
        }

        public function get_all_blog()
        {
        	$this->db->select('blog_id, blog_title, blog_desc, blog_image, blog_date');
			$this->db->from($this->tabel_blog);
			$this->db->where('blog_status', '1');
			$this->db->order_by('blog_id', 'desc');

			$result = $this->db->get();

			if($result->num_rows() > 0)
			{
				return $result->result();
			}
			else
			{
				return "empty";
			}
        }

        public function get_by_id_blog($id = null)
        {
        	if(!empty($id))
        	{
	        	$this->db->select('blog_id, blog_title, blog_desc, blog_image');
						$this->db->from($this->tabel_blog);
						$this->db->where('blog_id', $id);
						$this->db->where('blog_status', '1');
						return $this->db->get();
			}
        }

				function getBlogDetails($blog_id)
				{

						$this->db->select("*");
						$this->db->where('blog_id', $blog_id);
						$result = $this->db->get($this->tabel_blog);
						if($result->num_rows()>0)
						return $result->row();
						else
						return 'empty';

				}


        public function insert_blog($data = array())
        {
            if(!empty($data) && is_array($data))
            {
                $this->db->set($data);
                $this->db->insert($this->tabel_blog);
            }
        }

        public function update_blog($id, $data = array())
        {
            if(!empty($id) && !empty($data) && is_array($data))
            {
                $this->db->where('blog_id', $id);
                $this->db->update($this->tabel_blog, $data);
            }
        }
	}
