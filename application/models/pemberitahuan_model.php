<?php
    class pemberitahuan_model extends CI_Model
    {
        private $tabel_item = 'wg_pemberitahuan';

        public function __construct()
        {
            parent::__construct();
        }

        public function getAllPemberitahuan()
        {
        	$this->db->select();
            $this->db->from($this->tabel_item);
            // $this->db->limit($end, $start);
            $this->db->order_by('pemberitahuan_id', 'desc');

            $result = $this->db->get();
            if($result->num_rows()>0)
            {
            	return $result->result();
            }
            else
            {
            	return 'empty';
            }
        }

        public function get_pemberitahuan($id = null)
        {
            return $this->db->get('wg_pemberitahuan');
            
        }

        //tambah
        public function insert_pemberitahuan($data = array())
        {
            if(!empty($data) && is_array($data))
            {
                $this->db->set($data);
                $this->db->insert($this->tabel_item);
            }
        }

        function edit_data($where,$table){      
            return $this->db->get_where($table,$where);
        }

        // edit
        function update_pemberitahuan($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
    }   

        //delete
        function delete($where,$table){
            $this->db->where($where);
            $this->db->delete($table);
        }
    }
