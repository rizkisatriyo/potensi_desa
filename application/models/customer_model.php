<?php
    class Customer_model extends CI_Model
    {
        private $tabel_item = 'wg_items';

        public function __construct()
        {
            parent::__construct();
        }

        function getCustomerList()
        {

          $this->db->select("*");
          $this->db->where('status', 1);
          $this->db->order_by('user_id','DESC');
          $result = $this->db->get('wg_users');

          if($result->num_rows()>0)
          return $result->result();
          else
          return "empty";

        }

        // show cust
        public function get_customer($id = null)
        {   
            if(!empty($id))
            {
                $this->db->from('wg_users');
                $this->db->where('user_id', $id);
                return $this->db->get();
            }
        }

        // edit
        public function update_customer($id, $data = array())
        {   
            if(!empty($id) && !empty($data) && is_array($data))
            {
                $this->db->where('user_id', $id);
                $this->db->update('wg_users', $data);    
            }
        }

        //delete
        public function delete_customer($id = null)
        {
            if(!empty($id))
            {
                $this->db->where('user_id', $id);
                $this->db->delete('wg_users');
            }
        }


        function getCustomerCountt()
        {
          $this->db->select('*');
          $this->db->order_by("user_id","desc");
          $result = $this->db->get('wg_users');
          if($result->num_rows()>0)
          return $result->num_rows();
          else
          return "empty";

        }
    }
