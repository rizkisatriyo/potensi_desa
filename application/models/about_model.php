<?php
	class about_model extends CI_Model
	{
		public function getAbout()
		{
			$this->db->where('about_id', '1');
			$rs = $this->db->get('wg_about');

			if($rs->num_rows() > 0)
			{
				return $rs;
			}
			else
			{
				return FALSE;
			}
		}

		public function update_about($data = array())
		{
			if(!empty($data) && is_array($data))
			{
				$this->db->where('about_id', '1');
				$this->db->update('wg_about', $data);
			}
		}

		public function getProfil()
		{
			$this->db->where('profil_id', '1');
			$rs = $this->db->get('wg_profil');

			if($rs->num_rows() > 0)
			{
				return $rs;
			}
			else
			{
				return FALSE;
			}
		}

		public function update_Profil($data = array())
		{
			if(!empty($data) && is_array($data))
			{
				$this->db->where('profil_id', '1');
				$this->db->update('wg_profil', $data);
			}
		}
	}