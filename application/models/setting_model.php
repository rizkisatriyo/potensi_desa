<?php if(! defined('BASEPATH')) exit('No direct script access allowed');
	class Setting_model extends CI_Model
	{
		private $tabel_logo    = 'wg_logo';
		private $tabel_contact = 'wg_contact';
		private $tabel_social  = 'wg_social';

        public function __construct()
        {
            parent::__construct();
        }

        public function get_all_logo()
        {
        	$this->db->select('*');
			$this->db->from($this->tabel_logo);
			$this->db->where('id', 1);
			return $this->db->get()->row();
        }

      /*public function getLogoById($id)
        {
        	if(!empty($id))
        	{
        		$this->db->select('*');
        		$this->db->from($this->tabel_logo);
        		$this->db->where('id', $id);
        		return $this->db->get();
        	}
        }*/

        public function get_all_contact()
        {
        	$this->db->select('*');
			$this->db->from($this->tabel_contact);
			return $this->db->get()->result();
        }

        public function get_all_social()
		{
			$this->db->select('*');
			$this->db->from($this->tabel_social);
			return $this->db->get()->result();
		}

		public function update($id, $data = array(), $tabel)
        {
        	if(!empty($id) && !empty($data) && is_array($data) && !empty($tabel))
            {
            	switch ($tabel) 
            	{
            		case 'logo':
            			$tabel = $this->tabel_logo;
            			# code...
            			break;
            		case 'contact':
            			$tabel = $this->tabel_contact;
            			# code...
            			break;
            		case 'social':
            			$tabel = $this->tabel_social;
            			# code...
            			break;
            		default:
            			$tabel = null;
            			# code...
            			break;
            	}

            	if(!is_null($tabel))
            	{
	                $this->db->where('id', $id);
	                $this->db->update($tabel, $data);
            	}
            }
        }
    }