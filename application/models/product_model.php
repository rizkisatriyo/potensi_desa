<?php
    class product_model extends CI_Model
    {
        private $tabel_item = 'wg_items';

        public function __construct()
        {
            parent::__construct();
        }

        public function getAllCategory()
        {
            $this->db->select('cat_id, cat_name');
            $this->db->from('wg_categories');
            $this->db->where('status', 1);
            return $this->db->get()->result();
        }

        public function getAllProduct()
        {
        	$this->db->select();
            $this->db->from('wg_items');
            $this->db->join('wg_categories', 'wg_categories.cat_id = wg_items.cat_id');
            // $this->db->limit($end, $start);
            $this->db->order_by('item_id', 'desc');

            $result = $this->db->get();
            if($result->num_rows()>0)
            {
            	return $result->result();
            }
            else
            {
            	return 'empty';
            }
        }

        public function get_product($id = null)
        {
            if(!empty($id))
            {
                $this->db->from($this->tabel_item);
                $this->db->where('item_id', $id);
                return $this->db->get();
            }
        }

        //tambah
        public function insert_product($data = array())
        {
            if(!empty($data) && is_array($data))
            {
                $this->db->set($data);
                $this->db->insert($this->tabel_item);
            }
        }

        // edit
        public function update_product($id, $data = array())
        {
            if(!empty($id) && !empty($data) && is_array($data))
            {
                $this->db->where('item_id', $id);
                $this->db->update($this->tabel_item, $data);
            }
        }

        //delete
        public function delete_product($id = null)
        {
            if(!empty($id))
            {
                $this->db->where('item_id', $id);
                $this->db->delete($this->tabel_item);
            }
        }
    }
