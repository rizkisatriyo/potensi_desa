<?php
	class Category_model extends CI_Model
	{
		function __construct()
    	{
	        parent::__construct();
	    }

	    public function getAllCategory()
    	{
        	$this->db->select("cat_id, cat_name");
        	$this->db->where('status', 1);
        	$result = $this->db->get('wg_categories');

        	if($result->num_rows() > 0)
        	{
        		return $result->result();
        	}
        	else
        	{
        		return 'empty';
        	}
    	}

			function add_category($options)
			{

				$cat_name = $options['cat_name'];

				$data = array(
				"cat_name" => $cat_name);

					if($this->check_category_exists($cat_name)=='0')
					{
						$this->db->insert('wg_categories',$data);
						$url = base_url()."index.php/form-html/category";
						header("Location:$url");
						exit();
					}
					else
					{
							$url = base_url()."index.php/form-html/add_category/failed";
							header("Location:$url");
					}

				}

			function check_category_exists($cat_name)
			{
				$this->db->select('*');
				$this->db->where('cat_name',$cat_name);
				$result = $this->db->get('wg_categories');

				if($result->num_rows()>0)
				return "1";
				else
				return "0";

			}

			function getAllCategories()
			{

					$this->db->select("*");
					$result = $this->db->get('wg_categories');
					if($result->num_rows()>0)
					return $result->result();
					else
					return 'empty';

			}

			public function update_category($cat_id, $data = array())
			{
					if(!empty($cat_id) && !empty($data) && is_array($data))
					{
							$this->db->where('cat_id', $cat_id);
							$this->db->update('wg_categories', $data);
					}
			}

			public function delete_category($id = null)
		 {
				 if(!empty($id))
				 {
						 $this->db->where('cat_id', $id);
						 $this->db->delete('wg_categories');
				 }
		 }

		 function getCategoryDetails($cat_id)
		 	{

		 			$this->db->select("*");
		 			$this->db->where('cat_id',$cat_id);
		 			$result = $this->db->get('wg_categories');
		 			if($result->num_rows()>0)
		 			return $result->result();
		 			else
		 			return 'empty';

		 	}


	}
