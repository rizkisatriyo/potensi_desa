<?php
class banner_front extends CI_Model
{

  function selectAllBannerFront()
  {
    $this->db->where('kode_banner', '1');
    return $this->db->get('wg_banner')->result();
   }

   function selectAllBannerProduct()
  {
    $this->db->where('kode_banner', '2');
    return $this->db->get('wg_banner')->result();
   }

    function getBannerProduct()
  {
    $this->db->where('kode_banner', '2');
    $rs =$this->db->get('wg_banner'); 
    if($rs->num_rows() > 0)
    {
      return $rs->result();
    }
    else
      return "kosong";
   }

    function selectAllBannerSingle()
    {
    $this->db->where('kode_banner', '3');
    return $this->db->get('wg_banner')->row();
    }

   function getAllBannerCount()
   {

     $this->db->select("*");
     $result = $this->db->get('wg_banner');
     if($result->num_rows()>0)
     return $result->num_rows();
     else
     return 'empty';

   }
   function insert_banner_front($data = array())
   {
     if(!empty($data) && is_array($data))
     {
         $this->db->insert('wg_banner', $data);
     }

   }
   function update_image_links($image,$thumbnail,$id)
 	{

 			$data = array(
 			"image" => $image,
      "thumbnail" => $thumbnail
    );

 			$this->db->where('banner_id',$id);
 			$this->db->update('wg_banner',$data);

 	}
  function delete_banner($id)
  {

    $this->db->where('banner_id',$id);
    $this->db->delete('wg_banner');

  }
  function getBannerDetails($id, $kode)
  {
      if(!empty($id) && !empty($kode))
      {
        $this->db->select("*");
        $this->db->where('banner_id', $id);
        $this->db->where('kode_banner', $kode);
        return $this->db->get('wg_banner');  
      }
  }


  function update_banner_front($id, $data)
  {
    if(!empty($id) && !empty($data) && is_array($data))
    {
      $this->db->where('banner_id', $id);
      $this->db->update('wg_banner', $data);
  }
  }

 }
