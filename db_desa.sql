-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 20, 2018 at 04:09 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_desa`
--

-- --------------------------------------------------------

--
-- Table structure for table `wg_about`
--

CREATE TABLE `wg_about` (
  `about_id` int(3) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wg_about`
--

INSERT INTO `wg_about` (`about_id`, `title`, `description`, `image`) VALUES
(1, 'Visimisi Desa', 'Sesuai dengan kaidah perundang-undangan bahwa RKP Desa harus selaras dengan RPJM Desa, maka RKP Desa Banyusari Tahun 2017 disusun dengan memperhatikan Visi dan Misi Desa Banyusari yang tertuang dalam RPJM Desa Banyusari Tahun 2017 , sebagai dasar dalam pelaksanaan pembangunan Desa Banyusari, yaitu : “MEWUJUDKAN DESA BANYUSARI YANG MANDIRI, MAJU DAN SEJAHTERA DENGAN DILANDASI MASYARAKAT YANG AGAMIS”.Definisi operasional atau yang dimaksud dengan “ Mandiri, Maju dan Sejahtera dengan Dilandasi Masyarakat yang Agamis ” dalam Visi kami adalah sebagai berikut :Mandiri&nbsp;: Suatu keadaan dimana tatanan kehidupan masyarakat Desa Banyusari berada dalam tatanan penghidupan yang cukup baik sehingga dapat memenuhi kebutuhan hidupnya dengan baik dan secara terus menerus berubah kearah yang lebih maju serta tanpa kebergantungan kepada pihak-pihak lain.Maju : Suatu keadaan dimana masyarakat Desa Banyusari yang lebih baik dengan meningkatnya sumberdaya manusia yang berkualitas, keadaan sosial ekonomi masyarakat yang stabil, yang ditandai dengan berkurangnya tingkat kemiskinan dan pengangguran serta ditopang oleh keadaan masyarakat Desa Banyusari yang partisipasif.Sejahtera : Suatu kondisi dimana masyarakat Desa Banyusari dapat meningkatkan pemenuhan kebutuhan dasarnya yang ditandai dengan peningkatan kesehatan, penuhan pendidikan dasar dan peningkatan daya beli dalam kondisi masyarakat Desa Banyusari yang aman, tentram dan saling menghormati.Agamis	:	Suatu kondisi dimana masyarakat Desa Banyusari dapat mempelajari, memahami, melaksanakan dan mengamalkan ajaran agamanya dalam tatanan kehidupan sehari hari. Dalam Rangka mencapai visi yang telah ditetapkan, maka visi tersebut diimplementasikan dalam beberapa misi pembangunan sebagai berikut :<br><ol><li>Meningkatkan tarap kehidupan masyarakat Desa Banyusari melalui peningkatan perekonomian, pendidikan dan kesehatan yang merata.</li><li>Meningkatkan kehidupan beragama yang berkualitas yang diimplentasikan dalam kehidupan sehari-hari.</li><li>Meningkatkan pembangunan infrastruktur perDesaan sebagai pendukung terhadap peningkatan sosial budaya dan perekonomian masyarakat.</li><li>Meningkatkan pelayanan kepada masyarakat Desa Banyusari melalui peningkatan aparatur dan permudahan birokrasi.</li><li>Meningkatkan kesejahteraan masyarakat Desa Banyusari melalui peningkatan partisipasi dan pemberdayaan.</li></ol><br><br><br>', 'IMG20180101080704.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `wg_address`
--

CREATE TABLE `wg_address` (
  `address_id` int(10) NOT NULL,
  `order_id` int(10) NOT NULL,
  `address1` varchar(100) NOT NULL,
  `address2` varchar(100) NOT NULL,
  `city` varchar(20) NOT NULL,
  `county` varchar(20) NOT NULL,
  `post_code` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wg_address`
--

INSERT INTO `wg_address` (`address_id`, `order_id`, `address1`, `address2`, `city`, `county`, `post_code`) VALUES
(1, 1, 'batujaya', 'cigasong', 'majalengka', 'Shetland', '2019'),
(2, 2, 'batujaya', 'cigasong', 'majalengka', 'Shetland', '2019'),
(3, 3, 'batujaya', 'cigasong', 'majalengka', 'Shetland', '2019'),
(4, 4, 'batujaya', 'cigasong', 'majalengka', 'Shetland', '2019'),
(5, 5, 'batujaya', 'cigasong', 'majalengka', 'Shetland', '2019'),
(6, 6, 'batujaya', 'cigasong', 'majalengka', 'Shetland', '2019'),
(7, 7, 'batujaya', 'cigasong', 'majalengka', 'Shetland', '2019'),
(8, 8, 'batujaya', 'cigasong', 'majalengka', 'Shetland', '2019'),
(9, 9, 'batujaya', 'cigasong', 'majalengka', 'Shetland', '2019'),
(10, 10, 'batujaya', 'cigasong', 'majalengka', 'Shetland', '2019'),
(11, 11, 'batujaya', 'cigasong', 'majalengka', 'Shetland', '2019'),
(12, 12, 'batujaya', 'cigasong', 'majalengka', 'Shetland', '2019'),
(13, 13, 'batujaya', 'cigasong', 'majalengka', 'Shetland', '2019'),
(14, 14, 'Ds Blimbing Wuluh RT 01 RW 05 Kec. Siwalan Kab. Pekalongan', 'yugi', 'Kab. Pekalongan', 'Jawa Tengah', '51154');

-- --------------------------------------------------------

--
-- Table structure for table `wg_admin`
--

CREATE TABLE `wg_admin` (
  `admin_id` int(4) NOT NULL,
  `username` varchar(15) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wg_admin`
--

INSERT INTO `wg_admin` (`admin_id`, `username`, `password`) VALUES
(4, 'admin', 'V+BMuAYoCk9hs2t6h/FZyqYVk5/6NK7z3EYHjbw0u9/8OkILGFMpV8G3qI4tcdUp6EkH2yCWtCpBb+6SLbtgwg==');

-- --------------------------------------------------------

--
-- Table structure for table `wg_banner`
--

CREATE TABLE `wg_banner` (
  `banner_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `image` text,
  `kode_banner` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wg_banner`
--

INSERT INTO `wg_banner` (`banner_id`, `title`, `description`, `image`, `kode_banner`) VALUES
(30, 'banner satu', 's', 'slide3-1.jpg', 1),
(31, 'banner dua', 'dua', 'slide3-2.jpg', 1),
(32, 'terserah nantikan', '', 'IMG_20180107_070644.jpg', 3),
(33, 'SHOP', 'Total Cosmetics Solution', 'shop-banner1.jpg', 2);

-- --------------------------------------------------------

--
-- Table structure for table `wg_blog`
--

CREATE TABLE `wg_blog` (
  `blog_id` int(11) NOT NULL,
  `blog_title` varchar(100) NOT NULL,
  `blog_desc` text NOT NULL,
  `blog_image` text NOT NULL,
  `blog_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `blog_status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wg_blog`
--

INSERT INTO `wg_blog` (`blog_id`, `blog_title`, `blog_desc`, `blog_image`, `blog_date`, `blog_status`) VALUES
(1, 'Postingan Pertama', 'postingan awal yang baru', '26047392_1696001870421278_7232703141766461567_n1.jpg', '2017-03-31 01:44:18', 1),
(2, 'Blog 2', 'dgsdfgfg', 'IMG20180101074700.jpg', '2017-03-31 01:58:08', 1),
(3, 'Postingan Kedua', 'egfdsgsdgdsfg', 'IMG_20180107_070427.jpg', '2017-03-31 02:30:13', 1),
(4, 'Postingan Kedua', 'rawa - rawwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww', 'IMG_20180107_0704541.jpg', '2018-01-11 06:03:30', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wg_categories`
--

CREATE TABLE `wg_categories` (
  `cat_id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(30) NOT NULL DEFAULT '',
  `status` varchar(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wg_categories`
--

INSERT INTO `wg_categories` (`cat_id`, `cat_name`, `status`) VALUES
(55, 'Kerajinan', '1'),
(56, 'Wana Wisata', '1'),
(59, 'Makanan Khas', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wg_contact`
--

CREATE TABLE `wg_contact` (
  `id` int(11) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wg_contact`
--

INSERT INTO `wg_contact` (`id`, `address`, `telephone`, `email`) VALUES
(1, 'Malausma', '(0233) 12346789', 'banyusari@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `wg_items`
--

CREATE TABLE `wg_items` (
  `cat_id` int(10) NOT NULL DEFAULT '0',
  `item_id` bigint(20) NOT NULL,
  `item_name` varchar(250) NOT NULL DEFAULT '',
  `item_desc_short` text NOT NULL,
  `item_status` tinyint(1) NOT NULL DEFAULT '0',
  `thumbnail` varchar(100) NOT NULL DEFAULT '',
  `big_image` varchar(100) NOT NULL DEFAULT '',
  `medium_image` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wg_items`
--

INSERT INTO `wg_items` (`cat_id`, `item_id`, `item_name`, `item_desc_short`, `item_status`, `thumbnail`, `big_image`, `medium_image`) VALUES
(55, 74, 'CREAM - New Gamat', 'Gamat yang\r\nberasal dari spesies Golden Stichopus Variegatus yaitu spesies terbaik dan\r\nsatu-satunya spesies yang mengandung Gamapeptide yang kaya akan unsur kolagen.\r\nDapat meningkatkan kecantikan. Tanpa perlu obat-obatan kimia yang memberikan\r\nefek samping.', 0, 'images/uploads/CREAM_-_New_Gamat_small.jpg', 'images/uploads/CREAM_-_New_Gamat.jpg', 'images/uploads/CREAM_-_New_Gamat_med.jpg'),
(55, 75, 'iugftirug', 'oihfdouhsf', 0, 'images/uploads/iugftirug_small.jpg', 'images/uploads/iugftirug.jpg', 'images/uploads/iugftirug_med.jpg'),
(56, 76, 'r3fewewrw', 'weqqrewt', 0, 'images/uploads/r3fewewrw_small.jpg', 'images/uploads/r3fewewrw.jpg', 'images/uploads/r3fewewrw_med.jpg'),
(59, 77, 'qwrwer', 'etr', 0, 'images/uploads/qwrwer_small.jpg', 'images/uploads/qwrwer.jpg', 'images/uploads/qwrwer_med.jpg'),
(55, 78, 'Kerajinan Tangan Mendong', 'Kerajinan\r\ntangan yang terbuat dari bahan bambu (awi tali) ini dibuat untuk dijadikan\r\ntambang dan berbagai kerajinan. Tali tambang ini dinamakan kerajinan mendong\r\natau warga Desa Banyusari sering menyebut itu kerajinan putihan. Putihan atau\r\nmendong ini sudah dipasarkan ke industri daerah Cirebon dan Tasik.&nbsp;', 0, 'images/uploads/Mendong_small.jpg', 'images/uploads/Mendong.jpg', 'images/uploads/Mendong_med.jpg'),
(59, 79, 'majalengka', 'rtuyuio', 0, 'images/uploads/majalengka_small.jpg', 'images/uploads/majalengka.jpg', 'images/uploads/majalengka_med.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `wg_logo`
--

CREATE TABLE `wg_logo` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `tentang` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wg_logo`
--

INSERT INTO `wg_logo` (`id`, `image`, `tentang`) VALUES
(1, 'IMG_20180107_070454.jpg', 'Dilihat dari topologi dan kantur tanah desa Banyusari Kecamatan Malausma secara umum berupa pegunungan dan bukit yang berada di pegunungan dan bukit yang berada pada ketinggia antara 900 s/d 1.000 m diatas permukaan laut dengan suhu rata-rata 25-30 0 Celcius. Potensi Desa Banyusari cukup besar baik yang sudah dimanpaatkan maupun yang belum dimanpaatkan secara maksimal. ');

-- --------------------------------------------------------

--
-- Table structure for table `wg_profil`
--

CREATE TABLE `wg_profil` (
  `profil_id` int(3) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wg_profil`
--

INSERT INTO `wg_profil` (`profil_id`, `title`, `description`, `image`) VALUES
(1, 'Profil Desa', 'Desa Banyusari merupakan bagian dari\r\nwilayah administrasi Kecamatan Malausma Kabupaten Majalengka dengan luas\r\nwilayah ± 378 hektar yang\r\nterdiri atas 6 Dusun, 8 RW dan 31 RT dengan jumlah\r\npenduduk 5.422 jiwa yang terdiri dari 2.724 laki-laki dan\r\n2.698 perempuan dan jumlah Kepala Keluarga 1.542 KK,\r\n\r\nBatas wilayah administrasi Desa Banyusari sebelah barat Desa Malausma, sebelah Utara\r\nDesa Lebakwangisebelah Selatan Kabupaten Ciamis, dan sebelah\r\nTimur Desa Jagamulya. Sedangkan Jarak dari\r\nDesa Banyusari ke Ibu Kota Kecamatan ± 1\r\nkm, ke Ibu Kota Kabupaten ± 54 km, dan ke Ibu Kota Provinsi ± 124 km.\r\n\r\nDilihat dari topologi dan kantur tanah desa Banyusari Kecamatan Malausma\r\nsecara umum berupa pegunungan dan bukit yang berada di pegunungan dan bukit\r\nyang berada pada ketinggia antara 900 s/d 1.000 m diatas permukaan laut dengan\r\nsuhu rata-rata 25-30 0 Celcius. Potensi Desa Banyusari cukup besar\r\nbaik yang sudah dimanpaatkan maupun yang belum dimanpaatkan secara maksimal. \r\n\r\nDesa\r\nBanyusari merupakan bagian dari wilayah administrasi Kecamatan Malausma\r\nKabupaten Majalengka dengan luas wilayah ± 378 hektar yang terdiri\r\natas 6 Dusun, 8 RW dan 31 RT dengan jumlah penduduk 5.422 jiwa yang terdiri\r\ndari 2.724 laki-laki dan 2.698 perempuan dan jumlah Kepala Keluarga 1.542 KK.\r\n\r\nPerkembangan\r\npemerintahan yang baru dibentuk dengan Sumber Daya Manusia yang memadai, maka\r\nperkembangan Desa Banyusari sangat pesat dibidang ekonomi. Hal ini menyebabkan\r\nDesa Banyusari punya ciri dibandingkan dengan desa-desa yang lain, ciri\r\ntersebut yaitu kerajinan sapunya yang terkenal ke luar desa bahkan keluar\r\nnegeri. Para pimpinan Desa Banyusari dari mulai berdirinya Desa Banyusari\r\nadalah sebagai berikut :\r\n<br><br>&nbsp;1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\nBpk. AMIN\r\ndari Tahun 1981 s/d Tahun 1983\r\n\r\n2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bpk. MADKONI dari Tahun 1984 s/d Tahun 1990\r\n\r\n3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bpk. SIROJUDIN dari Tahun 1991 s/d Tahun 1992\r\n\r\n4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bpk. MAMAN dari Tahun 1993 s/d Tahun 1997\r\n\r\n5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bpk. ALINURDIN dari Tahun 1998 s/d Tahun 1999\r\n\r\n6.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bpk. KOMAR dari Tahun 2000 s/d Tahun 2008\r\n\r\n7.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bpk. JEJEN SURJAENA, S.Pd.I dari Tahun 2009 s/d Tahun 2010\r\n\r\n8.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bpk. ASEP PAHRUDIN&nbsp; dari Tahun\r\n2011 s/d 2012\r\n\r\n9.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Bpk.\r\nJUNADI dari Tahun 2014 s/d sekarang.\r\n\r\n&nbsp;\r\n\r\nDilihat dari topologi dan kantur tanah desa Banyusari Kecamatan Malausma\r\nsecara umum berupa pegunungan dan bukit yang berada di pegunungan dan bukit\r\nyang berada pada ketinggia antara 900 s/d 1.000 m diatas permukaan laut dengan\r\nsuhu rata-rata 25-30 0 Celcius. Potensi Desa Banyusari cukup besar\r\nbaik yang sudah dimanpaatkan maupun yang belum dimanpaatkan secara maksimal. \r\n\r\nBatas wilayah administrasi Desa Banyusari sebelah\r\nbarat Desa Malausma, sebelah Utara Desa Lebakwangi sebelah Selatan Kabupaten\r\nCiamis, dan sebelah Timur Desa Jagamulya. Sedangkan Jarak dari\r\nDesa Banyusari ke Ibu Kota Kecamatan ± 1 km, ke Ibu Kota Kabupaten ± 54 km, dan\r\nke Ibu Kota Provinsi ± 124 km. Sesuai dengan peta sebagai berikut :Desa Banyusari merupakan bagian dari\r\nwilayah administrasi Kecamatan Malausma Kabupaten Majalengka dengan luas\r\nwilayah ± 378 hektar yang\r\nterdiri atas 6 Dusun, 8 RW dan 31 RT dengan jumlah\r\npenduduk 5.422 jiwa yang terdiri dari 2.724 laki-laki dan\r\n2.698 perempuan dan jumlah Kepala Keluarga 1.542 KK,\r\n\r\nBatas wilayah administrasi Desa Banyusari sebelah barat Desa Malausma, sebelah Utara\r\nDesa Lebakwangisebelah Selatan Kabupaten Ciamis, dan sebelah\r\nTimur Desa Jagamulya. Sedangkan Jarak dari\r\nDesa Banyusari ke Ibu Kota Kecamatan ± 1\r\nkm, ke Ibu Kota Kabupaten ± 54 km, dan ke Ibu Kota Provinsi ± 124 km.\r\n\r\nDilihat dari topologi dan kantur tanah desa Banyusari Kecamatan Malausma\r\nsecara umum berupa pegunungan dan bukit yang berada di pegunungan dan bukit\r\nyang berada pada ketinggia antara 900 s/d 1.000 m diatas permukaan laut dengan\r\nsuhu rata-rata 25-30 0 Celcius. Potensi Desa Banyusari cukup besar\r\nbaik yang sudah dimanpaatkan maupun yang belum dimanpaatkan secara maksimal. \r\n\r\nDesa\r\nBanyusari merupakan bagian dari wilayah administrasi Kecamatan Malausma\r\nKabupaten Majalengka dengan luas wilayah ± 378 hektar yang terdiri\r\natas 6 Dusun, 8 RW dan 31 RT dengan jumlah penduduk 5.422 jiwa yang terdiri\r\ndari 2.724 laki-laki dan 2.698 perempuan dan jumlah Kepala Keluarga 1.542 KK.\r\n\r\nPerkembangan\r\npemerintahan yang baru dibentuk dengan Sumber Daya Manusia yang memadai, maka\r\nperkembangan Desa Banyusari sangat pesat dibidang ekonomi. Hal ini menyebabkan\r\nDesa Banyusari punya ciri dibandingkan dengan desa-desa yang lain, ciri\r\ntersebut yaitu kerajinan sapunya yang terkenal ke luar desa bahkan keluar\r\nnegeri. Para pimpinan Desa Banyusari dari mulai berdirinya Desa Banyusari\r\nadalah sebagai berikut :\r\n<br><br><ol><li>Bpk. AMIN\r\ndari Tahun 1981 s/d Tahun 1983&nbsp;</li><li>Bpk. MADKONI dari Tahun 1984 s/d Tahun 1990&nbsp;</li><li>Bpk. SIROJUDIN dari Tahun 1991 s/d Tahun 1992&nbsp;</li><li>Bpk. MAMAN dari Tahun 1993 s/d Tahun 1997&nbsp;</li><li>Bpk. ALINURDIN dari Tahun 1998 s/d Tahun 1999&nbsp;</li><li>Bpk. KOMAR dari Tahun 2000 s/d Tahun 2008&nbsp;</li><li>Bpk. JEJEN SURJAENA, S.Pd.I dari Tahun 2009 s/d Tahun 2010</li><li>Bpk. ASEP PAHRUDIN&nbsp; dari Tahun\r\n2011 s/d 2012&nbsp;</li><li>Bpk.\r\nJUNADI dari Tahun 2014 s/d sekarang. &nbsp; &nbsp;</li></ol><br>Dilihat dari topologi dan kantur tanah desa Banyusari Kecamatan Malausma\r\nsecara umum berupa pegunungan dan bukit yang berada di pegunungan dan bukit\r\nyang berada pada ketinggia antara 900 s/d 1.000 m diatas permukaan laut dengan\r\nsuhu rata-rata 25-30 0 Celcius. Potensi Desa Banyusari cukup besar\r\nbaik yang sudah dimanpaatkan maupun yang belum dimanpaatkan secara maksimal. \r\n\r\nBatas wilayah administrasi Desa Banyusari sebelah\r\nbarat Desa Malausma, sebelah Utara Desa Lebakwangi sebelah Selatan Kabupaten\r\nCiamis, dan sebelah Timur Desa Jagamulya. Sedangkan Jarak dari\r\nDesa Banyusari ke Ibu Kota Kecamatan ± 1 km, ke Ibu Kota Kabupaten ± 54 km, dan\r\nke Ibu Kota Provinsi ± 124 km. Sesuai dengan peta sebagai berikut :', 'peta_desa.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `wg_social`
--

CREATE TABLE `wg_social` (
  `id` int(11) NOT NULL,
  `google_plus` varchar(100) NOT NULL,
  `twitter` varchar(100) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `instagram` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wg_social`
--

INSERT INTO `wg_social` (`id`, `google_plus`, `twitter`, `facebook`, `instagram`) VALUES
(1, 't', 't', 't', 'rytryt');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wg_about`
--
ALTER TABLE `wg_about`
  ADD PRIMARY KEY (`about_id`);

--
-- Indexes for table `wg_address`
--
ALTER TABLE `wg_address`
  ADD PRIMARY KEY (`address_id`);

--
-- Indexes for table `wg_admin`
--
ALTER TABLE `wg_admin`
  ADD UNIQUE KEY `admin_id` (`admin_id`);

--
-- Indexes for table `wg_banner`
--
ALTER TABLE `wg_banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indexes for table `wg_blog`
--
ALTER TABLE `wg_blog`
  ADD PRIMARY KEY (`blog_id`);

--
-- Indexes for table `wg_categories`
--
ALTER TABLE `wg_categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `wg_contact`
--
ALTER TABLE `wg_contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wg_items`
--
ALTER TABLE `wg_items`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `wg_logo`
--
ALTER TABLE `wg_logo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wg_profil`
--
ALTER TABLE `wg_profil`
  ADD PRIMARY KEY (`profil_id`);

--
-- Indexes for table `wg_social`
--
ALTER TABLE `wg_social`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wg_about`
--
ALTER TABLE `wg_about`
  MODIFY `about_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wg_address`
--
ALTER TABLE `wg_address`
  MODIFY `address_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `wg_admin`
--
ALTER TABLE `wg_admin`
  MODIFY `admin_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `wg_banner`
--
ALTER TABLE `wg_banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `wg_blog`
--
ALTER TABLE `wg_blog`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `wg_categories`
--
ALTER TABLE `wg_categories`
  MODIFY `cat_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT for table `wg_contact`
--
ALTER TABLE `wg_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wg_items`
--
ALTER TABLE `wg_items`
  MODIFY `item_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT for table `wg_logo`
--
ALTER TABLE `wg_logo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wg_profil`
--
ALTER TABLE `wg_profil`
  MODIFY `profil_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wg_social`
--
ALTER TABLE `wg_social`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
